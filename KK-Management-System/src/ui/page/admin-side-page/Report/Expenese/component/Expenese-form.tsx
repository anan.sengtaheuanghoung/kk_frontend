import './Expenese-form.css'
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers-pro";
import { AdapterDayjs } from "@mui/x-date-pickers-pro/AdapterDayjs";
import ExpensePopUP from './Expense-Popup';
import { create } from 'zustand';
import dayjs from 'dayjs';

interface StoreState {
  startDate: string;
  endDate: string;
  setStartDate: (date: string) => void;
  setEndDate: (date: string) => void;
}

export const useExpenseStore = create<StoreState>((set) => ({
  startDate: '',
  endDate: '',
  setStartDate: (date: string) => set({ startDate: date }),
  setEndDate: (date: string) => set({ endDate: date }),
}));


function ExpeneseForm () {
  const {  setStartDate, setEndDate } = useExpenseStore();
  const handleStartDateChange = (date: Date | null) => {
    if (date) {
      setStartDate(dayjs(date).format('YYYY-MM-DD')); // Format as needed
    }
  };
  const handleEndDateChange = (date: Date | null) => {
    if (date) {
      setEndDate(dayjs(date).format('YYYY-MM-DD')); // Format as needed
    }
  };  
  return (
    <div>
      <div className="ExpeneseForm">
        <div className="ExpeneseForm-title">
          <p>ລາຍງານລາຍຈ່າຍ</p>
        </div>
        <div className="ExpeneseForm-input">
          <div className="ExpeneseForm-input-grid1 ">
          <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={["DatePicker"]}>
                <DatePicker
                  label="ຈາກວັນທີ"
                  format="DD/MM/YYYY"
                  onChange={handleStartDateChange}

                />
              </DemoContainer>
            </LocalizationProvider>_
             
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={["DatePicker"]}>
                <DatePicker format="DD/MM/YYYY"
                label="ເຖິງວັນທີ"
                onChange={handleEndDateChange}
/>
              </DemoContainer>
            </LocalizationProvider>
          </div>
          <div className="ExpeneseForm-input-grid2 ">           
            <ExpensePopUP/>            
          </div>
           
        </div>
      </div>
    </div>
  );
}
export default ExpeneseForm