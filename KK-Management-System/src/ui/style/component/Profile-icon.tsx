import * as React from "react";
import { Popper } from "@mui/base/Popper";
import { styled, css } from "@mui/system";
import PersonIcon from "@mui/icons-material/Person";
import "./Profile-icon.css";
import LOGO from "../../../image/LOGO.png";
import { handleLogOut } from "../../../service/http/axios-client/LogOut-axios"; 
import { useAuthStore } from "../Header";
import { Navigate, useNavigate } from "react-router-dom";
import { BookingCode, useBookingCodeStore } from "../../../service/http/axios-client/Appointment/BookingCode";


// export default function ProfilePopper(  ) {
//   const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
//   const navigate = useNavigate();
//   const handleClick = (event: React.MouseEvent<HTMLElement>) => {
//     setAnchorEl(anchorEl ? null : event.currentTarget);
//   };

//   const open = Boolean(anchorEl);
//   const id = open ? "simple-popper" : undefined;

//   const showBookingCode = () => {
//     BookingCode()
//     navigate('/UserBillPage')
//   }
//   const setIsLoggedIn = useAuthStore((state) => state.setIsLoggedIn); 
//   // const LogOut = () =>{
//   //   handleLogOut();
//   //   setIsLoggedIn(false)
//   //   localStorage.clear();
//   //   navigate('/')

//   // } 

//   const LogOut = () => {
//     handleLogOut(); // Perform any logout-related operations
//     setIsLoggedIn(false); // Update isLoggedIn state
//     localStorage.removeItem("token"); // Clear the token from localStorage
//     navigate('/'); // Redirect to the home page or login page
//   };
  
//   return (
//     <div>
//       <TriggerButton aria-describedby={id} type="button" onClick={handleClick}>
//         <PersonIcon />
//       </TriggerButton>
//       <Popper id={id} open={open} anchorEl={anchorEl}>
//         <StyledPopperDiv>
//         <div className="Profile-popper-content2">
           
//               <h3>Khunmaek@gmail.com</h3>
//             </div>
//           <div className="Profile-popper">
//             <div className="Profile-popper-content1">
//               <div className="Profile-popper-imgbox">
//                 <img src={LOGO} alt="My Image" />
//               </div>
//             </div>
//             <div className="Profile-popper-content2">
//               <h1>Khumaek Khotyotha</h1>
//             </div>
           
//             <div className="Profile-popper-content3"> 
//             <button  className="Profile-popper-content3-button  border1" style={{color:"white"}} onClick={showBookingCode} >ລະຫັດຂອງທ່ານ</button>


//             <button className="Profile-popper-content3-button border2"  onClick={LogOut}>ອອກຈາກລະບົບ</button>

//             </div>
//           </div>
//         </StyledPopperDiv>
//       </Popper>
//     </div>
//   );
// }











import { getUserInfo, useUserProfileStore } from "../../../service/http/axios-client/Login-axios";
import { useEffect, useState } from "react";


interface UserData {
  cus_email: string;
  cus_username: string;
  emp_name: string;
  emp_photo: string;
}


export default function ProfilePopper( ) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [userDataLoaded, setUserDataLoaded] = React.useState(false);

  //Access to get profile user data by zustand
  const userProfileData = useUserProfileStore(state => state.userData); 
 
  useEffect(() => {
    // Check if user data exists in local storage
    const userDataFromLocalStorage = localStorage.getItem('userData');
    if (userDataFromLocalStorage) {
      // Update Zustand store with user data from local storage
      useUserProfileStore.getState().setUserData(JSON.parse(userDataFromLocalStorage));
      setUserDataLoaded(true);
    }
  }, []);


  const navigate = useNavigate();
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popper" : undefined;

  //get cusID
  const [cusId] = useState<string | undefined>(userProfileData?.cus_id);
  

  //get appId by zustand 
  const bookingCodes = useBookingCodeStore((state) => state.bookingCodes);



  //handle click to see App id
  const showBookingCode = () => {
    if(cusId)
    BookingCode(cusId)
    navigate(`/UserBillPage`)
  }

  const setIsLoggedIn = useAuthStore((state) => state.setIsLoggedIn); 
  const LogOut = () => {
    handleLogOut(); // Perform any logout-related operations
    setIsLoggedIn(false); // Update isLoggedIn state
    localStorage.removeItem("token"); // Clear the token from localStorage
    navigate('/'); // Redirect to the home page or login page
  };  



  return (
    <div>
      <TriggerButton aria-describedby={id} type="button" onClick={handleClick}>
        <PersonIcon />
      </TriggerButton>
      <Popper id={id} open={open} anchorEl={anchorEl}>
        <StyledPopperDiv>
        <div className="Profile-popper-content2">
              <h3>{userProfileData?.cus_email  }</h3>
            </div>
          <div className="Profile-popper">
            <div className="Profile-popper-content1">
              <div className="Profile-popper-imgbox">
                <img src={LOGO} alt="My Image" />
              </div>
            </div>
            <div className="Profile-popper-content2">
              <h1>{userProfileData?.cus_username}</h1>
            </div>         
            <div className="Profile-popper-content3"> 
            <button  className="Profile-popper-content3-button  border1" style={{color:"white"}} onClick={showBookingCode} >ລະຫັດຂອງທ່ານ</button>
            <button className="Profile-popper-content3-button border2"  onClick={LogOut}>ອອກຈາກລະບົບ</button>
            </div>
          </div>
        </StyledPopperDiv>
      </Popper>
    </div>
  );
}












 

const TriggerButton = styled("button")(
  ({ theme }) => `
    font-family: 'IBM Plex Sans', sans-serif;
    font-weight: 600;
    font-size: 0.875rem;
    line-height: 1.5;
    background-color: red;
    padding: 8px 16px;
    border-radius: 8px;
    color: white;
    transition: all 150ms ease;
    cursor: pointer;
    border: 1px solid red;
    box-shadow: 0 2px 1px ${
      theme.palette.mode === "dark"
        ? "rgba(0, 0, 0, 0.5)"
        : "rgba(45, 45, 60, 0.2)"
    }, inset 0 1.5px 1px red, inset 0 -2px 1px red;
  
  
 
   
  
  `
);

const StyledPopperDiv = styled("div")(
  ({ theme }) => css`
    background-color: #302e2e;
    border-radius: 10px;
    border: 1px solid #302e2e;
    box-shadow: ${theme.palette.mode === "dark"
      ? `0px 4px 8px rgb(0 0 0 / 0.7)`
      : `0px 4px 8px rgb(0 0 0 / 0.1)`};
    padding: 0.75rem;
    font-size: 0.875rem;
    font-family: "IBM Plex Sans", sans-serif;
    width: 300px;
    height: 350px;
    font-weight: 500;
    opacity: 1;
    margin-top: 30px;
    margin-right: 20px;
  `
);
