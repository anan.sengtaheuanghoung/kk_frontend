import axios from "axios"
 
interface UsecusData {
  username: string;
  email: string;
  phonenumber: string;
  password: string;
}

export const SaveCus = (username: string, email: string, phonenumber: string, password: string,onSuccess: () => void,
onUnsuccess: () => void,) => {
   const cusData: UsecusData = {
    username: username,
    email: email,
    phonenumber: phonenumber,
    password: password,
  };

   axios
    .post("http://localhost:8080/management/signup", cusData ,{        
      })  
    .then((response) => {
       console.log("Customer signup successful");
        console.log(response.data); 
        onSuccess();

    })
    .catch((err) => {

      console.error("Error during customer signup:", err.message);
      onUnsuccess();
 
    });
};





interface UpdateCustomerData {
    username: string;
    email: string;
    phonenumber: string;
    password: string;
  }
  
  export const EditCus = (
    customerId: string, // Pass the customer ID as a parameter
    username: string,
    email: string,
    phoneNumber: string,
    password: string,
    onSuccess: () => void,
  onUnsuccess: () => void,
  ) => {
    const cusData: UpdateCustomerData = {
      username: username,
      email: email,
      phonenumber: phoneNumber,
      password: password,
    };
  
    axios
      .put(
        `http://localhost:8080/management/customer/${customerId}`, // Use correct endpoint with ID
        cusData,
        {
          
        }
      )
      .then((response) => {
        console.log("Edit successful:", response.data);
        onSuccess();

      })
      .catch((err) => {
        console.error("Error during EditCus:", err.message);
        onUnsuccess();
      });
  };
 

export const DeleteCus = (customerId: string, onSuccess: () => void,) => {
  axios
    .delete(`http://localhost:8080/management/customer/${customerId}`, {
      headers: {
        "Content-Type": "application/json", 
      },
    })
    .then((response) => {
      console.log("Customer deleted successfully:", response.data);
      onSuccess();

    })
    .catch((err) => {
      console.error("Error during DeleteCus:", err.message);
    });
};
  


