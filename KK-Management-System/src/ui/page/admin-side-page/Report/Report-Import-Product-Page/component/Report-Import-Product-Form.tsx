import "./Report-Import-Product-Form.css";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers-pro";
import { AdapterDayjs } from "@mui/x-date-pickers-pro/AdapterDayjs";
import { DateRangePicker } from "@mui/x-date-pickers-pro/DateRangePicker";
import {
  Button,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  debounce,
} from "@mui/material";
import React, { useCallback, useState } from "react";
import dayjs, { Dayjs } from "dayjs";


interface ImportFormProps {
  onSearch: (value: string) => void;
  onDateChange: (startDate: Dayjs | null, endDate: Dayjs | null) => void;
}


function ReportImportProductForm({ onSearch, onDateChange }: ImportFormProps) {
  //handle search by text
  const [searchValue, setSearchValue] = useState("");
  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setSearchValue(value);
    onSearch(value);
    debouncedSearch;
  };

  //handle date Range picker
  const [startDate, setStartDate] = useState<Dayjs | null>(null);
  const [endDate, setEndDate] = useState<Dayjs | null>(null);

  const debouncedSearch = useCallback(
    debounce((value) => onSearch(value), 300),
    []
  );
  const handleStartDateChange = (date: Dayjs | null) => {
    setStartDate(date);
    onDateChange(date, endDate);
  };
  const handleEndDateChange = (date: Dayjs | null) => {
    setEndDate(date);
    onDateChange(startDate, date);
  };

  
  return (
    <div>
      <div className="ImportProduct-Form">
        <div className="ImportProduct-title">
          <p>ລາຍງານນໍາເຂົ້າສິນຄ້າ </p>
        </div>
        <div className="ImportProduct-input">
          <div className="ImportProduct-grid1 ">
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={["DatePicker"]}>
                <DatePicker
                  label="ຈາກວັນທີ"
                  value={startDate}
                  onChange={handleStartDateChange}
                  format="DD/MM/YYYY"
                />
              </DemoContainer>
            </LocalizationProvider>
            _
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={["DatePicker"]}>
                <DatePicker
                  label="ເຖິງວັນທີ"
                  value={endDate}
                  onChange={handleEndDateChange}
                  format="DD/MM/YYYY"
                />
              </DemoContainer>
            </LocalizationProvider>
          </div>
          <div className="ImportProduct-grid2 ">
            <div className="ImportProduct-grid2-content ">
              <p> ຄົ້ນຫາ:</p>

              <TextField
                value={searchValue}
                onChange={handleSearchChange}
              ></TextField>
            </div>
            <div className="ImportProduct-grid2-content "></div>
          </div>
          <div className="ImportProduct-grid3">
            {/* <Button>ຕົກລົງ</Button> */}
          </div>
        </div>
      </div>
    </div>
  );
}
export default ReportImportProductForm;
