import './Sup-Form.css'
import TextField from "@mui/material/TextField";
import { Button } from "@mui/material";
import { SaveSupplier,EditSupplier,DeleteSupplier } from '../../../../../service/http/axios-client/Admin-Side/Supplier-Management';
import { useEffect, useState } from 'react';
import { SaveCompleteSnackbar, useSaveButtonAlert } from '../../../../style/Alert/AdminSide-Alert/SaveButtonAlert';
import {ErrorSaving, useErrorSaving } from '../../../../style/Alert/AdminSide-Alert/ErrorSaving';
import { useSupStore } from './Sup-Display';
import {EditSuccess, useEditSuccess } from '../../../../style/Alert/AdminSide-Alert/EditSuccessful';
import {ErrorEdit, useErrorEdit } from '../../../../style/Alert/AdminSide-Alert/ErrorEditing';
import {DeleteSuccess, useDeleteSuccess } from '../../../../style/Alert/AdminSide-Alert/DeleteSuccessful';

function SupForm () {
     //take state from zustand
  const { selectedData, setSelectedData } = useSupStore();
    //state for storing the value of supForm
    const [supId,setSupId] = useState("");
    const [supName,setSupname] =useState ("");
    const [supPnumber,setSupPnumber]=useState ("");

    //call out to use the alert 
    const setopenSuccessCreateAccount = useSaveButtonAlert((state) => state.setopenSuccessCreateAccount);
    const setUnsuccessOpen = useErrorSaving((state) => state.setUnsuccessOpen);
    const setopenEditSuccess = useEditSuccess((state) => state.setopenEditSuccess);
    const setErrorEdit = useErrorEdit((state) => state.setErrorEdit);
    const setopenDeleteSuccess = useDeleteSuccess((state) => state.setopenDeleteSuccess);


    //save button
    const handleSave = () =>{
      SaveSupplier( 
        supName,
        supPnumber,
        () =>{
          setSelectedData({
            sup_id: '',
            sup_name: '',
            sup_pnumber: ''
          });
          setopenSuccessCreateAccount(true);  
        },
        ()=>{
          setUnsuccessOpen(true);  

        },
      );
      console.log(supName)
      console.log(supPnumber)

    }



  //EDIT BUTTON
  const handleEdit = () => {
    EditSupplier(
      supId,
      supName,
        supPnumber,
      () => {
        setSelectedData({
          sup_id: '',
          sup_name: '',
          sup_pnumber: ''
        });
        setopenEditSuccess(true);  
      },
      () => {
        setErrorEdit(true);  
      },
    );
  };

   //DELETE BUTTON
   const handleDelete = () => {
    DeleteSupplier(supId,
      () => {
        setSelectedData({
          sup_id: '',
          sup_name: '',
          sup_pnumber: ''
        });
        setopenDeleteSuccess(true);  
      },
    );

     
  };

  //CLEAR BUTTON
  const handleClear = () => {
    setSelectedData({
      sup_id: '',
      sup_name: '',
      sup_pnumber: ''
    });
   };

    //HANDLDE MAP DATA FROM TABLE ROW
   useEffect(() => {
    console.log("Selected to see pic data :", selectedData);
    setSupId(selectedData.sup_id);
    setSupname(selectedData.sup_name);
    setSupPnumber(selectedData.sup_pnumber);

  }, [selectedData]);

    return(
        <div className="SupForm">
        <div className="SupForm-title">
          <h1>ຈັດການຜູ້ສະໜອງ</h1>
        </div>
        <div className="SupForm-input">
            {/* <div className="SupForm-input-detail SupForm-input-detail-margin ">
              <p>ລະຫັດຜູ້ສະໜອງ:</p>
              <TextField value={supId} disabled> </TextField>
            </div> */}
          <div className="SupForm-input-detail SupForm-input-detail-margin">
            <p>ຊື່ຜູ້ສະໜອງ:</p>
            <TextField value={supName}   onChange={(e) => setSupname(e.target.value)}> </TextField>
          </div>
          <div className="SupForm-input-detail">
            <p>ເບີໂທ:</p>
            <TextField  value={supPnumber} onChange={(e) => setSupPnumber(e.target.value)}> </TextField>
          </div>
        </div>
        <div className="SupManageForm-button">
          <Button onClick={handleSave}>ບັນທຶກ</Button>
          <Button onClick={handleEdit}>ເເກ້ໄຂ</Button>
          <Button onClick={handleClear} >ລ້າງ</Button>
          <Button onClick={handleDelete}>ລົບ</Button>
        </div>
        <SaveCompleteSnackbar/>
        <ErrorSaving/>
        <EditSuccess/>
        <ErrorEdit/>
        <DeleteSuccess/>
      </div>
    )
}
export default SupForm