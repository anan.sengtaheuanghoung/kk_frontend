
// import {
//   Table,
//   TableBody,
//   TableCell,
//   TableContainer,
//   TableHead,
//   TableRow,
//   TextField,
//   styled,
//   tableCellClasses,
// } from "@mui/material";
// import React, { ReactNode, useEffect, useState } from "react";
// import axios from "axios";
// import { create } from "zustand";
// import { getStoredToken } from "../../../../../service/http/axios-client/Login-axios";

// const StyledTableRow = styled(TableRow)(({ theme }) => ({
//   "&:nth-of-type(odd)": {
//     backgroundColor: theme.palette.action.hover,
//   },
//   // hide last border
//   "&:last-child td, &:last-child th": {
//     border: 0,
//   },
// }));

// const StyledTableCell = styled(TableCell)(({ theme }) => ({
//   [`&.${tableCellClasses.head}`]: {
//     backgroundColor: theme.palette.common.black,
//     color: theme.palette.common.white,
//   },
//   [`&.${tableCellClasses.body}`]: {
//     fontSize: 14,
//   },
// }));

// interface useProductState {
//   selectedData: {
//     proid: string;
//     proName: string;
//     proPrice:string;
//     proCost:string;
//     selectProtype:string;
//     selectBranch:string;
//     proPic: File | null;

//   };
//   setSelectedData: (data: {
//     proid: string;
//     proName: string;
//     proPrice:string;
//     proCost:string;
//     selectProtype:string;
//     selectBranch:string;
//     proPic: File | null;
//   }) => void;
// }

// // Zustand store
// export const useProductStore = create<useProductState>((set) => ({
//   selectedData: {
//     proid: "",
//     proName: "",
//     proPrice: "",
//     proCost: "",
//     selectProtype: "",
//     selectBranch: "",
//     proPic: null,
    
//   },
//   setSelectedData: (data: any) => set({ selectedData: data }),
// }));

// interface Column {
//   id: keyof Data;
//   label: string;
//   minWidth?: number;
//   align?: "left" | "center" | "right";
// }

// const columns: readonly Column[] = [
//   { id: "pro_id", label: "ລະຫັດ" },
//   { id: "pro_name", label: "ຊື່ສິນຄ້າ" },
//   { id: "pro_price", label: "ລາຄາ" },
//   { id: "pro_cost", label: "ລາຄາຕົ້ນທຶນ" },
//   { id: "proType_id", label: "ລະຫັດປະເພດສິນຄ້າ" },
//   { id: "branch_id", label: "ສາຂາ" },
//   { id: "pro_photo", label: "ຮູບ" },
// ];


// interface Data {
//  pro_id:string;
//  pro_name:string;
//  pro_price:string;
//  pro_cost:string;
//  pro_photo:string;
//  proType_id:string;
//  branch_id:string;
//  branch_name:string;
//  proType_name:string;
// }

// function ProManageDisplay() {
//   const [rows, setRows] = useState<Data[]>([]);
//   const { selectedData, setSelectedData } = useProductStore();
//   const [searchQuery, setSearchQuery] = useState("");

//   //table row click function
//   const handleRowClick = (row: {
//     pro_id:string;
//  pro_name:string;
//  pro_price:string;
//  pro_cost:string;
//  pro_photo:any;
//  proType_id:string;
//  branch_id:string;
//   }) => {
//     setSelectedData({
//       proid: row.pro_id,
//       proName: row.pro_name,
//       proPrice: row.pro_price,
//       proCost: row.pro_cost,
//       selectProtype: row.proType_id,
//       selectBranch: row.branch_id,
//       proPic: row.pro_photo,
//     });
    
//   };

//   //fect data to show on table
//   const fetchProductData = () => {
//     const authToken = getStoredToken();
//     const headers = {
//       Authorization: `Bearer ${authToken}`,
//     };
//     axios
//       .get("http://localhost:8080/product_and_service", {
//         headers:headers,
//       })
//       .then((response) => {
//          setRows(response.data);
//        })
//       .catch((error) => {
//         console.error("Error fetching product data:", error);
//       });
//   };

//   useEffect(() => {
//     fetchProductData();
//   }, [selectedData]);

//   const filteredRows = rows.filter(
//     (row) =>
//       typeof row.pro_name === "string" &&
//       row.pro_name.toLowerCase().includes(searchQuery.toLowerCase())
//   );

//   return (
//     <div className="EmpManageDisplay">
//       <div className="EmpManageDisplay-grid1">
//         <p>ຄົ້ນຫາ:</p>
//         <TextField
//           className="EmpManageDisplay-grid1-textfield"
//           value={searchQuery}
//           onChange={(e) => setSearchQuery(e.target.value)}
//           sx={{
//             "& fieldset": { border: 'none' },
//           }}
//         />
//       </div>
//       <div className="EmpManageDisplay-grid2">
//         <TableContainer sx={{ maxHeight: 340 }}>
//           <Table stickyHeader aria-label="sticky table">
//             <TableHead>
//               <TableRow>
//                 {columns.map((column) => (
//                   <TableCell
//                     key={column.id}
//                     align={column.align}
//                     style={{
//                       minWidth: column.minWidth,
//                       fontSize: 24,
//                       width: 100,
//                       fontFamily: "phetsarath ot",
//                     }}
//                   >
//                     {column.label}
//                   </TableCell>
//                 ))}
//               </TableRow>
//             </TableHead>
//             <TableBody>
//               {filteredRows.map((row) => (
//                 <StyledTableRow
//                   key={row.pro_id}
//                   onClick={() => handleRowClick(row)}
//                 >
//                   <StyledTableCell style={{ fontSize: 17 }}>
//                     {row.pro_id}
//                   </StyledTableCell>
//                   <StyledTableCell style={{ fontSize: 17 }}>
//                     {row.pro_name}
//                   </StyledTableCell>
//                   <StyledTableCell style={{ fontSize: 17 }}>
//                     {row.pro_price}
//                   </StyledTableCell>
//                   <StyledTableCell style={{ fontSize: 17 }}>
//                     {row.pro_cost}
//                   </StyledTableCell>
//                   <StyledTableCell style={{ fontSize: 17 }}  data-proType-id={row.proType_id}>
//                     {row.proType_name}
//                   </StyledTableCell>
//                   <StyledTableCell style={{ fontSize: 17 }}  data-branch-id={row.branch_id}>
//                   {row.branch_name }
//                   </StyledTableCell>                 
//                   <StyledTableCell style={{ fontSize: 17 }}>
//                     {typeof row.pro_photo === "string" ? (
//                       <img
//                         src={row.pro_photo}
//                         alt="Employee Photo"
//                         style={{
//                           height: "50px",
//                           width: "50px",
//                           objectFit: "cover",
//                         }}
//                       />
//                     ) : (
//                       "No Photo"
//                     )}
//                   </StyledTableCell>
                   
//                 </StyledTableRow>
//               ))}
//             </TableBody>
//           </Table>
//         </TableContainer>
//       </div>
//     </div>
//   );
// }

// export default ProManageDisplay;


import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  styled,
  tableCellClasses,
} from "@mui/material";
import React, {   useEffect, useState } from "react";
import axios from "axios";
import { create } from "zustand";
import { getStoredToken } from "../../../../../service/http/axios-client/Login-axios";
 
const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

interface useProductState {
  selectedData: {
    proid: string;
    proName: string;
    proPrice:string;
    proCost:string;
    selectProtype:string;
    selectBranch:string;
    proPic: File | null;
    amount:string;
    status:string;

  };
  setSelectedData: (data: {
    proid: string;
    proName: string;
    proPrice:string;
    proCost:string;
    selectProtype:string;
    selectBranch:string;
    proPic: File | null;
    amount:string;
    status:string;

  }) => void;
}

// Zustand store
export const useProductStore = create<useProductState>((set) => ({
  selectedData: {
    proid: "",
    proName: "",
    proPrice: "",
    proCost: "",
    selectProtype: "",
    selectBranch: "",
    proPic: null,
    amount:"",
    status:"",
    
  },
  setSelectedData: (data: any) => set({ selectedData: data }),
}));

interface Column {
  id: keyof Data;
  label: string;
  minWidth?: number;
  align?: "left" | "center" | "right";
}

const columns: readonly Column[] = [
  { id: "pro_id", label: "ລະຫັດ" },
  { id: "pro_name", label: "ຊື່ສິນຄ້າ" },
  { id: "pro_price", label: "ລາຄາ" },
  { id: "pro_cost", label: "ລາຄາຕົ້ນທຶນ" },
  { id: "proType_id", label: "ປະເພດສິນຄ້າ " },
  { id: "amount", label: "ຈໍານວນ " },
  { id: "pro_photo", label: "ສະຖານະ" },
  { id: "status", label: "ຮູບ"},

];


interface Data {
 pro_id:string;
 pro_name:string;
 pro_price:string;
 pro_cost:string;
 pro_photo:string;
 proType_id:string;
 branch_id:string;
 branch_name:string;
 proType_name:string;
 amount:string;
 status:string;
}

function ProManageDisplay() {
  const [rows, setRows] = useState<Data[]>([]);
  const { selectedData, setSelectedData } = useProductStore();
  const [searchQuery, setSearchQuery] = useState("");

  //table row click function
  const handleRowClick = (row: {
  pro_id:string;
  pro_name:string;
  pro_price:string;
  pro_cost:string;
  pro_photo:any;
  proType_id:string;
  branch_id:string;
  amount:string;
  status:string;

  }) => {
    setSelectedData({
      proid: row.pro_id,
      proName: row.pro_name,
      proPrice: row.pro_price,
      proCost: row.pro_cost,
      selectProtype: row.proType_id,
      selectBranch: row.branch_id,
      proPic: row.pro_photo,
      amount:row.amount,
      status:row.status,

    });
    
  };

  //fect data to show on table
  const fetchProductData = () => {
    const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
    axios
      .get("http://localhost:8080/product_and_services", {
        headers:headers,
      })
      .then((response) => {
         setRows(response.data);
       })
      .catch((error) => {
        console.error("Error fetching product data:", error);
      });
  };
  useEffect(() => {
    fetchProductData();
  }, [selectedData]);

 
  //fil
  const filteredRows = rows.filter(
    (row) =>
      typeof row.pro_name === "string" &&
      row.pro_name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <div className="EmpManageDisplay">
      <div className="EmpManageDisplay-grid1">
        <p>ຄົ້ນຫາ:</p>
        <TextField
          className="EmpManageDisplay-grid1-textfield"
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
          sx={{
            "& fieldset": { border: 'none' },
          }}
        />
      </div>
      <div className="EmpManageDisplay-grid2">
        <TableContainer sx={{ maxHeight: 340 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{
                      minWidth: column.minWidth,
                      fontSize: 24,
                      width: 100,
                      fontFamily: "phetsarath ot",
                    }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredRows.map((row) => (
                <StyledTableRow
                  key={row.pro_id}
                  onClick={() => handleRowClick(row)}
                >
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.pro_id}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.pro_name}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17  }}>
                    {row.pro_price}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.pro_cost}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}  data-proType-id={row.proType_id}>
                    {row.proType_name}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}  >
                  {row.amount}
                  </StyledTableCell>   
                  <StyledTableCell style={{ fontSize: 17 }}  >
                  {row.status}
                  </StyledTableCell>              
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {typeof row.pro_photo === "string" ? (
                      <img
                        src={row.pro_photo}
                        alt="Employee Photo"
                        style={{
                          height: "50px",
                          width: "50px",
                          objectFit: "cover",
                        }}
                      />
                    ) : (
                      "No Photo"
                    )}
                  </StyledTableCell>
                   
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}

export default ProManageDisplay;
