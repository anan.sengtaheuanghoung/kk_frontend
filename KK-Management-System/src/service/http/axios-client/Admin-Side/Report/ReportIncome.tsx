import axios from "axios";
import { getStoredToken } from "../../Login-axios";

//this route is use for fetching Order Report
interface ImportData {
    imp_id:string,
    imp_date:string,
    imp_total:string,
    branch_name:string,
}
   const authToken = getStoredToken();
   const headers = {
    Authorization: `Bearer ${authToken}`,
};



export const fetchImportReport = async (): Promise<ImportData[]> => {
  try {
      const response = await axios.get("http://localhost:8080/imports/report", {
         headers: headers,
       });
       console.log( response.data.report) 
       return response.data.report as ImportData[];  
     } catch (error) {
       console.error("Error fetching Import Report:", error);
       throw error;
     }
   };





//report bill
export const fecthImportBill = async (imp_id: string) => {
  try {
    const response = await axios.get(`http://localhost:8080/imports/bill/${imp_id}`, {
      headers: headers,
    });
    
    const total_price = response.data.bill[0].total_price;
    console.log("----------", total_price);
    
    return response.data.bill;
  } catch (error) {
    console.error("Error during fetching import bill:", error);
    throw error;
  }
};