
import {
    
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    styled,
    tableCellClasses,
  } from "@mui/material";
   import React, {  ReactNode, useEffect, useState } from "react";
  import axios from "axios";
  import { create } from "zustand";
  import './ProType-display.css'
  
  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));
  
  
  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
  
  
  
  
  interface useProTypeStoreState {
    selectedData: {
      proType_id: string,
      proType_name: string,
     
    
    },
    setSelectedData: (data: {
        proType_id: string,
        proType_name: string,
  
    }) => void;
  }
  
  
  
  
  // Zustand store
  export const useProTypeStore = create<useProTypeStoreState>((set) => ({
    selectedData: {
    proType_id: "",
      proType_name: "",
     
    },
    setSelectedData: (data: any) => set({ selectedData: data }),
  }));
  
  
  
  interface Column {
    id: keyof Data;  
    label: string;
    minWidth?: number;
    align?: "left" | "center" | "right"; 
  }
  
  
  const columns: readonly Column[] = [
    { id: "ID", label: "ລະຫັດ" },
    { id: "Name", label: "ຊື່" },
   
  ];
  
  interface Data {
    
     
    proType_id:string;
    proType_name:string;
    ID: number;
    Name: string;
     
    
   
  }
  
  function  ProTypedisplay() {
    const [rows, setRows] = useState<Data[]>([]);
    const { selectedData, setSelectedData } = useProTypeStore();
    const [searchQuery, setSearchQuery] = useState("");
   
  
    const handleRowClick = (row: {proType_id:any;proType_name:any }) => {
      setSelectedData({
        proType_id: row.proType_id,
        proType_name: row.proType_name,
       });
  
    };

    //fect data to show on table
  const fetchProtypeData = () => {
   
    axios
      .get("http://localhost:8080/product_types", {
      })
      .then((response) => {
        setRows(response.data);
      })
      .catch((error) => {
        console.error("Error fetching protypedata data:", error);
      });
  };
  useEffect(() => {
    fetchProtypeData();
  }, [selectedData]);

  const filteredRows = rows.filter(
    (row) =>
      typeof row.proType_name === "string" &&
      row.proType_name.toLowerCase().includes(searchQuery.toLowerCase())
  );
   
    return (
      <div className="ProtypeManageDisplay">
        <div className="ProtypeManageDisplay-grid1">
          <p>ຄົ້ນຫາ:</p>
          <TextField className="ProtypeManageDisplay-grid1-textfield" 
           value={searchQuery}
           onChange={(e) => setSearchQuery(e.target.value)}
           sx={{
            "& fieldset": { border: 'none' },
          }}
           />
  
        </div>
        <div className="ProtypeManageDisplay-grid2">
          <TableContainer sx={{ maxHeight: 340 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{
                        minWidth: column.minWidth,
                        fontSize: 24,
                        width: 70,
                        fontFamily:"phetsarath ot",
                      }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
              {filteredRows.map((row) => (
  
                   <StyledTableRow     key={row.proType_id} onClick={() => handleRowClick(row)}>
                    <StyledTableCell  style={{fontSize:17}} >{row.proType_id}</StyledTableCell>
                    <StyledTableCell style={{fontSize:17}}>{row.proType_name}</StyledTableCell>
       
                  </StyledTableRow>
                  ))}  
  
               </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
    );
  }
  
  
  export default ProTypedisplay