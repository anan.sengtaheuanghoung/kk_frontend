import * as React from "react";
import clsx from "clsx";
import { styled, css } from "@mui/system";
import { Modal as BaseModal } from "@mui/base/Modal";
import "./ChooseBarber.css";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { useState } from "react";
import { fecthEmployeeName } from "../../../../service/http/axios-client/Appointment/CusAppointment";

interface Employee {
  emp_id: string;
  emp_name: string;
}

interface ChooseBarberProps {
  branchId: string;
  branchName: string;
  onSelect: (branchId: string, branchName: string, empName: string,empId:string) => void;
}

export default function ChooseBarber({
  branchId,
  branchName,
  onSelect,
}: ChooseBarberProps) {
  const [open, setOpen] = useState(false);
  const handleClose = () => setOpen(false);
  const [employees, setEmployees] = useState<Employee[]>([]);

  //this code use to mapping emp_name
  const handleChange = async () => {
    setOpen(true);
    try {
      const response = await fecthEmployeeName(branchId);
      setEmployees(response.employees);
    } catch (error) {
      console.error("Error fetching employee names:", error);
    }
  };

  
  const handleEmployeeSelect = (
    branchId: string,
    branchName: string,
    empName: string,
    empId:string
  ) => {
    onSelect(branchId, branchName, empName,empId);
    handleClose();
  };

  return (
    <div>
      <TriggerButton type="button" onClick={handleChange}>
        ເລືອກ
      </TriggerButton>
      <Modal
        aria-labelledby="unstyled-modal-title"
        aria-describedby="unstyled-modal-description"
        open={open}
        onClose={handleClose}
        slots={{ backdrop: StyledBackdrop }}
      >
        <ModalContent sx={{ width: 430, height: 500, overflow: "auto" }}>
          <div className="choosebarber-content">
            <div className="choosebarber-content-title">ເລືອກຊ່າງຕັດຜົມ</div>
            <div className="choosebarber-selection">
              {employees.map((employee, index) => (
                <div key={index} className="choosebarber-selection-style">
                  <div className="choosebarber-selection-style1"> 
                  <AccountCircleIcon />
                  <p>{employee.emp_name}</p>
                  </div>
                  <div className="choosebarber-selection-style2">
                  <button
                    className="choosebarber-selection-button"
                    onClick={() =>
                      handleEmployeeSelect(
                        branchId,
                        branchName,
                        employee.emp_name,
                        employee.emp_id
                      )
                    }
                  >
                    ເລືອກ
                  </button>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </ModalContent>
      </Modal>
    </div>
  );
}

const Backdrop = React.forwardRef<
  HTMLDivElement,
  { open?: boolean; className: string }
>((props, ref) => {
  const { open, className, ...other } = props;
  return (
    <div
      className={clsx({ "base-Backdrop-open": open }, className)}
      ref={ref}
      {...other}
    />
  );
});

const blue = {
  200: "#99CCFF",
  300: "#66B2FF",
  400: "#3399FF",
  500: "#007FFF",
  600: "#0072E5",
  700: "#0066CC",
};

const grey = {
  50: "#F3F6F9",
  100: "#E5EAF2",
  200: "#DAE2ED",
  300: "#C7D0DD",
  400: "#B0B8C4",
  500: "#9DA8B7",
  600: "#6B7A90",
  700: "#434D5B",
  800: "#303740",
  900: "#1C2025",
};

const Modal = styled(BaseModal)`
  position: fixed;
  z-index: 1300;
  inset: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledBackdrop = styled(Backdrop)`
  z-index: -1;
  position: fixed;
  inset: 0;
  background-color: rgb(0 0 0 / 0.5);
  -webkit-tap-highlight-color: transparent;
`;

const ModalContent = styled("div")(
  ({ theme }) => css`
    font-family: "IBM Plex Sans", sans-serif;
    font-weight: 500;
    text-align: start;
    position: relative;
    display: flex;
    flex-direction: column;
    gap: 8px;
    overflow: hidden;
    background-color: ${theme.palette.mode === "dark" ? grey[900] : "white"};
    border-radius: 8px;
    box-shadow: 0 4px 12px
      ${theme.palette.mode === "dark" ? "rgb(0 0 0 / 0.5)" : "rgb(0 0 0 / 0.2)"};
    padding: 24px;
    color: ${theme.palette.mode === "dark" ? grey[50] : grey[900]};

    & .modal-title {
      margin: 0;
      line-height: 1.5rem;
      margin-bottom: 8px;
    }

    & .modal-description {
      margin: 0;
      line-height: 1.5rem;
      font-weight: 400;
      color: ${theme.palette.mode === "dark" ? grey[400] : grey[800]};
      margin-bottom: 4px;
    }
  `
);

const TriggerButton = styled("button")(
  ({ theme }) => css`
    font-family: "phetsarath ot";
    font-weight: 400;
    font-size: 20px;
    line-height: 1.5;
    padding: 8px 16px;
    border-radius: 8px;
    transition: all 150ms ease;
    cursor: pointer;
    background: ${theme.palette.mode === "dark" ? grey[900] : "red"};
    color: "white";
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);

    &:hover {
      background: "red";
      border-color: ${theme.palette.mode === "dark" ? grey[600] : grey[300]};
    }

    &:active {
      background: ${theme.palette.mode === "dark" ? grey[700] : grey[100]};
    }

    &:focus-visible {
      box-shadow: 0 0 0 4px
        ${theme.palette.mode === "dark" ? blue[300] : blue[200]};
      outline: none;
    }
  `
);
