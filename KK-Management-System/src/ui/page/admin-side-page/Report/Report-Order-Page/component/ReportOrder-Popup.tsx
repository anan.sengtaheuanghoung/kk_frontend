 
import React, { useState, useRef } from 'react';
import { styled, css } from '@mui/system';
import { Modal as BaseModal } from '@mui/base/Modal';
import { useReactToPrint } from 'react-to-print';
import {
  Backdrop,
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';
import './ReportOrder-Popup.css'
import { fecthOrderBill } from '../../../../../../service/http/axios-client/Admin-Side/Report/ReportOrder';
import LOGO from "../../../../../../image/LOGO.png";

const blue = {
  200: "#99CCFF",
  300: "#66B2FF",
  400: "#3399FF",
  500: "#007FFF",
  600: "#0072E5",
  700: "#0066CC",
};

const grey = {
  50: "#F3F6F9",
  100: "#E5EAF2",
  200: "#DAE2ED",
  300: "#C7D0DD",
  400: "#B0B8C4",
  500: "#9DA8B7",
  600: "#6B7A90",
  700: "#434D5B",
  800: "#303740",
  900: "#1C2025",
};

const Modal = styled(BaseModal)`
  position: fixed;
  z-index: 1300;
  inset: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledBackdrop = styled(Backdrop)`
  z-index: -1;
  position: fixed;
  inset: 0;
  background-color: rgb(0 0 0 / 0.5);
  -webkit-tap-highlight-color: transparent;
`;

const ModalContent = styled("div")(
  ({ theme }) => css`
    font-family: "IBM Plex Sans", sans-serif;
    font-weight: 500;
    text-align: start;
    position: relative;
    display: flex;
    flex-direction: column;
    gap: 8px;
    width: 900px;
    height: auto;
    overflow: auto;
    background-color: ${theme.palette.mode === "dark" ? grey[900] : "white"};
    border-radius: 8px;
    box-shadow: 0 4px 12px
      ${theme.palette.mode === "dark" ? "rgb(0 0 0 / 0.5)" : "rgb(0 0 0 / 0.2)"};
    padding: 24px;
    color: ${theme.palette.mode === "dark" ? grey[50] : grey[900]};

    & .modal-title {
      margin: 0;
      line-height: 1.5rem;
      margin-bottom: 8px;
    }

    & .modal-description {
      margin: 0;
      line-height: 1.5rem;
      font-weight: 400;
      color: ${theme.palette.mode === "dark" ? grey[400] : grey[800]};
      margin-bottom: 4px;
    }
  `
);

const TriggerButton = styled("button")(
  ({ theme }) => css`
    font-family: "phetsarath ot";
    font-weight: 400;
    font-size: 18px;
    line-height: 1.5;
    padding: 8px 16px;
    border-radius: 8px;
    transition: all 150ms ease;
    cursor: pointer;
    background: #D9D9D9;
    color: black;
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);

    &:hover {
      background: "grey";
      border-color: ${theme.palette.mode === "dark" ? grey[600] : grey[300]};
    }

    &:active {
      background: ${theme.palette.mode === "dark" ? grey[700] : grey[100]};
    }

    &:focus-visible {
      box-shadow: 0 0 0 4px
        ${theme.palette.mode === "dark" ? blue[300] : blue[200]};
      outline: none;
    }
  `
);
 
 

interface RecieveData {
  orderID:string,
  supName:string,
  branchName:string,
  Date:string,
}
interface OrderListBill{
  order_id:string,
  sup_name:string,
  pro_id:string,
  pro_name:string,
  amount:string,
  branch_name:string,
  order_date:string,
  proType_name:string,
}


export default function ReportOrderPopUP({orderID,supName,branchName,Date}:RecieveData) {
  const [open, setOpen] = useState(false);
  const contentRef = useRef<HTMLDivElement | null>(null);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [loading, setLoading] = useState<boolean>(true);


  const handlePrint = useReactToPrint({
    content: () => contentRef.current,
    documentTitle: 'Report Order', // Optional title for the printout
  });


  const [OrderBill, setOrderBill] = useState<OrderListBill[]>([]);
  //this code use to mapping emp_name
  const handleChange = async () => {
    setOpen(true);
    try {
      const response = await fecthOrderBill(orderID);
      setOrderBill(response);
      setLoading(false)
    } catch (error) {
      console.error("Error fetching employee names:", error);
    }
  };


  return (
    <div>
      <TriggerButton onClick={handleChange}>ເລືອກ</TriggerButton> {/* Opens the modal */}
      <Modal
        open={open}
        onClose={handleClose}
        slots={{ backdrop: StyledBackdrop }}
      >
         
        <ModalContent  >
          <div className="Report-Order-Maindiv"  ref={contentRef}>
            <div className="order-popup-title">
              <img  style={{width:200,height:200}}src={LOGO}/>
            </div>
            <div className="order-popup-title">
              <p>ໃບບິນສັ່ງຊື້</p>
            </div>
            <div className="order-popup-showID">
              <div className="popup-showid-grid1">
              <p className="">ຊື່ຮ້ານ: KICKS & KUTS </p>

                <p>ລະຫັດ:{orderID} </p>
                 <p>ຜູ້ສະໜອງ: {supName}</p>
                 
              </div>
              <div className="popup-showid-grid2">
              <p className="">ສາຂາ:{branchName} </p>           
              <p className="">ວັນທີ: {Date}</p>


              </div>
            </div>
            <div className="order-popup-showList">
              <TableContainer  component={Paper}>
                <Table
                  sx={{ minWidth: 560}}
                  size="small"
                  aria-label="a dense table"
                  className='reportOrder-tableContainer'              
                >
                  <TableHead>
                    <TableRow>
                      <TableCell className="TableHead-style">ລໍາດັບ</TableCell>
                      <TableCell className="TableHead-style">ຊື່ສິນຄ້າ</TableCell>
                      <TableCell className="TableHead-style">ຈໍານວນ</TableCell>
                      <TableCell className="TableHead-style">ປະເພດສິນຄ້າ</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody className='reportOrder-table-body'>  
                  {loading ? (
                      <TableRow className="Loading">
                        <p>ກະລຸນາເລືອກວັນທີເພື່ອສະເເດງລາຍຮັບ !!!</p>
                      </TableRow>
                    ) : (OrderBill.map((order,index) => (
                      <TableRow
                        key={order.order_id}
                        sx={{
                          "&:last-child td, &:last-child th": { border: 0 },
                        }}
                      >
                        <TableCell className='reportOrder-deatail' align="center" >{index + 1}</TableCell>
                        <TableCell className='reportOrder-deatail' align="center">{order.pro_name}</TableCell>
                        <TableCell className='reportOrder-deatail' align="center">{order.amount}</TableCell>
                        <TableCell className='reportOrder-deatail' align="center">{order.proType_name}</TableCell>
                      </TableRow>
                    )))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
             
          </div>
          <div className='order-print-button'>
          <Button onClick={handlePrint}>Print</Button>  
          </div>
        </ModalContent>
       

      </Modal>
    </div>
  );
}


