import axios from "axios";
import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";

interface RoleCheckerProps {
    element: JSX.Element;
    adminOnly?: boolean;
    employee?: boolean;
    ceo?: boolean;

    user?:boolean;
    token: string;  
  }
  
export const RoleChecker: React.FC<RoleCheckerProps> = ({ element, adminOnly, employee, token,user,ceo }) => {
    const [role, setRole] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState(true); // Add loading state
  
    useEffect(() => {
      const token = localStorage.getItem('token');
      if (!token) {
        console.error("No token found");
        return;  
      }
      
      setIsLoading(true);  
  
      axios.get("http://localhost:8080/login/user", {
        headers: {
          Authorization: ` ${token}`  
        }
      })
        .then(response => {
          const userRole = response.data.role;
          setRole(userRole);
           
        })
        .catch(error => {
          console.error("Error fetching user role:", error);
          
        })
        .finally(() => {
          setIsLoading(false);  
        });
    }, [token]);  
  
  if (isLoading) {
    // Display a loading indicator while fetching role
    return <div>Loading...</div>;
  } else if (
    (role === "admin" && adminOnly) ||
    (role === "employee" &&  employee) ||
    (role === "ceo" &&  ceo) ||
    (role === "user" &&  user) ||  
    (role !== "admin" && role !== "employee" && !adminOnly && !employee)  
  ) {
    // console.log("Is admin?", role === "admin");
    // console.log("Is employee?", role === "employee");
    // console.log("Admin only route?", adminOnly);
    // console.log("Employee only route?", employee); 
    // console.log("Rendering element?", element);
    return element;
  } else {
    if (role === "admin"  ){
    return <Navigate to="/AdminDashBoardPage" />;
    } 
    else if(role === "employee"){
      return <Navigate to="/BookingPage" />;
    }
    else if(role === "ceo"){
      return  <Navigate to="/BranchManagePage" />;
    }
  }
  };


  