    // import "./Input-Product-Form.css";
    // import { Button } from "@mui/material";
    // import TextField from "@mui/material/TextField";

    // function InputProductForm() {
    // return (
    //     <div>
    //     <div className="InputProductForm">
    //         <div className="InputProductForm-title">
    //          </div>
    //         <div className="InputProductForm-input">
    //         <div className="InputProductForm-input-grid1 InputProductForm-grid-direction">
    //             <div className="InputProductForm-input-detail ">
    //             <p>ລະຫັດ:</p>

    //             <div className="InputProductForm-textfield ">
    //                 <TextField></TextField>
    //             </div>
    //             </div>
    //             <div className="InputProductForm-input-detail">
    //             <p>ຊື່:</p>
    //             <div className="InputProductForm-textfield  ">
    //                 <TextField></TextField>
    //             </div>
    //             </div>
    //             <div className="InputProductForm-input-detail">
    //             <p>ລາຄາ:</p>
    //             <div className="Cus-textfield">
    //                 <TextField></TextField>
    //             </div>
    //             </div>
    //         </div>
    //         <div className="InputProductForm-input-grid2  InputProductForm-grid-direction">
    //             <div className="InputProductForm-input-detail">
    //             <p>ຈໍານວນ:</p>
    //             <div className="InputProductForm-textfield  ">
    //                 <TextField></TextField>
    //             </div>
    //             </div>
    //             <div className="InputProductForm-input-detail">
    //             <p>ປະເພດຂອງສິນຄ້າ:</p>
    //             <div className="InputProductForm-textfield ">
    //                 <TextField></TextField>
    //             </div>
    //             </div>
                
    //         </div>
    //         </div>
    //         <div className="InputProductForm-button">
    //         <Button>ຕົກລົງ</Button>
             
    //         </div>
    //     </div>
    //     </div>
    // );
    // }
    // export default InputProductForm;
    // import React, { useEffect, useState } from "react";
    
    // import Table from "@mui/material/Table";
    // import TableBody from "@mui/material/TableBody";
    // import TableCell from "@mui/material/TableCell";
    // import TableContainer from "@mui/material/TableContainer";
    // import TableHead from "@mui/material/TableHead";
    // import TableRow from "@mui/material/TableRow";
    // import { Button, Paper } from "@mui/material";
    // import { FetchOrderDetail } from "../../../../../service/http/axios-client/Admin-Side/Order-and-Import/Import";
    // import './Input-Product-Form.css'
    // import {  useParams } from "react-router-dom";
    // interface Product {
    //   pro_id: number;
    //   pro_name: string;
    //   pro_cost: number;
    //   amount: number;
    //   order_id:string;
    // }
    // interface Params {
    //     order_id:string;
 
    //    }
    
    // function InputProductForm({
    //     onProductSelect,
    //   }: {
    //     onProductSelect: (product: Product) => void;
    //   }) {
    //   const [productList, setProductList] = useState<Product[]>([]);
    //   const [loading, setLoading] = useState<boolean>(true);
    
    //   const fetchProducts = async () => {
    //     try {
    //       const products: Product[] = await FetchOrderDetail();
    //       setProductList(products);
    //       setLoading(false);
    //     } catch (error) {
    //       console.error("Error fetching product data:", error);
    //     }
    //   };
    
    //   useEffect(() => {
    //     fetchProducts();
    //   }, []);



    //   const {order_id =""} = useParams<Params>();
    //   const getOrderid =  order_id || "";

      
    //   return (
    //     <div className="inputProduct-maindiv">
    //       <p>ສິນຄ້າໃນຄັ່ງ</p>
    //       <div className="inputProductDisplay">
    //         <Paper>
    //           <TableContainer sx={{ maxHeight: 350 }}>
    //             <Table stickyHeader aria-label="sticky table">
    //               <TableHead>
    //                 <TableRow>
    //                   <TableCell className="inputProduct-TableCell-Style">
    //                     ລະຫັດ
    //                   </TableCell>
    //                   <TableCell className="inputProduct-TableCell-Style">
    //                     ຊື່
    //                   </TableCell>
    //                   <TableCell className="inputProduct-TableCell-Style">
    //                     ລາຄາ
    //                   </TableCell>
    //                   <TableCell className="inputProduct-TableCell-Style">
    //                     ຈໍານວນ
    //                   </TableCell>
    //                   <TableCell className="inputProduct-TableCell-Style"></TableCell>
    //                 </TableRow>
    //               </TableHead>
    //               <TableBody>
    //                 {loading ? (
    //                   <TableRow>
    //                     <TableCell colSpan={5}>Loading...</TableCell>
    //                   </TableRow>
    //                 ) : (
    //                   productList.map((product) => (
    //                     <TableRow key={product.pro_id}>
    //                       <TableCell className="inputProduct-ProductList">
    //                         {product.pro_id}
    //                       </TableCell>
    //                       <TableCell className="inputProduct-ProductList">
    //                         {product.pro_name}
    //                       </TableCell>
    //                       <TableCell className="inputProduct-ProductList">
    //                         {product.pro_cost}
    //                       </TableCell>
    //                       <TableCell className="inputProduct-ProductList">
    //                         {product.amount}
    //                       </TableCell>
    //                       <TableCell>
    //                         <div className="inputProduct-button-orderProduct">
    //                           <Button onClick={() => onProductSelect(product)}>
    //                             ເລືອກ
    //                           </Button>
    //                         </div>
    //                       </TableCell>
    //                     </TableRow>
    //                   ))
    //                 )}
    //               </TableBody>
    //             </Table>
    //           </TableContainer>
    //         </Paper>
    //       </div>
    //     </div>
    //   );
    // }
    
    // export default InputProductForm;
    

    import React, { useState, useEffect } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button, Paper } from "@mui/material";
import { FetchOrderDetail } from "../../../../../service/http/axios-client/Admin-Side/Order-and-Import/Import";
import './Input-Product-Form.css';
import { useParams } from "react-router-dom";

interface Product {
  pro_id: number;
  pro_name: string;
  pro_cost: number;
  amount: number;
  order_id: string;
}

interface Params {
    getOrderid: string;
  [key: string]: string | undefined;

}

function InputProductForm({
  onProductSelect,
}: {
  onProductSelect: (product: Product) => void;
}) {
  const [productList, setProductList] = useState<Product[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  const { getOrderid } = useParams<Params>();
   const fetchProducts = async (orderId: string) => {
    try {
      const products = await FetchOrderDetail(orderId);
      setProductList(products);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching product data:", error);
      setLoading(false); // Ensure loading is set to false in case of error
    }
  };


  useEffect(() => {
    if (getOrderid) {
      fetchProducts(getOrderid);
    }
  }, [getOrderid]);


  
  return (
    <div className="inputProduct-maindiv">
      <p>ສິນຄ້າທີ່ສັ່ງ</p>
      <div className="inputProductDisplay">
        <Paper>
          <TableContainer sx={{ maxHeight: 350 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell className="inputProduct-TableCell-Style">
                    ລະຫັດ
                  </TableCell>
                  <TableCell className="inputProduct-TableCell-Style">
                    ຊື່
                  </TableCell>
                  <TableCell className="inputProduct-TableCell-Style">
                    ລາຄາ
                  </TableCell>
                  <TableCell className="inputProduct-TableCell-Style">
                    ຈໍານວນ
                  </TableCell>
                  <TableCell className="inputProduct-TableCell-Style"></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {loading ? (
                  <TableRow>
                    <TableCell colSpan={5}>Loading...</TableCell>
                  </TableRow>
                ) : (
                  productList.map((product) => (
                    <TableRow key={product.pro_id}>
                      <TableCell className="inputProduct-ProductList">
                        {product.pro_id}
                      </TableCell>
                      <TableCell className="inputProduct-ProductList">
                        {product.pro_name}
                      </TableCell>
                      <TableCell className="inputProduct-ProductList">
                        {product.pro_cost}
                      </TableCell>
                      <TableCell className="inputProduct-ProductList">
                        {product.amount}
                      </TableCell>
                      <TableCell>
                        <div className="inputProduct-button-orderProduct">
                          <Button onClick={() => onProductSelect(product)}>
                            ເລືອກ
                          </Button>
                        </div>
                      </TableCell>
                    </TableRow>
                  ))
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </div>
    </div>
  );
}

export default InputProductForm;
