

import axios from "axios";
import { getAuthHeaders } from "../Login-axios";
 interface branchData {
    branch_name:string,
    branch_pnumber:string,
    branch_address:string,
}

//Save button
export const SaveBranch = (
    branch_name:string,
    branch_pnumber:string,
    branch_address:string,
  onSuccess: () => void,
  onError: (error: any) => void,
) => {

  const BranchData: branchData = {
        branch_name:branch_name,
        branch_pnumber:branch_pnumber,
        branch_address:branch_address,
  };

 
 


  axios
    .post("http://localhost:8080/management/branch",BranchData, {
      headers:getAuthHeaders(),
    }
   )
    .then((response) => {
      console.log("branch added successfully");
      console.log(response.data);
      onSuccess();  
    })
    .catch((error) => {
      console.error("Error adding Branch:", error);
      onError(error);  
    });
};











interface branchEditData {
    branch_id:string;
    branch_name:string,
    branch_pnumber:string,
    branch_address:string,
}
   //Save button
  export const EditBranch = (
    branch_id:string,
    branch_name:string,
    branch_pnumber:string,
    branch_address:string,
    onSuccess: () => void,
    onError: (error: any) => void,
  ) => {
    const branchEditData: branchEditData = {
        branch_id:branch_id,
        branch_name:branch_name,
        branch_pnumber:branch_pnumber,
        branch_address:branch_address,
    };
 

    axios
      .put(`http://localhost:8080/management/branch/${branch_id}`, branchEditData, {
        headers:getAuthHeaders(),
      })
      .then((response) => {
        console.log("branch Edit successfully");
        console.log(response.data);
        onSuccess();  
      })
      .catch((error) => {
        console.error("Error editting branch:", error);
        onError(error);  
      });
  };



    //DeleteProtype
export const DeleteBranch = (branch_id:string,
    onSuccess: () => void,
  ) => {
 
    axios
      .delete(`http://localhost:8080/management/branch/${branch_id}`, {
        headers:getAuthHeaders(),
      })
      .then((response) => {
        console.log("Deletion successful");
        console.log(response.data);
        onSuccess();
      })
      .catch((err) => {
        console.error("Error during deletion:", err);
      });
  };