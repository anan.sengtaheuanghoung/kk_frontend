import axios from "axios";
import { getStoredToken } from "../../Login-axios";


 

//this route is use for fetching Order Report
interface IncomeData {
   branch_name: string;
  pro_id: string;
  pro_name: string;
  amount: string;
  pro_price: string;
  total_price: string;
  ser_total: string;
   ser_date: string;


}
   const authToken = getStoredToken();
   const headers = {
    Authorization: `Bearer ${authToken}`,
};
export const fetchIncomeReport = async (startDate: string, endDate: string): Promise<IncomeData[]> => {
    try {
        const response = await axios.get("http://localhost:8080/incomes/report", {
            headers: headers,
            params: {
                startDate: startDate,
                endDate: endDate
            }
        });
        console.log("dwadadawd",response.data.report);
        return response.data.report;  // Accessing the report data correctly
    } catch (error) {
        console.error("Error fetching income report:", error);
        throw error;
    }
   };
