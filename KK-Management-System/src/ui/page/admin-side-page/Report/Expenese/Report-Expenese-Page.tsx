import './Report-Expenese-Page.css'
import SideBar from '../../../../style/AdminPage-sideBar'
import ExpeneseForm from './component/Expenese-form'
import ExpeneseDisplay from './component/Expenese-Display'
function Expenesepage () {
    return(
        <div className="Expenesepage">
        <div className="Expenesepage-first-grid">
          <SideBar />
        </div>
        <div className="Expenesepage-second-grid">
        <ExpeneseForm/>
        <ExpeneseDisplay/>

      </div>
      </div>

    )
}
export default Expenesepage