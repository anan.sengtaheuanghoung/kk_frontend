 





// export default AdminAppointmentForm;
import './Admin-Datetime-Appointment-form.css'
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DateCalendar } from "@mui/x-date-pickers/DateCalendar";
import { Button, MenuItem, Select, SelectChangeEvent, TextField } from "@mui/material";
import { useState, useEffect } from "react";
import { Params, useNavigate, useParams } from "react-router-dom";
import dayjs, { Dayjs } from "dayjs";
import { fetchEmployeeData } from "../../../../../service/http/axios-client/Admin-Side/Service/Appointment-by-admin";
import { AppointmentByAdmin } from '../../../../../service/http/axios-client/Admin-Side/Service/Appointment-by-admin';
import { getStoredToken } from '../../../../../service/http/axios-client/Login-axios';
import axios from 'axios';
import { useAppSuccessAlert, ApponitmentSuccessSnackbar } from '../../../../style/Alert/AdminSide-Alert/AppointmentSuccessfull';
import { ErrorApp, useErrorApp } from '../../../../style/Alert/AdminSide-Alert/AppointmentError';

interface Employee {
  emp_id: string;
  emp_name: string | number;
  branch_id:string;
}

function AdminAppointmentForm() {

  const [getTime, setTime] = useState<string>("");
  const [getDate, setDate] = useState<string>("");
  const [bookedTimes, setBookedTimes] = useState<string[]>([]);
  const [userName, setUsername]= useState("");
  const navigate = useNavigate( )

  //call out alert to use
  const setopenSuccessCreateAccount = useAppSuccessAlert(
    (state) => state.setopenSuccessCreateAccount
  );
  const setUnsuccessOpen = useErrorApp(
    (state) => state.setUnsuccessOpen
  );
  const handleTimeValue = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    const selectedTime = (event.currentTarget as HTMLButtonElement).value;
    if (!bookedTimes.includes(selectedTime)) {
      setTime(selectedTime);
    }
  };

  //set disable date
  const isPastDate = (date: Dayjs): boolean => {
    const today = dayjs();
    return date.isBefore(today, "day");
  };

  //fect employee to select
  const [employeesName, setEmployees] = useState<Employee[]>([]);
  const [takeEmpID, settakeEmpID] = useState<string[]>([]);
  const [takebranchID, settakebranchID] = useState<string[]>([]);

   useEffect(() => {
    const fetchEmployeesName = async () => {
      try {
        const employeedata = await fetchEmployeeData();
        setEmployees(employeedata); // Correctly set the state
      } catch (error) {
        console.error("Error fetching branches:", error);
      }
    };

    fetchEmployeesName();
  }, []);

  //handle Appointment 
  const hanleAppointment = () =>{
    AppointmentByAdmin(
      userName,
      selectedEmployeeName,
      getDate,
      getTime, 
      () => {
        setopenSuccessCreateAccount(true);
        setTimeout(() => {
          navigate("/BookingSevice");
        }, 1000);  
      },
      () => {
        setUnsuccessOpen(true)
      },
    );
   }

  //take branchId , empID and empName from select 
  const handleEmployeeChange = (event: SelectChangeEvent<string>) => {
    const selectedEmpName = event.target.value;
    const selectedEmp = employeesName.find(emp => emp.emp_name === selectedEmpName);
    
    if (selectedEmp) {
      setSelectedEmployeeName(selectedEmpName);
      settakeEmpID([selectedEmp.emp_id]); // Update takeEmpID with the emp_id of the selected employee
      settakebranchID([selectedEmp.branch_id]); // Update takeBranchID with the branch_id of the selected employee
    }
  };
  const [selectedEmployeeName, setSelectedEmployeeName] = useState<string>(""); // Define state variable for selected employee name




  // Fetch booked times for the selected date, employee, and branch
  useEffect(() => {
    const fetchBookedTimes = async () => {
      const authToken = getStoredToken();
      const headers = {
        Authorization: `Bearer ${authToken}`,
      };
      if (getDate) {
        try {
          const response = await axios.get(
            "http://localhost:8080/booked-times",
            {
              headers: headers,
              params: {
                date: getDate,
                employeeId: takeEmpID,
                branchId :takebranchID,
              },
            }
          );
          setBookedTimes(response.data.bookedTimes);
        } catch (error) {
          console.error("Error fetching booked times:", error);
        }
      }
    };
    fetchBookedTimes();
  }, [getDate, takeEmpID,takebranchID ]);



  //Disable the past time 
  const formatTime = (time: string): string => {
    const [hour, minute] = time.split(':').map(Number);
    const ampm = hour >= 12 ? 'PM' : 'AM';
    const formattedHour = hour % 12 === 0 ? 12 : hour % 12;
    return `${formattedHour}:${minute.toString().padStart(2, '0')} ${ampm}`;
  };
  
  const isTimePast = (time: string, selectedDate: string): boolean => {
    const currentDate = dayjs();
    const buttonDate = dayjs(selectedDate).set('hour', parseInt(time.split(':')[0])).set('minute', parseInt(time.split(':')[1])).set('second', parseInt(time.split(':')[2]));
    if (buttonDate.isAfter(currentDate)) {
      // If the selected date is in the future, no need to disable
      return false;
    }
    return true;
  };

  return (
    <div className="admin-carlendar-main-div">
      <div className="admin-carlendar-selection-detail">
        <div className="admin-carlendar-selection-content-first">
          <div className="admin-carlendar-deatail-first-div">
            <div className="CalendarSelection-first-div-textfield">
              <p>ຊື່:</p>
              <TextField  onChange={(e) => setUsername(e.target.value)}></TextField>
            </div>
            <div className="CalendarSelection-first-div">
              <p>ຊ່າງຕັດຜົມ:</p>
              <Select
                className="CalendarSelection-first-div-select"
                value={selectedEmployeeName}
                onChange={handleEmployeeChange}
              >
                {Array.isArray(employeesName) && employeesName.length > 0 ? (
                  employeesName.map((employee) => (
                    <MenuItem key={employee.emp_id} value={employee.emp_name}>
                      {employee.emp_name}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>No branches available</MenuItem>
                )}
              </Select>
            </div>
          </div>
          <div className="admin-carlendar-calendar-div">
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DateCalendar
                views={["year", "month", "day"]}
                className="admin-carlendar-datecalendar"
                onChange={(date) => {
                  const formattedDate = date?.format("YYYY-MM-DD");
                  if (formattedDate && !isPastDate(dayjs(formattedDate))) {
                    setDate(formattedDate);
                  }
                }}
                shouldDisableDate={isPastDate}
              />
            </LocalizationProvider>
          </div>
          <div className="admin-carlendar-deatail-third-div">
            <div className="admin-carlendar-third-div-title">
              <h3>ເລືອກເວລາຕັດຜົມ</h3>
            </div>

            <div className="admin-carlendar-third-div-grid">
              <div className="admin-carlendar-grid-style">
                <p>ເຊົ້າ</p>
                <div className="admin-carlendar-morning-grid-style-detail button-Style">
                {["8:00:00", "9:00:00", "10:00:00", "11:00:00"].map(
                    (time) => {
                      const isDisabled = bookedTimes.includes(time) || isTimePast(time,getDate);
                      return (
                        <Button
                          key={time}
                          variant="outlined"
                          value={time}
                          onClick={handleTimeValue}
                          disabled={isDisabled} // Disable button if time is booked or current time has passed
                          className={isDisabled ? "button-booked" : ""} // Conditionally apply booked class
                        >
                          {formatTime(time)}
                        </Button>
                      );
                    }
                  )}
                </div>
              </div>

              <div className="admin-carlendar-grid-style">
                <p>ສວາຍ</p>
                <div className="admin-carlendar-afternoon-grid-style-detail admin-carlendar-button-Style">
                {[
                    "12:00:00",
                    "13:00:00",
                    "14:00:00",
                    "15:00:00",
                    "16:00:00",
                    "17:00:00",
                  ].map((time) => {
                    const isDisabled = bookedTimes.includes(time) || isTimePast(time,getDate);
                    return (
                      <Button
                        key={time}
                        variant="outlined"
                        value={time}
                        onClick={handleTimeValue}
                        disabled={isDisabled} // Disable button if time is booked or current time has passed
                        className={isDisabled ? "button-booked" : ""} // Conditionally apply booked class
                      >
                        {formatTime(time)}
                      </Button>
                    );
                  })}
                </div>
              </div>
              <div className="admin-carlendar-grid-style">
                <p>ເເລງ</p>
                <div className="admin-carlendar-evening-grid-style-detail button-Style">
                  {["18:00:00", "19:00:00", "20:00:00"].map((time) => {
                    const isDisabled = bookedTimes.includes(time) || isTimePast(time,getDate);
                    return (
                      <Button
                        key={time}
                        variant="outlined"
                        value={time}
                        onClick={handleTimeValue}
                        disabled={isDisabled} // Disable button if time is booked or current time has passed
                        className={isDisabled ? "button-booked" : ""} // Conditionally apply booked class
                      >
                        {formatTime(time)}
                      </Button>
                    );
                  })}
                </div>
              </div>
            </div>
            <div className="admin-carlendar-input-name-div">
              <Button
                variant="outlined"
                disabled={!getTime || bookedTimes.includes(getTime)}
                onClick={hanleAppointment }
              >
                ຕົກລົງ
              </Button>
            </div>
          </div>
        </div>
      </div>
      <ApponitmentSuccessSnackbar/>
      <ErrorApp/>
    </div>
  );
}

export default AdminAppointmentForm;
