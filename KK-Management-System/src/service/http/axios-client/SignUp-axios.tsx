import axios from "axios";

export const handleUserSignUp = (username: string, email: string, phonenumber: string, password: string, onNavigate: () => void, onSuccess: () => void) => {
  const userData = {
    username: username,
    email: email,
    password: password,
    phonenumber: phonenumber,
  };

  axios
    .post("http://localhost:8080/signup", userData)
    .then(() => {
      console.log("Sign up successful");
      onNavigate();
      onSuccess()
      
    })
    .catch((err) => {
      console.error("Error during sign up:", err);
    });
};
