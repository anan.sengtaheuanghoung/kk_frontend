import Header from "../../style/Header"
import BarberSelection from "./component/Barber-Selection"
import "./Barber-Seletion-Page.css"

function BarberSelectionPage () {
    return(
        <div>
            <Header isLoggedIn={false}/>
            <div className="barber-selection-div">
            <BarberSelection/>
            </div>

        </div>
    )
}
export default BarberSelectionPage