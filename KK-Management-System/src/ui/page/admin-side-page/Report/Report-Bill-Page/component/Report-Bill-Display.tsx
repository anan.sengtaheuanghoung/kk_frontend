import './Report-Bill-Display.css'
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button } from "@mui/material";
import ReportBillPopUP from './Report-Bill-PopUp';
import { useEffect, useState } from 'react';
import { fetchServiceReport } from '../../../../../../service/http/axios-client/Admin-Side/Report/Service';
import dayjs, { Dayjs } from 'dayjs';


interface ServiceReportData {
  ser_id:string,
  admin_name:string,
  employee_name:string,
  branch_name:string,
  ser_date:string,
  ser_total:string,
}


interface ReportServieDisplayProps {
  searchData: string; 
  startDate: Dayjs | null;
  endDate: Dayjs | null;
}

function ReportBillDisplay ({ searchData, startDate, endDate }:ReportServieDisplayProps) {
   //store state
   const [ReportService, setServiceImport] = useState<ServiceReportData[]>([]);
   const [loading, setLoading] = useState<boolean>(true);
 
   const fetchServiceImport = async () => {
     try {
       const ServicetList: ServiceReportData[] = await fetchServiceReport();
       setServiceImport(ServicetList);
       setLoading(false);
     } catch (error) {
       console.error("Error fetching product data:", error);
     }
   };
  
   useEffect(() => {
    fetchServiceImport();
   }, []);
 


    const filteredServiceList = ReportService.filter(service => {
    const serviceDate = dayjs(service.ser_date, 'DD/MM/YYYY');
    const searchDataLower = searchData ? searchData.toLowerCase() : ''; // Guard against null searchData
  
    // Convert to string and handle null values
    const serviceIdLower = service.ser_id ? service.ser_id.toLowerCase() : '';
    const serviceDateLower = service.ser_date ? service.ser_date.toLowerCase() : '';
    const serviceTotalString = service.ser_total ? service.ser_total.toString() : '';
    const serviceTotalLower = serviceTotalString.toLowerCase();
    const branchNameLower = service.branch_name ? service.branch_name.toLowerCase() : '';
    const employeeNameLower = service.employee_name ? service.employee_name.toLowerCase() : '';
    const adminNameLower = service.admin_name ? service.admin_name.toLowerCase() : '';
  
    const matchesSearchQuery =
      serviceIdLower.includes(searchDataLower) ||
      serviceDateLower.includes(searchDataLower) ||
      serviceTotalLower.includes(searchDataLower) ||
      branchNameLower.includes(searchDataLower) ||
      employeeNameLower.includes(searchDataLower) ||
      adminNameLower.includes(searchDataLower);
  
    const matchesDateRange =
      (!startDate || serviceDate.isSame(startDate, 'day') || serviceDate.isAfter(startDate)) &&
      (!endDate || serviceDate.isSame(endDate, 'day') || serviceDate.isBefore(endDate, 'day'));
  
    return matchesSearchQuery && matchesDateRange;
  });
  
  
  
 
    return(
        <div className="ReportBillDisplay">
        <TableContainer  sx={{ maxHeight: 530 }} >         
          <Table>
            <TableHead>
              <TableRow>
              <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ລະຫັດ</TableCell>
                 <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ຊ່າງຕັດຜົມ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ສາຂາ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ວັນທີ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ລາຄາລວມ</TableCell>
                <TableCell align="right"> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={5}>Loading...</TableCell>
              </TableRow>
            ) : (
              filteredServiceList.map((service) => (
                <TableRow key={service.ser_id}>
                  <TableCell className="Map-ReportOrder">
                    {service.ser_id}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {service.employee_name}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {service.branch_name}
                  </TableCell>  
                  <TableCell className="Map-ReportOrder">
                    {service.ser_date}
                  </TableCell>  
                  <TableCell className="Map-ReportOrder">
                    {service.ser_total}
                  </TableCell>              
                  <TableCell className="Map-ReportOrder"  >
                    <ReportBillPopUP  SerId={service.ser_id}  employeeName={service.employee_name} branchName={service.branch_name} Date={service.ser_date}/>
                  </TableCell>
                </TableRow>
              ))
            )}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    )
}
export default ReportBillDisplay