import axios from "axios";
import { getStoredToken } from "../../Login-axios";


//this route is use for fetching Order Report
interface ExpenseData {
    pro_id:string,
    pro_name:string,
    amount:string,
    pro_cost:string,
    total_price:string,
    imp_total:string,
    branch_name:string,
 
}
   const authToken = getStoredToken();
   const headers = {
    Authorization: `Bearer ${authToken}`,
};
export const fetchExpenseReport = async (startDate: string, endDate: string): Promise<ExpenseData[]> => {
    try {
        const response = await axios.get("http://localhost:8080/expenses/report", {
            headers: headers,
            params: {
                startDate: startDate,
                endDate: endDate
            }
        });
         return response.data.report;  
    } catch (error) {
        console.error("Error fetching Expense report:", error);
        throw error;
    }
   };