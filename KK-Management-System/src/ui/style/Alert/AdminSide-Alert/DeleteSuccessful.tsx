import { Alert, Snackbar, Stack } from "@mui/material";
import { create } from "zustand";

interface useDeleteType {
    openDeleteSuccess: boolean;
    setopenDeleteSuccess: (open: boolean) => void;
}

export const useDeleteSuccess = create<useDeleteType>((set) => ({
    openDeleteSuccess: false,
    setopenDeleteSuccess: (openDeleteSuccess) => set({ openDeleteSuccess }),
}));

export const DeleteSuccess = () => {
  const { openDeleteSuccess , setopenDeleteSuccess } = useDeleteSuccess();

  const handleClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setopenDeleteSuccess(false);
  };

  return (
    <div>
      <Snackbar
        open={openDeleteSuccess}
        autoHideDuration={1200}
        onClose={handleClose}
        message="This Snackbar will be dismissed in 5 seconds."
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Stack sx={{ width: "100%" }} spacing={2}>
          <Alert
            severity="success"
            style={{ width: 200, fontSize: 20, fontFamily: "phetsarath ot" }}
          >
            ລົບສໍາເຫຼັດ
          </Alert>
        </Stack>
      </Snackbar>
    </div>
  );
};
