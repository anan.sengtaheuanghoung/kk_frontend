import "./Selected-Input-Product.css";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button } from "@mui/material";
function createData(
  id: number,
  name: number,
  date: number,
  time: number,
  barber: String,
  status: string,
  Branch: string
) {
  return { id, name, date, time, barber, status, Branch };
}

const rows = [
  createData(1, 159, 6.0, 24, "A", "haha", "haha"),
  createData(2, 237, 9.0, 37, "b", "haha", "haha"),
  createData(3, 262, 16.0, 24, "c", "haha", "haha"),
  createData(4, 305, 3.7, 67, "d", "haha", "haha"),
  createData(2, 237, 9.0, 37, "b", "haha", "haha"),
  createData(3, 262, 16.0, 24, "c", "haha", "haha"),
  createData(4, 305, 3.7, 67, "d", "haha", "haha"),
  createData(2, 237, 9.0, 37, "b", "haha", "haha"),
  createData(3, 262, 16.0, 24, "c", "haha", "haha"),
  createData(4, 305, 3.7, 67, "d", "haha", "haha"),
  createData(2, 237, 9.0, 37, "b", "haha", "haha"),
  createData(3, 262, 16.0, 24, "c", "haha", "haha"),
  createData(4, 305, 3.7, 67, "d", "haha", "haha"),
];
function SelectedProduct() {
  return (
    <div className="SelectedProduct">
      <TableContainer sx={{ maxHeight: 410 }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ລະຫັດ</TableCell>
              <TableCell style={{ fontSize: 20 }} align="center">
                ຊື່
              </TableCell>
              <TableCell style={{ fontSize: 20 }} align="center">
                ລາຄາ
              </TableCell>
              <TableCell style={{ fontSize: 20 }} align="center">
                ຈໍານວນ
              </TableCell>
              <TableCell style={{ fontSize: 20 }} align="center">
                ປະເພດຂອງສິນຄ້າ
              </TableCell>
              <TableCell style={{ fontSize: 20 }} align="center"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow>
                <TableCell component="th" scope="row">
                  {row.id}
                </TableCell>
                <TableCell align="center">{row.name}</TableCell>
                <TableCell align="center">{row.date}</TableCell>
                <TableCell align="center">{row.time}</TableCell>
                <TableCell align="center">{row.barber}</TableCell>
                <TableCell align="center">
                  <Button className="SelectedProduct-edit-button">ລົບ</Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <div className="select-input-product-button">
      <Button        
      >
        ຍືນຍັນ
      </Button>
      </div>
    </div>
  );
}
export default SelectedProduct;
