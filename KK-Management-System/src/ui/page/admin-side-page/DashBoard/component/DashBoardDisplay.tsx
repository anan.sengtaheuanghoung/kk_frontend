import "./DashBoardDisplay.css";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import approved from "../../../../../image/approved.png";
import readyStock from "../../../../../image/ready-stock.png";
import schedule from "../../../../../image/schedule.png";
import { Button } from "@material-ui/core";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import LOGO from "../../../../../image/LOGO.png";
import user from "../../../../../image/user.png";
import { useMapRowCountStore } from "../../Admin-Booking-Page/component/Admin-Booking-Display";
import { useEffect, useState } from "react";
import ReportBillPopUP from "../../Report/Report-Bill-Page/component/Report-Bill-PopUp";
import {
  fetchDashboardReport,
  fetchDashboardEmp,
  fetchIndexEmp,
  fetchIndexApp,
  fetchIndexStock
} from "../../../../../service/http/axios-client/Dashboard/Dashboard-Report";
import { useNavigate } from "react-router-dom";

interface Data {
  ser_id: string;
  cus_username: string;
  emp_name: string;
  ser_total: string;
}

interface empData {
  emp_name: string;
  emp_pnumber: string;
  emp_photo: string | null;
 
}
interface Indexcustomer {
  cus_id:string,
 }
interface IndexApp {
  app_id:string,
}
interface IndexStock {
  stock:string,
}
export default function DashBoardDisplay() {
 
  // Store state for reports
  const [ReportDashboard, setReportDashboard] = useState<Data[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const navigate = useNavigate()

  useEffect(() => {
  const fetchDashboard = async () => {
    try {
      const DashboardList: Data[] = await fetchDashboardReport();
      setReportDashboard(DashboardList);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching Dashboard data:", error);
      setLoading(false);
    }
  };
    fetchDashboard();
  }, []);

  
  // Store state for employee reports
  const [ReportEmpDashboard, setReportEmpDashboard] = useState<empData[]>([]);
  const [emploading, setEmpLoading] = useState<boolean>(true);
  useEffect(() => {
  const fetchEmp = async () => {
    try {
      const DashboardEmpList: empData[] = await fetchDashboardEmp();
      setReportEmpDashboard(DashboardEmpList);
      setEmpLoading(false);
    } catch (error) {
      console.error("Error fetching employee data:", error);
      setEmpLoading(false);
    }
  };
    fetchEmp();
  }, []);



  // Store state for customer index reports
  const [ReportIndexEmp, setReportIndexEmp] = useState<Indexcustomer[]>([]);
  const [Indexemploading, setIndexEmpLoading] = useState<boolean>(true);
  useEffect(() => {
  const fetchEmpIndex = async () => {
    try {
      const IndexEmp: Indexcustomer[] = await fetchIndexEmp();
      setReportIndexEmp(IndexEmp);
      setIndexEmpLoading(false);
    } catch (error) {
      console.error("Error fetching employee data:", error);
      setIndexEmpLoading(false);
    }
  };
  fetchEmpIndex();
  }, []);


  
  // Store state for appointment  index reports
  const [ReportIndexApp, setReportIndexApp] = useState<IndexApp[]>([]);
  const [IndexApploading, setAppEmpLoading] = useState<boolean>(true);
  useEffect(() => {
  const fetchApp = async () => {
    try {
      const IndexEmpList: IndexApp[] = await fetchIndexApp();
      setReportIndexApp(IndexEmpList);
      setAppEmpLoading(false);
    } catch (error) {
      console.error("Error fetching employee data:", error);
      setIndexEmpLoading(false);
    }
  };
    fetchApp();
  }, []);




  // Store state for appointment  index reports
  const [ReportIndexStock, setReportIndexStock] = useState<IndexStock[]>([]);
  const [IndexStockloading, setAppStockLoading] = useState<boolean>(true);
  useEffect(() => {
  const fetchStockIndex = async () => {
    try {
      const IndexStockList: IndexStock[] = await fetchIndexStock();
      setReportIndexStock(IndexStockList);
      setAppStockLoading(false);
    } catch (error) {
      console.error("Error fetching employee data:", error);
      setIndexEmpLoading(false);
    }
  };
  fetchStockIndex();
  }, []);


  
  return (
    <div className="DashBoardDisplay-maindiv">
      <div className="Appointment-div" onClick={() => navigate('/BookingSevice')}>
        <div className="Appointment-item"  >
        {IndexApploading ? (
          <p style={{ fontSize: "30px", margin: "0px" }}>ຍັງບໍ່ມີການຈອງ</p>
        ) : (
          ReportIndexApp.map((indexApp) => (
              <h1 style={{ fontSize: "90px", margin: "0px" }}  >{indexApp.app_id}</h1>
            ))
        )}
          <h5 style={{ fontSize: "25px", margin: "0px" }}>ຈອງ </h5>
        </div>
        <div className="Appointment-icon">
          <img src={schedule} alt="Schedule Icon" style={{ width: "100px", height: "100px" }} />
        </div>
      </div>
      <div className="stock-div" onClick={() => navigate('/StockPage')}> 
        <div className="stock-item">
        {IndexStockloading ? (
          <p style={{ fontSize: "30px", margin: "0px" }}>ຍັງບໍ່ມີສິນຄ້າໃກ້ໝົດ</p>
        ) : (
          ReportIndexStock.map((indexStock) => (
              <h1 style={{ fontSize: "90px", margin: "0px" }}  >{indexStock.stock}</h1>
            ))
        )}
          <h5 style={{ fontSize: "25px", margin: "0px" ,width:140}}>ສິນຄ້າໃກ້ໝົດ </h5>
        </div>
        <div className="stock-icon">
          <img src={readyStock} alt="Ready Stock Icon" style={{ width: "100px", height: "100px" }} />
        </div>
      </div>
      <div className="orders-div" >
        <div className="order-item">
        {Indexemploading ? (
          <p style={{ fontSize: "30px", margin: "0px" }}>ຍັງບໍ່ມີຜູ້ໃຊ້</p>
        ) : (
            ReportIndexEmp.map((indexcus) => (
              <h1 style={{ fontSize: "90px", margin: "0px" }}  >{indexcus.cus_id}</h1>
            ))
        )}
          <h5 style={{ fontSize: "25px", margin: "0px" }}>ຜູ້ໃຊ້ </h5>
        </div>
        <div className="order-icon">
          <img src={user} alt="User Icon" style={{ width: "80px", height: "80px" }} />
        </div>
      </div>
      <div className="payment-div">
        <div className="payment-title">
          <p style={{ fontSize: "30px" }}>ຈ່າຍລ້າສຸດ</p>
          <button className="payment-div-button" onClick={() => navigate('/ReportBillPage')}>ເບິ່ງທັງໝົດ</button>
        </div>
        <div className="table-list">
          <TableContainer sx={{ maxHeight: 400 }} >
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell style={{ fontSize: 23, fontFamily: "phetsarath ot" }} align="center">
                    ຊື່ລູກຄ້າ
                  </TableCell>
                  <TableCell style={{ fontSize: 23, fontFamily: "phetsarath ot" }} align="center">
                    ພະນັກງານ
                  </TableCell>
                  <TableCell style={{ fontSize: 23, fontFamily: "phetsarath ot" }} align="center">
                    ຈໍານວນທັງໝົດ
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {loading ? (
                  <TableRow>
                    <TableCell colSpan={4} align="center">Loading...</TableCell>
                  </TableRow>
                ) : (
                  ReportDashboard.map((dashboard) => (
                    <TableRow key={dashboard.ser_id}>
                      <TableCell className="Map-ReportOrder">{dashboard.cus_username}</TableCell>
                      <TableCell className="Map-ReportOrder">{dashboard.emp_name}</TableCell>
                      <TableCell className="Map-ReportOrder">{dashboard.ser_total}</TableCell>
                        {/* <TableCell align="center">
                          <button className="Report-Display-edit-button" onClick={() => navigate('')} >ເບິ່ງ</button>
                        </TableCell> */}
                    </TableRow>
                  ))
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
      <div className="barber-div">
        <div className="barber-title">
          <p style={{ fontSize: "30px", marginLeft: "10px" }}>ຊ່າງຕັດຜົມ</p>
        </div>
        <div className="employee-list">
        {emploading ? (
          <p>loading.......</p>
        ) : (
          ReportEmpDashboard.map((empDashboard) => (
            <div className="barber-display" key={empDashboard.emp_pnumber}>
              <div className="barber-display-profile">
                <div className="barber-display-profile-logo">
                  {empDashboard.emp_photo ? (
                    <img src={empDashboard.emp_photo} className="display-img" />
                  ) : (
                    <img src="path-to-default-photo" alt="Default Photo" style={{ width: "70px", height: "70px" }} /> // Add a default photo path
                  )}
                </div>
              </div>
              <div className="barber-display-detail">
                <p>{empDashboard.emp_name}</p>
                <p>{empDashboard.emp_pnumber}</p>
              </div>                          
            </div>
          ))
        )}
        </div>
      </div>
    </div>
  );
}
