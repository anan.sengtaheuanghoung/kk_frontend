import axios from "axios";
import { getStoredToken } from "../../Login-axios";

//this route is use for fetching Order Report
interface OrderReportData {
    order_id:string,
    order_date:string,
    sup_name:string,
    branch_name:string,
}
   const authToken = getStoredToken();
   const headers = {
    Authorization: `Bearer ${authToken}`,
};

export const fetchOrderReport = async (): Promise<OrderReportData[]> => {
     try {
       const response = await axios.get("http://localhost:8080/orders/report", {
         headers: headers,
       });
       return response.data.report as OrderReportData[];  
     } catch (error) {
       console.error("Error fetching Order Report:", error);
       throw error;
    }
};
   




//report bill
export const fecthOrderBill = async (order_id: string) => {
  console.log("--------------",order_id)
  try {
    const response = await axios.get(`http://localhost:8080/orders/bill/${order_id}`,{
      headers: headers,
    });
     return response.data.bill ; 

  } catch (error) {
    console.error("Error during fetching employee names:", error);
    throw error;
  }
};
