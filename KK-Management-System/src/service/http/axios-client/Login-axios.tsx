 

import axios from 'axios';
import { create } from 'zustand';

interface User {
  branch_id: string;
  emp_address: string;
  emp_email: string;
  emp_id: string;
  emp_name: string;
  emp_password: string;
  emp_photo: string;
  emp_pnumber: string;
  emp_status: string;
  emp_username: string;
}

interface LoginResponse {
  message: string;
  user: User;
  role: string;
  branchId: string;
  token: string;
}
interface UserData {
  cus_email: string;
  cus_username: string;
  emp_name: string;
  emp_photo: string;
  cus_id:string
  role : string;

}

interface UserState {
  userData: UserData | null;
  setUserData: (userData: UserData | null) => void;
}

export const useUserProfileStore = create<UserState>((set) => ({
  userData: null,
  setUserData: (userData) => set({ userData }),
}));
 

// Function to handle user login
export const handleUserLogin = (
  usernameOrEmail: string,
  password: string,
  onNavigate: () => void,
  getUserInfo: () => void,
  onError: (error: any) => void,
  onSuccess: (token: string, role: string, user: User, branchId: string) => void
) => {
  const userData = {
    usernameOrEmail: usernameOrEmail,
    password: password,
  };
  axios
    .post("http://localhost:8080/login", userData)
    .then((response) => {
      const { token, role, user, branchId } = response.data as LoginResponse;
      console.log("---Login data-----", response.data);
      localStorage.setItem("token", token);
      localStorage.setItem("branchId", branchId);

      // Set the userData in the Zustand store
      useUserProfileStore.getState().setUserData({ 
        cus_email: response.data.user.cus_email, 
        cus_username: response.data.user.cus_username,
        emp_name: response.data.user.emp_name,
        emp_photo: response.data.user.emp_photo,
        cus_id: response.data.user.cus_id,
        role: response.data.role,

      });
      
      // Store the user data in local storage
        localStorage.setItem('userData', JSON.stringify({
          cus_email: response.data.user.cus_email, 
          cus_username: response.data.user.cus_username,
          emp_name: response.data.user.emp_name,
          emp_photo: response.data.user.emp_photo,
          cus_id:response.data.user.cus_id,
          role: response.data.role,
}));


      // Call the onSuccess callback with additional details
      onSuccess(token, role, user, branchId);
      onNavigate();
      getUserInfo();


    })
    .catch((err) => {
      console.error("Error during Login:", err);
      onError(err);
    });
   console.log(usernameOrEmail);
  console.log(password);
};


 
// Function to retrieve the stored token from local storage
export const getStoredToken = (): string | null => {
  return localStorage.getItem("token");
};
// authHelpers.js

export const getAuthHeaders = () => {
  const authToken = getStoredToken();
  return {
    Authorization: `Bearer ${authToken}`,
  };
};

// Function to retrieve the stored branchId from local storage
export const getStoredBranchId = (): string | null => {
  return localStorage.getItem("branchId");
};









interface UserData {
  cus_email: string;
  cus_username: string;
  emp_name: string;
  emp_photo: string;
}

const authToken = getStoredToken();  
const headers = {
  Authorization: `Bearer ${authToken}`,
};

export const getUserInfo = async (): Promise<UserData> => {
  try {
    const response = await axios.get("http://localhost:8080/login/profile", {
      headers: headers,
    });
     if (response.status === 200) {
      return response.data.user;  
    } else {
       throw new Error(`Request failed with status ${response.status}`);
    }
  } catch (error) {
    console.error("Error fetching user information:", error);
    throw error;
  }
};





