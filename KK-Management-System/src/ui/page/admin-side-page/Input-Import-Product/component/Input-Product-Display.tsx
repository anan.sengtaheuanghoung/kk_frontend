 

import React, { useEffect, useState } from "react";
import {
  Button,
  MenuItem,
  Select,
  SelectChangeEvent,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
} from "@mui/material";

import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { FetchBranchByUser } from "../../../../../service/http/axios-client/Admin-Side/EmployeeManagement";
import { Branch } from "../../../../../service/http/axios-client/Appointment/CusAppointment";
import { Params, useNavigate, useParams } from "react-router-dom";
import { importProductPost } from "../../../../../service/http/axios-client/Admin-Side/Order-and-Import/Import";
import './Input-Product-Display.css'
import { ImportSuccessSnackbar, useImportSuccessAlert } from "../../../../style/Alert/AdminSide-Alert/ImportSuccess";
import { useErrorImport ,ImportError} from "../../../../style/Alert/AdminSide-Alert/ImportError";
import dayjs from "dayjs";
interface Product {
  pro_id: number;
  pro_name: string;
  pro_cost: number;
  amount: number;
}

function InputProductDisplay({
  productList,
  onDeleteProduct,
}: {
  productList: Product[];
  onDeleteProduct: (index: number) => void;
}) {
  //store collecting data state
  const [order_date, setOrder_date] = useState("");
  const [totalPrice, setTotalPrice] = useState("");


  const [productAmounts, setProductAmounts] = useState<string[]>(
    Array(productList.length).fill("")
  );

  
 


  const { getOrderid } = useParams<Params>();

  //map out branch name
  const [branches, setBranches] = useState<Branch[]>([]);
  const [selectedBranch, setSelectedBranch] = useState<string>("");

  // Handle amount change for a specific product
  const handleAmountChange = (value: string, index: number) => {
    const newAmounts = [...productAmounts];
    newAmounts[index] = value;
    setProductAmounts(newAmounts);
  };
  // Handle price change for a specific product
  const [productPriceRaw, setProductPriceRaw] = useState<string[]>(Array(productList.length).fill(''));

  const handlePriceChange = (value: string, index: number) => {
    // Normalize input value (remove non-digit characters)
    const normalizedValue = normalizeNumberInput(value) || ''; // Ensure normalizedValue is a string
  
    // Update state with normalized value using functional setState
    setProductPriceRaw(prevProductPriceRaw => {
      const newPriceRaw = [...prevProductPriceRaw];
      newPriceRaw[index] = normalizedValue;
      return newPriceRaw;
    });
  };
  const normalizeNumberInput = (value: string): string => {
    return value.replace(/\D/g, '');
  };
  
  
  
  
  
  console.log(productPriceRaw)
  
  

  const handleImport = () => {
    const formattedImport = productList.map((product, index) => ({
      price: productPriceRaw[index],
      amount: productAmounts[index],
      pro_id: String(product.pro_id),
    }));
    importProductPost(
      order_date,
      totalPriceRaw,
      getOrderid,
      selectedBranch,
      formattedImport,
      () => {
        setopenSuccessCreateAccount(true);
        setTimeout(() => {
          navigate("/ReportImportProduct");
        }, 1000);  
      },
      () => {
        setUnsuccessOpen(true)
      },
    );
  };

  const navigate = useNavigate()
  //this useEffect use to mapp a branch by each id
  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const branchesData = await FetchBranchByUser();
        setBranches(branchesData); // Correctly set the state
      } catch (error) {
        console.error("Error fetching branches:", error);
      }
    };

    fetchBranches();
  }, []);
  const handleMapbranch = (event: SelectChangeEvent<string>) => {
    setSelectedBranch(event.target.value);
  };



  

  //change format price 
  const [totalPriceRaw, settotalPriceRaw] = useState("");
  console.log("bg-------",totalPriceRaw)
  // Format number as user types
  const handleTotalChange = (e:any) => {
    const rawValue = e.target.value.replace(/\./g, ''); // Remove dots
    settotalPriceRaw(rawValue);
    const formattedValue = formatNumber(e.target.value); // Format with dots
    setTotalPrice(formattedValue);
  };

     // Format number with dots as separators
     const formatNumber = (value:any) => {
      // Remove non-digit characters
      const numericValue = value.replace(/\D/g, '');
      // Add dots every three digits
      const formattedValue = numericValue.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
      return formattedValue;
    };


 
    const formatNumberForDisplay = (value: string | undefined): string => {
      if (!value) {
        return ''; // Return empty string or handle accordingly based on your use case
      }
  
      // Remove non-digit characters
      const numericValue = value.replace(/\D/g, '') || '';
  
      // Add dots every three digits
      const formattedValue = numericValue.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  
      return formattedValue;
    };
    
    
    




  //alert
  const setopenSuccessCreateAccount = useImportSuccessAlert(
    (state) => state.setopenSuccessCreateAccount
  );
  const setUnsuccessOpen = useErrorImport(
    (state) => state.setUnsuccessOpen
  );


  return (
    <div>
      <p style={{ fontSize: 20 }}>ລາຍການສິນຄ້ານໍາເຂົ້າ</p>
      <div className="OrderListDisplay">
        <div className="OrderListDisplay-grid1">
          <div className="OrderListDisplay-grid1-content">
            <p>ລາຄາລວມ:</p>
            <TextField value={totalPrice} onChange={handleTotalChange}  onBlur={() => setTotalPrice(formatNumber(totalPrice))}>
          
            </TextField>
            {/* onChange={(e) => setTotalPrice(e.target.value)} */}
          </div>
          <div className="OrderListDisplay-grid2-content">
            <p>ເວລາ:</p>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={["DatePicker"]}>
                <DatePicker
                  format="DD-MM-YYYY"
                  value={order_date ? dayjs(order_date, "YYYY-MM-DD") : null}
                  onChange={(date) => {
                    if (date && date.isValid()) {
                      setOrder_date(date.format("YYYY-MM-DD"));
                      console.log("----------", date.format("YYYY-MM-DD"));
                    } else {
                      setOrder_date("");
                    }
                  }}
                />
              </DemoContainer>
            </LocalizationProvider>
          </div>
          <div className="OrderListDisplay-grid1-content">
            <p>ສາຂາ:</p>
            <Select
              className="Pro-textfield-select"
              value={selectedBranch}
              onChange={handleMapbranch}
            >
              {Array.isArray(branches) && branches.length > 0 ? (
                branches.map((branch) => (
                  <MenuItem key={branch.branch_id} value={branch.branch_id}>
                    {branch.branch_name}
                  </MenuItem>
                ))
              ) : (
                <MenuItem disabled>No branches available</MenuItem>
              )}
            </Select>
          </div>
        </div>
        <div className="OrderListDisplay-grid2">
          <TableContainer sx={{ maxHeight: 210 }}>
            <Table>
              <TableBody>
                {productList.map((product, index) => (
                  <TableRow key={product.pro_id}>
                    <TableCell className="Map-selectedProducts">
                      {product.pro_id}
                    </TableCell>
                    <TableCell className="Map-selectedProducts">
                      {product.pro_name}
                    </TableCell>
                    <TableCell className="Map-productlist-price">
                    <TextField value={formatNumberForDisplay(productPriceRaw[index])} onChange={(e) => handlePriceChange(e.target.value, index)}/>
                    </TableCell>
                    <TableCell className="Map-productlist-textfield">
                      <TextField
                        onChange={(e) =>
                          handleAmountChange(e.target.value, index)
                        }
                      ></TextField>
                    </TableCell>
                    <TableCell>
                      <div className="Select-button-orderProduct">
                        <Button onClick={() => onDeleteProduct(product.pro_id)}>
                          ລົບ
                        </Button>
                      </div>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
        <div className="OrderListDisplay-grid3">
          <Button onClick={handleImport}>ຕົກລົງ</Button>
       
        </div>
      </div>
      <ImportSuccessSnackbar/>
      <ImportError/>
    </div>
  );
}

export default InputProductDisplay;
 

