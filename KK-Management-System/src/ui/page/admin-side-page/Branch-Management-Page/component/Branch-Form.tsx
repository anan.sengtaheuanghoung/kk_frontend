import './Branch-Form.css'
import TextField from "@mui/material/TextField";
import { Button } from "@mui/material";
import { useEffect, useState } from 'react';
import { SaveBranch } from '../../../../../service/http/axios-client/Admin-Side/Branch-Management';
import {SaveCompleteSnackbar, useSaveButtonAlert } from '../../../../style/Alert/AdminSide-Alert/SaveButtonAlert';
import {ErrorSaving, useErrorSaving } from '../../../../style/Alert/AdminSide-Alert/ErrorSaving';
import { usebranchStore } from './Branch-Display';
import { EditBranch } from '../../../../../service/http/axios-client/Admin-Side/Branch-Management';
import {EditSuccess, useEditSuccess } from '../../../../style/Alert/AdminSide-Alert/EditSuccessful';
import {ErrorEdit, useErrorEdit } from '../../../../style/Alert/AdminSide-Alert/ErrorEditing';
import { DeleteBranch } from '../../../../../service/http/axios-client/Admin-Side/Branch-Management';
import {DeleteSuccess, useDeleteSuccess } from '../../../../style/Alert/AdminSide-Alert/DeleteSuccessful';
function BranchForm () {
       //take state from zustand
       const { selectedData, setSelectedData } = usebranchStore();

  //call state for using alert
  const setopenSuccessCreateAccount = useSaveButtonAlert((state) => state.setopenSuccessCreateAccount);
  const setUnsuccessOpen = useErrorSaving((state) => state.setUnsuccessOpen);
  const setopenEditSuccess = useEditSuccess((state) => state.setopenEditSuccess);
  const setErrorEdit = useErrorEdit((state) => state.setErrorEdit);
  const setopenDeleteSuccess = useDeleteSuccess((state) => state.setopenDeleteSuccess);






  //handle all state
  const [branch_id,setBranch_id] = useState("")
  const [branch_name,setBranch_name] = useState("")
  const [branch_pnumber,setBranch_pnumber] = useState("")
  const [branch_address,setBranch_address] = useState("")
  const handleSave = () =>{
    SaveBranch(
      branch_name,
      branch_pnumber,
      branch_address,
      ()=>{
        setSelectedData({
          branch_id: '',
          branch_name: '',
          branch_pnumber: '',
          branch_address: ''
        })
        setopenSuccessCreateAccount(true);  
      },
      ()=>{
        setUnsuccessOpen(true);  
      }
 
  )
  console.log (branch_name)
  }

  const handleEdit = () =>{
    EditBranch(
      branch_id,
      branch_name,
      branch_pnumber,
      branch_address,
      () => {
        setSelectedData({
          branch_id: '',
          branch_name: '',
          branch_pnumber: '',
          branch_address: ''
        });
        setopenEditSuccess(true);  
      },
      () => {
        setErrorEdit(true);  
      },

    )

  }

   //CLEAR BUTTON
   const handleClear = () => {
    setSelectedData({
      branch_id: '',
      branch_name: '',
      branch_pnumber: '',
      branch_address: ''
    });
   };


    //DELETE BUTTON
    const handleDelete = () => {
      DeleteBranch(branch_id,
        () => {
          setSelectedData({
            branch_id: '',
            branch_name: '',
            branch_pnumber: '',
            branch_address: ''
          });
          setopenDeleteSuccess(true);  
        },
      );
  
       
    };
  //HANDLDE MAP DATA FROM TABLE ROW
  useEffect(() => {
    console.log("Selected to see pic data :", selectedData);
    setBranch_id(selectedData.branch_id);
    setBranch_name(selectedData.branch_name);
    setBranch_pnumber(selectedData.branch_pnumber);
    setBranch_address(selectedData.branch_address);
  }, [selectedData]);
    return(
        <div className="BranchForm">
        <div className="BranchForm-title">
          <h1>ຈັດການ ສາຂາ </h1>
        </div>
        <div className="BranchForm-input"                      >
          <div className="BranchForm-input-detail  BranchForm-input-detail-margin">
            <p>ລະຫັດສາຂາ:</p>
            <TextField disabled value={branch_id}> </TextField>
          </div>
          <div className="BranchForm-input-detail">
            <p>ຊື່ສາຂາ:</p>
            <TextField  value={branch_name} onChange={(e) => setBranch_name(e.target.value)}> </TextField>
          </div>
          <div className="BranchForm-input-detail">
            <p>ເບີໂທ:</p>
            <TextField value={branch_pnumber} onChange={(e) => setBranch_pnumber(e.target.value)}> </TextField>
          </div>
          <div className="BranchForm-input-detail">
            <p>ທີ່ຢູ່ສາຂາ:</p>
            <TextField value={branch_address} onChange={(e) => setBranch_address(e.target.value)}> </TextField>
          </div>
        </div>
        <div className="BranchManageForm-button">
          <Button onClick={handleSave}>ບັນທຶກ</Button>
          <Button onClick={handleEdit}>ເເກ້ໄຂ</Button>
          <Button onClick={handleClear} >ລ້າງ</Button>
          <Button  onClick={handleDelete}>ລົບ</Button>
        </div>
        <SaveCompleteSnackbar/>
        <ErrorSaving/>
        <EditSuccess/>
        <ErrorEdit/>
        <DeleteSuccess/>
      </div>
    )
}
export default BranchForm