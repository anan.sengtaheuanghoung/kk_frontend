import "./Barber-Selection.css";
import LOGO from "../../../../image/LOGO.png";
import { Button } from "@mui/material";
function BarberSelection() {
  return (
    <div className="barber-main-div">
      <div className="barber-content">
        <div className="barber-first-content">
          <h1>ເລືອກຊ່າງຕັດຜົມ</h1>
        </div>
        <div className="barber-second-content">
          <div className="grid-second-content">
            <img src={LOGO} />
            <p>
              Just like the good ol'days. A series of hot towels followed by a
              straight razor and aftershave.
            </p>
            <Button variant="contained"> ເລືອກ </Button>
          </div>
          <div className="grid-second-content">
          <img src={LOGO} />
            <p>
              Just like the good ol'days. A series of hot towels followed by a
              straight razor and aftershave.
            </p>
            <Button variant="contained"> ເລືອກ </Button>
          </div>
          <div className="grid-second-content">
          <img src={LOGO} />
            <p>
              Just like the good ol'days. A series of hot towels followed by a
              straight razor and aftershave.
            </p>
            <Button variant="contained"> ເລືອກ </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
export default BarberSelection;
