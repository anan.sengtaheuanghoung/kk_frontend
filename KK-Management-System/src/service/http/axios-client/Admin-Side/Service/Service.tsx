import axios from "axios";
import { getStoredToken } from "../../Login-axios";

interface Product {
    pro_id:string;
    pro_name:string;
    pro_price:string;
    pro_photo:string;
}
const authToken = getStoredToken();
const headers = {
    Authorization: `Bearer ${authToken}`,
};

export const FetchUseServicedata = async (): Promise<Product[]> => {
    
    try {
      const response = await axios.get("http://localhost:8080/use-services", {
        headers: headers,
      });
      console.log("fectching Service data successful")
      return response.data as Product[];
      
     } catch (error) {
      console.error("Error fetching Service data:", error);
      throw error;
    }
};








//import product
interface ServiceData {
  ser_total:number;
  ser_date:string | undefined;
  app_id: string| undefined;
  cus_id:string| undefined;
  branch_id:string| undefined;
  emp_id: string| undefined;
  service_details: { amount: number; pro_id: string , price:string}[]
}

export const ServicePost = (
  ser_total:number,
  ser_date: string| undefined,
  app_id: string| undefined,
  cus_id:string| undefined,
  branch_id:string| undefined,
  emp_id: string| undefined,
  import_details: { amount: number; pro_id: string , price:string}[],
  onSuccess: () => void,
  onUnsuccess: () => void,
) => {
  const ServiceDataDetail: ServiceData = {
    ser_total: ser_total,
    ser_date: ser_date,
    app_id:app_id,
    cus_id:cus_id,
    emp_id:emp_id,
    branch_id: branch_id,
    service_details: import_details,
  };
  console.log("this is Postdata",ServiceDataDetail)


  axios
    .post(`http://localhost:8080/service`, ServiceDataDetail,{
      headers:headers
    })
    .then((response) => {
      onSuccess()
      console.log("service successful:", response.data);
     })
    .catch((error) => {
      onUnsuccess()
      console.error("fail during ServiceDataDetail:", error.message);
     });
};

