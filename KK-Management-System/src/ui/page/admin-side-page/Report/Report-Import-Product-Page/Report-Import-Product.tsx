import './Report-Import-Product.css'
import SideBar from '../../../../style/AdminPage-sideBar'
import ReportImportProductDisplay from './component/Report-Import-Product-Display'
import ReportImportProductForm from './component/Report-Import-Product-Form'
import { SetStateAction, useState } from 'react';
import { Dayjs } from 'dayjs';


function ReportImportProduct () {
   //handle text search 
   const [searchQuery, setSearchQuery] = useState('');
   const handleSearch = (value: SetStateAction<string>) => {
     setSearchQuery(value);
   };
 
   //handle date Rangepicker 
   const [startDate, setStartDate] = useState<Dayjs | null>(null);
   const [endDate, setEndDate] = useState<Dayjs | null>(null);
   const handleDateChange = (start: Dayjs | null, end: Dayjs | null) => {
     setStartDate(start);
     setEndDate(end);
   };

   
    return(
        <div className="import-pro-page">
        <div className="import-pro-first-grid">
          <SideBar />
        </div>
        <div className="import-pro-second-grid">
          <ReportImportProductForm onSearch={handleSearch} onDateChange={handleDateChange}/>
          <ReportImportProductDisplay searchData={searchQuery}  startDate={startDate} endDate={endDate} />
        </div>
      </div>
    )
}
export default ReportImportProduct