import axios from "axios";
import { getStoredToken } from "../../Login-axios";

interface Product {
    app_id:string;
    cus_username:string;
    app_date:string;
    app_datetime:string;
    emp_name:string;
    app_status:string;
    branch_id:string;
}
const authToken = getStoredToken();
const headers = {
    Authorization: `Bearer ${authToken}`,
};




export const FetchManageAppointment = async (): Promise<Product[]> => {
  const authToken = getStoredToken();
  const headers = {
    Authorization: `Bearer ${authToken}`,
  };


  try {
      const response = await axios.get("http://localhost:8080/management-appointment", {
        headers: headers,
      });
      console.log("fectching Manage Appointment successful", response.data)
      return response.data as Product[];     
     } catch (error) {
      console.error("Error fetching Manage Appointment:", error);
      throw error;
    }
};





//import product
interface ServiceData {
    cus_username:string | undefined;
    emp_name: string;
    app_date:string;
    app_datetime:string;
    app_id:string | undefined;
 
}
  export const EditManageAppointment = (
    cus_username:string | undefined,
    emp_name:string,
    app_date:string,
    app_datetime:string,
    app_id: string | undefined,
    navigate : () => void

  ) => {
    const AppointDataDetail: ServiceData = {
        cus_username: cus_username,
        emp_name: emp_name,
        app_date:app_date,
        app_datetime:app_datetime,
        app_id:app_id,

     
    };
   
    axios
      .put(`http://localhost:8080/edit-appointment`,AppointDataDetail,{
        headers:headers
      })
      .then((response) => {
        console.log("-------------", response)
        navigate()
       })
      .catch((error) => {
        console.error("Edit appointment  fail", error.message);
       });
  };
  






//import product
interface DeleteAppointmentData {
  app_id:string | undefined;

}
export const deleteManageAppointment = (
 
  app_id: string | undefined,

) => {
  const DeleteAppointDataDetail: DeleteAppointmentData = {     
    app_id:app_id,
  };
  axios
    .put(`http://localhost:8080/admin-cancel-app`,DeleteAppointDataDetail,{
      headers:headers
    })
    .then((response) => {
 
      console.log("Delete appointment successful:", response.data);
     })
    .catch((error) => {
      console.error("Delete appointment  fail", error.message);
     });
};
