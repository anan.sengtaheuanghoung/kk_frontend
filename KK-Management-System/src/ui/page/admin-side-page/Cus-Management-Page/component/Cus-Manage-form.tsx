import './Cus-Manage-form.css'
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import { useEffect, useState } from 'react';
import { SaveCus,EditCus,DeleteCus } from '../../../../../service/http/axios-client/Admin-Side/CustomerManagement';
import { useCusStore } from './Cus-Manage-display';
import { SaveCompleteSnackbar ,useSaveButtonAlert } from "../../../../style/Alert/AdminSide-Alert/SaveButtonAlert";
import { ErrorSaving,useErrorSaving } from "../../../../style/Alert/AdminSide-Alert/ErrorSaving";
import { EditSuccess,useEditSuccess } from "../../../../style/Alert/AdminSide-Alert/EditSuccessful";
import { DeleteSuccess, useDeleteSuccess } from "../../../../style/Alert/AdminSide-Alert/DeleteSuccessful";
import { ErrorEdit ,useErrorEdit } from "../../../../style/Alert/AdminSide-Alert/ErrorEditing";


function CusManageForm () {

  //take state from zustand
  const { selectedData, setSelectedData } = useCusStore();

  //state that use to store the value 
    const [id, setId] = useState("");
    const [username,setusername] =  useState("");
    const [email,setemail] =  useState("");
    const [phoneNumber,setPhoneNumber] =  useState("");
    const [password,setPassword] =  useState("");

   //alert that use from zustand
   const setopenSuccessCreateAccount = useSaveButtonAlert((state) => state.setopenSuccessCreateAccount);
   const setUnsuccessOpen = useErrorSaving((state) => state.setUnsuccessOpen);
   const setopenEditSuccess = useEditSuccess((state) => state.setopenEditSuccess);
   const setopenDeleteSuccess = useDeleteSuccess((state) => state.setopenDeleteSuccess);
   const setErrorEdit = useErrorEdit((state) => state.setErrorEdit);  

  //save button
  const handleSave = () => {
      SaveCus(
        username,
        email,
        phoneNumber,
        password,
        () => {
          setSelectedData({
            id,
            username,
            email,
            phoneNumber,
            password,
            
          });
          
          setopenSuccessCreateAccount(true);  
        },
        () => {
          setUnsuccessOpen(true);  
        }
      )
      

  }
  //EDIT BUTTON
  const handleEdit = () => {
    EditCus(
        id,
        username,
        email,
        phoneNumber,
        password,
        () => {
          setSelectedData({
            id,
            username,
            email,
            phoneNumber,
            password,
            
          });
          setopenEditSuccess(true);  
        },
        () => {
          setErrorEdit(true);  
        },

    );
    // window.location.reload(); // Refreshes the page

  };
  //CLEAR BUTTON
  const handleClear = () => {
    setSelectedData({
      id: "",
      username: "",
      email: "",
      password: "",
      phoneNumber: "",
      });
   
  };
  //DELETE BUTTON
  const handleDelete = () => {
      DeleteCus(id, 
        () => {
          setSelectedData({
            id: "",
            username: "",
            email: "",
            password: "",
            phoneNumber: "",          
          });
        setopenDeleteSuccess(true);  
      },);
      console.log(id)
  };
  
  //HANDLDE MAP DATA FROM TABLE ROW
  useEffect(() => {
    console.log("Selected to see pic data :", selectedData); 
    setId(selectedData.id);
    setemail(selectedData.email);
    setPhoneNumber(selectedData.phoneNumber);
    setusername(selectedData.username);
    setPassword(selectedData.password);
  }, [selectedData]);

    return(
       <div>
        <div className="CusManageForm">
        <div className="CusManageForm-title">
          <h1> ຈັດການກ່ຽວກັບລູກຄ້າ</h1>
        </div>
        <div className="CusManageForm-input">
          <div className="CusManageForm-input-grid1 CusManageForm-grid-direction">
            {/* <div className="CusManageForm-input-detail ">
              <p>ລະຫັດ:</p>

              <div className="Cus-textfield ">
                <TextField  disabled value={id }onChange={(e) => setId(e.target.value)} ></TextField>
              </div>
            </div> */}
            <div className="CusManageForm-input-detail">
              <p>ຊື່:</p>
              <div className="Cus-textfield  ">
                <TextField value={username } onChange={(e)=> setusername(e.target.value)}></TextField>
              </div>
            </div>
            <div className="CusManageForm-input-detail">
              <p>ເບີໂທ:</p>
              <div className="Cus-textfield">
                <TextField value={phoneNumber}  onChange={(e)=> setPhoneNumber(e.target.value)}></TextField>
              </div>
            </div>
             
          </div>
          <div className="CusManageForm-input-grid2  CusManageForm-grid-direction">
            <div className="CusManageForm-input-detail">
              <p>ອີເມວ:</p>
              <div className="Cus-textfield  ">
                <TextField value={email } onChange={(e)=> setemail(e.target.value)}></TextField>
              </div>
            </div>
            {/* <div className="CusManageForm-input-detail">
              <p>ຊື່ຜູ້ໃຊ້:</p>
              <div className="Cus-textfield ">
                <TextField></TextField>
              </div>
            </div> */}
            <div className="CusManageForm-input-detail">
              <p>ລະຫັດຜ່ານ:</p>
              <div className="Cus-textfield  ">
                <TextField value={password} onChange={(e)=> setPassword(e.target.value)}></TextField>
              </div>
            </div>
          </div>
        </div>
        <div className="cusManageForm-button">
          <Button onClick={handleSave}>ບັນທືກ</Button>
          <Button onClick={handleEdit}>ແກ້ໄຂ</Button>
          <Button onClick={handleClear}>ລ້າງ</Button>
          <Button onClick={handleDelete}>ລົບ</Button>
        </div>
      </div>
      <SaveCompleteSnackbar/>
      <ErrorSaving/>
      <EditSuccess/>
      <DeleteSuccess/>
      <ErrorEdit/>       
      </div> 
    )
}
export default CusManageForm


