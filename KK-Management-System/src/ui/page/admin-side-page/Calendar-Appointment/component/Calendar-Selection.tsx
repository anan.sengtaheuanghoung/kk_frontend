// import './Calendar-Selection.css'
// import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
// import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
// import { DateCalendar } from "@mui/x-date-pickers/DateCalendar";
// import { Button, Select, TextField, } from "@mui/material";

// function CalendarSelection () {
//     return(
//       <div className="CalendarSelection-main-div">
//       <div className="CalendarSelection-detail">
//         <div className="CalendarSelection-content-first">
//           <div className="CalendarSelection-content-datail">
//             <div className="CalendarSelection-deatail-first-div">
//               <p>ຊື່:</p>
//               <TextField className="CalendarSelection-first-div-textfield">
//               </TextField>
//               <p>ສາຂາ:</p>
//               <Select className="CalendarSelection-first-div-select"/>
//               <p>ຊ່າງຕັດຜົມ: </p>
//               <TextField className="CalendarSelection-first-div-textfield">

//               </TextField>
//             </div>
//             {/* calendar */}
//             <div className="CalendarSelection-calendar-div">
//               <LocalizationProvider dateAdapter={AdapterDayjs} >
//                 <DateCalendar  />
//               </LocalizationProvider>
//             </div>
//             {/* time selection */}
//             <div className="CalendarSelection-deatail-third-div">
//               <div className="CalendarSelection-third-div-title">
//                 <p>ເລືອກເວລາຕັດຜົມ </p>
//               </div>
//               <div className="CalendarSelection-third-div-grid">
//                 <div className="CalendarSelection-grid-style">
//                   <p>ເຊົ້າ</p>
//                   <div className="morning-admin-appoint-grid-style-detail  CalendarSelection-button-Style">
//                     <div className="">
//                       <Button variant="outlined">8:00 am</Button>
//                     </div>
//                     <div>
//                       <Button variant="outlined">9:00 am</Button>
//                     </div>
//                     <div>
//                       <Button variant="outlined">10:00 am</Button>
//                     </div>
//                     <div>
//                       <Button variant="outlined">11:00 am</Button>
//                     </div>
//                   </div>
//                 </div>
//                 <div className=" CalendarSelection-grid-style  ">
//                   <p>ສວາຍ</p>
//                   <div className="afternoon-admin-appoint-grid-style-detail CalendarSelection-button-Style">
//                     <div>
//                       <Button variant="outlined">12:00 pm</Button>
//                     </div>
//                     <div>
//                       <Button variant="outlined">13:00 pm</Button>
//                     </div>
//                     <div>
//                       <Button variant="outlined">14:00 pm</Button>
//                     </div>
//                     <div>
//                       <Button variant="outlined">15:00 pm</Button>
//                     </div>
//                     <div>
//                       <Button variant="outlined">16:00 pm</Button>
//                     </div>
//                     <div>
//                       <Button variant="outlined">17:00 pm</Button>
//                     </div>
//                   </div>
//                 </div>
//                 <div className="CalendarSelection-grid-style CalendarSelection-button-Style">
//                   <p>ເເລງ </p>
//                   <div className="evening-admin-appoint-grid-style-detail">
//                     <div>
//                       <Button variant="outlined">18:00 pm</Button>
//                     </div>
//                     <div>
//                       <Button variant="outlined">19:00 pm</Button>
//                     </div>
//                     <div>
//                       <Button variant="outlined">20:00 pm</Button>
//                     </div>
//                   </div>
//                   <div></div>
//                 </div>
//               </div>
//               <div className="CalendarSelection-input-name-div">
//                 <Button variant="outlined">ຕົກລົງ</Button>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//     )
// }
// export default CalendarSelection

import "./Calendar-Selection.css";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DateCalendar } from "@mui/x-date-pickers/DateCalendar";
import {
  Button,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  colors,
} from "@mui/material";

import { useState, useEffect} from "react";
import { Params, useNavigate, useParams } from "react-router-dom";
import dayjs, { Dayjs } from "dayjs";
import { fetchEmployeeData } from "../../../../../service/http/axios-client/Admin-Side/Service/Appointment-by-admin";
import { EditManageAppointment } from "../../../../../service/http/axios-client/Admin-Side/Admin-Appointment-Route/Admin-Appointment-management";
import { getStoredToken } from "../../../../../service/http/axios-client/Login-axios";
import axios from "axios";
interface Employee {
  emp_id: string;
  emp_name: string | number;
  branch_id: string;
}

function CalendarSelection() {
    
  const [getTime, setTime] = useState<string>("");
  const [getDate, setDate] = useState<string>("");
  const [bookedTimes, setBookedTimes] = useState<string[]>([]);
 
  const handleTimeValue = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    const selectedTime = (event.currentTarget as HTMLButtonElement).value;
    if (!bookedTimes.includes(selectedTime)) {
      setTime(selectedTime);
    }
  };
  //set disable date
  const isPastDate = (date: Dayjs): boolean => {
    const today = dayjs();
    return date.isBefore(today, "day");
  };
  //fect employee to select
  const [employeesName, setEmployees] = useState<Employee[]>([]);
  const [takeEmpID, settakeEmpID] = useState<string[]>([]);
  const [takebranchID, settakebranchID] = useState<string[]>([]);
 
  
  useEffect(() => {
    const fetchEmployeesName = async () => {
      try {
        const employeedata = await fetchEmployeeData();
        setEmployees(employeedata); // Correctly set the state       
      } catch (error) {
        console.error("Error fetching employee data:", error);
      }
    };
    fetchEmployeesName();
  }, []);


  //USE PARAM
  const { appId, cusUsername } = useParams<Params>();
  const [selectedEmployeeName, setSelectedEmployeeName] = useState<string>(""); // Define state variable for selected employee name

  const handleEmployeeChange = (event: SelectChangeEvent<string>) => {
    const selectedEmpName = event.target.value;
    const selectedEmp = employeesName.find(emp => emp.emp_name === selectedEmpName);
    
    if (selectedEmp) {
      setSelectedEmployeeName(selectedEmpName);
      settakeEmpID([selectedEmp.emp_id]); // Update takeEmpID with the emp_id of the selected employee
      settakebranchID([selectedEmp.branch_id]); // Update takeBranchID with the branch_id of the selected employee

    }
  };


  const navigate = useNavigate()
  //handle Edit
  const hanleAppointmentEdit = () => {
    EditManageAppointment(
      cusUsername,
      selectedEmployeeName,
      getDate,
      getTime,
      appId,
      () => {
        setTimeout(() => {
          navigate("/AppointmentManagePage")
        },1000);
      }
    ) ;
  };

   // Check the time button, if it was booked it will change to red
   useEffect(() => {
    const fetchBookedTimes = async () => {
      const authToken = getStoredToken();
      const headers = {
        Authorization: `Bearer ${authToken}`,
      };
      if (getDate) {
        try {
          const response = await axios.get(
            "http://localhost:8080/booked-times",
            {
              headers: headers,
              params: {
                date: getDate,
                employeeId: takeEmpID,
                branchId: takebranchID,
              },
            }
          );
          setBookedTimes(response.data.bookedTimes);
        } catch (error) {
          console.error("Error fetching booked times:", error);
        }
      }
    };
    fetchBookedTimes();
  }, [getDate, takeEmpID, ]);



  const formatTime = (time: string): string => {
    const [hour, minute] = time.split(':').map(Number);
    const ampm = hour >= 12 ? 'PM' : 'AM';
    const formattedHour = hour % 12 === 0 ? 12 : hour % 12;
    return `${formattedHour}:${minute.toString().padStart(2, '0')} ${ampm}`;
  };
    
  const isTimePast = (time: string, selectedDate: string): boolean => {
    const currentDate = dayjs();
    const buttonDate = dayjs(selectedDate).set('hour', parseInt(time.split(':')[0])).set('minute', parseInt(time.split(':')[1])).set('second', parseInt(time.split(':')[2]));  
    if (buttonDate.isAfter(currentDate)) {
      // If the selected date is in the future, no need to disable
      return false;
    }
    return true;
  };
  
  return (
    <div className="admin-carlendar-main-div">
      <div className="admin-carlendar-selection-detail">
        <div className="admin-carlendar-selection-content-first">
          <div className="admin-carlendar-deatail-first-div">
            <div className="CalendarSelection-first-div-textfield">
              <p>ຊື່:</p>
              <TextField value={cusUsername}></TextField>
            </div>
            <div className="CalendarSelection-first-div">
              <p>ຊ່າງຕັດຜົມ:</p>
              <Select
                className="CalendarSelection-first-div-select"
                value={selectedEmployeeName}
                onChange={handleEmployeeChange}
              >
                {Array.isArray(employeesName) && employeesName.length > 0 ? (
                  employeesName.map((employee) => (
                    <MenuItem key={employee.emp_id} value={employee.emp_name}>
                      {employee.emp_name}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>No branches available</MenuItem>
                )}
              </Select>
            </div>
          </div>
          <div className="admin-carlendar-calendar-div">
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DateCalendar
                views={["year", "month", "day"]}
                className="admin-carlendar-datecalendar"
                onChange={(date) => {
                  const formattedDate = date?.format("YYYY-MM-DD");
                  if (formattedDate && !isPastDate(dayjs(formattedDate))) {
                    setDate(formattedDate);
                  }
                }}
                shouldDisableDate={isPastDate}
              />
            </LocalizationProvider>
          </div>

          <div className="admin-carlendar-deatail-third-div">
            <div className="admin-carlendar-third-div-title">
              <h3>ເລືອກເວລາຕັດຜົມ</h3>
            </div>

            <div className="admin-carlendar-third-div-grid">
              <div className="admin-carlendar-grid-style">
                <p>ເຊົ້າ</p>
                <div className="admin-carlendar-morning-grid-style-detail button-Style">
                {["8:00:00", "9:00:00", "10:00:00", "11:00:00"].map(
                    (time) => {
                      const isDisabled = bookedTimes.includes(time) || isTimePast(time,getDate);
                      return (
                        <Button
                          key={time}
                          variant="outlined"
                          value={time}
                          onClick={handleTimeValue}
                           className={isDisabled ? "button-booked" : ""} // Conditionally apply booked class
                        >
                          {formatTime(time)}
                        </Button>
                      );
                    }
                  )}
                </div>
              </div>

              <div className="admin-carlendar-grid-style">
                <p>ສວາຍ</p>
                <div className="admin-carlendar-afternoon-grid-style-detail admin-carlendar-button-Style">
                {[
                    "12:00:00",
                    "13:00:00",
                    "14:00:00",
                    "15:00:00",
                    "16:00:00",
                    "17:00:00",
                  ].map((time) => {
                    const isDisabled = bookedTimes.includes(time) || isTimePast(time,getDate);
                    return (
                      <Button
                        key={time}
                        variant="outlined"
                        value={time}
                        onClick={handleTimeValue}
                         className={isDisabled ? "button-booked" : ""} // Conditionally apply booked class
                      >
                        {formatTime(time)}
                      </Button>
                    );
                  })}
                </div>
              </div>

              <div className="admin-carlendar-grid-style">
                <p>ເເລງ</p>
                <div className="admin-carlendar-evening-grid-style-detail button-Style">
                {["18:00:00", "19:00:00", "20:00:00"].map((time) => {
                    const isDisabled = bookedTimes.includes(time) || isTimePast(time,getDate);
                    return (
                      <Button
                        key={time}
                        variant="outlined"
                        value={time}
                        onClick={handleTimeValue}
                         className={isDisabled ? "button-booked" : ""} // Conditionally apply booked class
                      >
                        {formatTime(time)}
                      </Button>
                    );
                  })}
                </div>
              </div>
            </div>
            <div className="admin-carlendar-input-name-div">
              <Button
                variant="outlined"
                disabled={!getTime || bookedTimes.includes(getTime)}
                onClick={hanleAppointmentEdit}
              >
                ຕົກລົງ
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CalendarSelection;
