import { Alert, Snackbar, Stack } from "@mui/material";
import { create } from "zustand";

interface SnackbarAdminSaveButton {
openSuccessCreateAccount: boolean;
setopenSuccessCreateAccount: (open: boolean) => void;
}

export const useBillSuccessAlert = create<SnackbarAdminSaveButton>((set) => ({
    openSuccessCreateAccount: false,
    setopenSuccessCreateAccount: (openSuccessCreateAccount) => set({ openSuccessCreateAccount }),
}));

export const BillSuccessSnackbar = () => {
  const { openSuccessCreateAccount , setopenSuccessCreateAccount } = useBillSuccessAlert();
  const handleClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setopenSuccessCreateAccount(false);
  };

  return (
    <div>
      <Snackbar
        open={openSuccessCreateAccount}
        autoHideDuration={1200}
        onClose={handleClose}
        message="This Snackbar will be dismissed in 5 seconds."
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Stack sx={{ width: "100%" }} spacing={2}>
          <Alert
            severity="success"
            style={{ width: 200, fontSize: 20, fontFamily: "phetsarath ot" }}
          >
            ໃຊ້ບໍລິການສໍາເຫຼັດ 
          </Alert>
        </Stack>
      </Snackbar>
    </div>
  );
};
