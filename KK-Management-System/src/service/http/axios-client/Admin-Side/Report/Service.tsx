import axios from "axios";
import { getStoredToken } from "../../Login-axios";

//this route is use for fetching service Report
interface ServiceData {
    ser_id:string,
    admin_name:string,
    employee_name:string,
    branch_name:string,
    ser_date:string,
    ser_total:string,
}
 

export const fetchServiceReport = async (): Promise<ServiceData[]> => {
  const authToken = getStoredToken();
  const headers = {
   Authorization: `Bearer ${authToken}`,
};
     try {
       const response = await axios.get("http://localhost:8080/service/report", {
         headers: headers,
       });
       console.log("----service report------", response.data.report) 
       return response.data.report as ServiceData[];  
     } catch (error) {
       console.error("Error fetching Service Report:", error);
       throw error;
     }
};



//report bill
export const fetchServiceBill = async (ser_id: string) => {
  const authToken = getStoredToken();
  const headers = {
   Authorization: `Bearer ${authToken}`,
};
     try {
      const response = await axios.get(`http://localhost:8080/service/bill/${ser_id}`,{
        headers: headers,
      });
      console.log("---------------------",response.data)
       return response.data;

    } catch (error) {
      console.error("Error during fetching service bill:", error);
      throw error;
    }
};
