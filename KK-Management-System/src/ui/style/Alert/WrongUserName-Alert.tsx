import * as React from "react";
import Snackbar from "@mui/material/Snackbar";

import { Alert, Stack } from "@mui/material";
import { create } from "zustand";

interface useWrongUserNameType {
    UnsuccessOpen: boolean;
    setUnsuccessOpen: (UnsuccessOpen: boolean) => void;
  }
  
  export const useWrongUserNameStore = create<useWrongUserNameType>((set) => ({
    UnsuccessOpen: false,
    setUnsuccessOpen: (UnsuccessOpen) => set({ UnsuccessOpen }),
  }));
  
export const WrongUsernameAlert = () => {
  const {UnsuccessOpen, setUnsuccessOpen} = useWrongUserNameStore();

  const handleClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setUnsuccessOpen(false);
  };

  return (
    <div>
      <Snackbar
        open={UnsuccessOpen}
        autoHideDuration={2500}
        onClose={handleClose}
        message="Note archived"
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Stack sx={{ width: "100%" }} spacing={2}>
          <Alert severity="error" style={{ width: 470, fontSize: 20,fontFamily:"phetsarath ot" }}>ຂໍ້ມູນຜິດພາດ!! (ກະລຸນາປ້ອນຂໍ້ມູນໃຫຖືກຕ້ອງ)
          </Alert>
        </Stack>
      </Snackbar>
    </div>
  );
}
