import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  styled,
  tableCellClasses,
} from "@mui/material";
import "./Emp-Management-display.css";
import React, { ReactNode, useEffect, useState } from "react";
import axios from "axios";
import { create } from "zustand";
import { getStoredToken } from "../../../../../service/http/axios-client/Login-axios";

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

interface useEmpStoreState {
  selectedData: {
    id: string;
    name: string;
    email: string;
    phoneNumber: string;
    address: string;
    username: string;
    password: string;
    pic: File | null;
    branch_id: string;
  };
  setSelectedData: (data: {
    id: string;
    name: string;
    email: string;
    phoneNumber: string;
    address: string;
    username: string;
    password: string;
    pic: File | null;
    branch_id: string;
  }) => void;
}

// Zustand store
export const useEmpStore = create<useEmpStoreState>((set) => ({
  selectedData: {
    id: "",
    name: "",
    email: "",
    phoneNumber: "",
    address: "",
    username: "",
    password: "",
    pic: null,
    branch_id: "",
  },
  setSelectedData: (data: any) => set({ selectedData: data }),
}));

interface Column {
  id: keyof Data;
  label: string;
  minWidth?: number;
  align?: "left" | "center" | "right";
}

const columns: readonly Column[] = [
  { id: "ID", label: "ລະຫັດ" },
  { id: "Name", label: "ຊື່" },
  { id: "PhoneNumber", label: "ເບີໂທ" },
  { id: "Address", label: "ທີ່ຢູ່" },
  { id: "Username", label: "ຊື່ຜູ້ໃຊ້" },
  { id: "Password", label: "ລະຫັດ" },
  { id: "Photo", label: "ຮູບ" },
  // { id: 'Status', label: 'ສະຖານະ' },
  { id: "BranchId", label: "ຊື່ສາຂາ" },
];

interface Data {
  branch_name: ReactNode;
  branch_id: ReactNode;
  emp_photo: ReactNode;
  emp_email: ReactNode;
  emp_address: ReactNode;
  emp_pnumber: ReactNode;
  emp_name: ReactNode;
  emp_username: string;
  emp_password: string;
  emp_pic: string;
  emp_branch_id: string;
  emp_id: number;
  ID: number;
  Name: string;
  PhoneNumber: number;
  Email: string;
  Address: string;
  Username: string;
  Password: string;
  Photo: File | null;
  BranchId: string;
}

function EmpManagedisplay() {
  const [rows, setRows] = useState<Data[]>([]);
  const { selectedData, setSelectedData } = useEmpStore();
  const [searchQuery, setSearchQuery] = useState("");

  //table row click function
  const handleRowClick = (row: {
    emp_id: any;
    emp_name: any;
    emp_email: any;
    emp_pnumber: any;
    emp_address: any;
    emp_username: any;
    emp_password: any;
    emp_photo: any;
    branch_id: any;
    branch_name: any;
  }) => {
    setSelectedData({
      id: row.emp_id,
      name: row.emp_name,
      username: row.emp_username,
      email: row.emp_email,
      password: row.emp_password,
      phoneNumber: row.emp_pnumber,
      address: row.emp_address,
      pic: row.emp_photo,
      branch_id: row.branch_id,
    });
    
  };
  //fect data to show on table
  const fetchEmployeeData = () => {
    const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
    axios
      .get("http://localhost:8080/management/employees", {
       
        headers:headers,
      }) 
      .then((response) => {
        // Set the fetched data to the rows state
        setRows(response.data);
      })
      .catch((error) => {
        console.error("Error fetching employee data:", error);
      });
  }; 
  useEffect(() => {
    fetchEmployeeData();
  }, [selectedData]);

  const filteredRows = rows.filter(
    (row) =>
      typeof row.emp_name === "string" &&
      row.emp_name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <div className="EmpManageDisplay">
      <div className="EmpManageDisplay-grid1">
        <p>ຄົ້ນຫາ:</p>
        <TextField
          className="EmpManageDisplay-grid1-textfield"
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
          sx={{
            "& fieldset": { border: 'none' },
          }}
        />
      </div>
      <div className="EmpManageDisplay-grid2">
        <TableContainer sx={{ maxHeight: 340 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{
                      minWidth: column.minWidth,
                      fontSize: 24,
                      width: 100,
                      fontFamily: "phetsarath ot",
                    }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredRows.map((row) => (
                <StyledTableRow
                  key={row.emp_id}
                  onClick={() => handleRowClick(row)}
                >
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.emp_id}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.emp_name}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.emp_pnumber}
                  </StyledTableCell>
                 
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.emp_address}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.emp_username}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.emp_password}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {typeof row.emp_photo === "string" ? (
                      <img
                        src={row.emp_photo}
                        alt="Employee Photo"
                        style={{
                          height: "50px",
                          width: "50px",
                          objectFit: "cover",
                        }}
                      />
                    ) : (
                      "No Photo"
                    )}
                  </StyledTableCell>
                  <TableCell
                    style={{ fontSize: 17 }}
                    data-branch-id={row.branch_id}
                  >
                    {row.branch_name}
                  </TableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}

export default EmpManagedisplay;
