import './Expenese-Display.css'
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button } from "@mui/material";
import ExpensePopUP from './Expense-Popup';
import { create } from 'zustand';
import { useExpenseStore } from './Expenese-form';
import { useEffect, useState } from 'react';
import { fetchExpenseReport } from '../../../../../../service/http/axios-client/Admin-Side/Report/Expense';

interface ExpenseData {
  pro_id: string;
  pro_name: string;
  amount: string;
  pro_cost: string;
  total_price: string;
  imp_total: string;
  branch_name:string;
}

interface ReportState {
  ExpenseOrder: ExpenseData[];
  setExpenseOrder: (reportOrder: ExpenseData[]) => void;
}

export const useExpenseMapoutData = create<ReportState>((set) => ({
  ExpenseOrder: [],
  setExpenseOrder: (ExpenseOrder) => set({ ExpenseOrder }),
}));




function ExpeneseDisplay(){
  const { ExpenseOrder, setExpenseOrder } = useExpenseMapoutData(); // Use Zustand store
  const [loading, setLoading] = useState<boolean>(true);
  const { startDate, endDate } = useExpenseStore();

  useEffect(() => {
    const fetchReportData = async () => {
      try {
        const ExpenseList: ExpenseData[] = await fetchExpenseReport(startDate, endDate);
        setExpenseOrder(ExpenseList); // Set the reportOrder in Zustand store
        setLoading(false);
      } catch (error) {
        console.error("Error fetching product data:", error);
      }
    };

    if (startDate && endDate) {
      fetchReportData();
    }
  }, [startDate, endDate, setExpenseOrder]);

    return(
        <div className="ExpeneseDisplay">
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
              <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ລະຫັດ</TableCell>
                <TableCell  style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ຊື່ສິນຄ້າ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ລາຄາ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ຈໍານວນ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ລາຄາລວມ</TableCell>
 
              </TableRow>
            </TableHead>
            <TableBody>
            {loading ? (
                <TableRow className='Loading'>
                   <p>ກະລຸນາເລືອກວັນທີເພື່ອສະເເດງລາຍຮັບ !!!</p>
                </TableRow>
            ) : (
              ExpenseOrder.map((expense,index) => (
                <TableRow  >
                  <TableCell className="Map-ReportOrder">
                    {index + 1}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {expense.pro_name}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {expense.pro_cost}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {expense.amount}
                  </TableCell>  
                  <TableCell className="Map-ReportOrder">
                    {expense.total_price}
                  </TableCell>                                
                </TableRow>
              ))
            )}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    )
}
export default ExpeneseDisplay