 
import React, { useEffect, useState } from "react";
import {
  Button,
  MenuItem,
  Select,
  SelectChangeEvent,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
} from "@mui/material";
import "./OrderPro-OrderList.css";
import { getStoredToken } from "../../../../../service/http/axios-client/Login-axios";
import axios from "axios";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { DemoContainer, DemoItem } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { FetchBranchByUser } from "../../../../../service/http/axios-client/Admin-Side/EmployeeManagement";
import { Branch } from "../../../../../service/http/axios-client/Appointment/CusAppointment";
import { OrderProductPost } from "../../../../../service/http/axios-client/Admin-Side/Order-and-Import/Orders";
import { useOrderSuccessAlert ,OrderSuccessSnackbar} from "../../../../style/Alert/AdminSide-Alert/OrderSuccess";
import { useErrorOrder ,OrderError} from "../../../../style/Alert/AdminSide-Alert/OrderError";
import { useNavigate } from "react-router-dom";
import dayjs from "dayjs";
interface Product {
  pro_id: number;
  pro_name: string;
  pro_cost: number;
  amount: number;
}

function OrderList({
  selectedProducts,
  onDeleteProduct,
}: {
  selectedProducts: Product[];
  onDeleteProduct: (index: number) => void;
}) {
  //store collecting data state
  const [order_date, setOrder_date] = useState("");
  const [supplierData, setSupplierData] = useState<any[]>([]);
  const [selectSuppier, setselectSuppier] = useState("");
  const [productAmounts, setProductAmounts] = useState<string[]>(
    Array(selectedProducts.length).fill("")
  );

  //map out branch name
  const [branches, setBranches] = useState<Branch[]>([]);
  const [selectedBranch, setSelectedBranch] = useState<string>("");

  // Handle amount change for a specific product
  const handleAmountChange = (value: string, index: number) => {
    const newAmounts = [...productAmounts];
    newAmounts[index] = value;
    setProductAmounts(newAmounts);
  };

  //post Order
  const handleOrder = () => {
    const formattedProducts = selectedProducts.map((product, index) => ({
      amount: productAmounts[index], // Use the corresponding amount for each product
      pro_id: String(product.pro_id),
    }));
     OrderProductPost(
      order_date,
      selectSuppier,
      selectedBranch,
      formattedProducts,
      () => {
        setopenSuccessCreateAccount(true);
        setTimeout(() => {
          navigate("/ReportOrderPage");
        }, 1000); 
      },
      () => {
        setUnsuccessOpen(true)
      },
      
    );
   };
  const navigate = useNavigate()



  //this useEffect use to mapp a branch by each id
  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const branchesData = await FetchBranchByUser();
        setBranches(branchesData); // Correctly set the state
      } catch (error) {
        console.error("Error fetching branches:", error);
      }
    };

    fetchBranches();
  }, []);
  const handleMapbranch = (event: SelectChangeEvent<string>) => {
    setSelectedBranch(event.target.value);
  };

  //fecth Suppiler
  const fetchSupplierData = () => {
    const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
    axios
      .get("http://localhost:8080/suppliers", {
        headers: headers,
      })
      .then((response) => {
        console.log("Fetched supplier data:", response.data);
        setSupplierData(response.data);
      })
      .catch((error) => {
        console.error("Error fetching supplier data:", error);
      });
  };
  useEffect(() => {
    fetchSupplierData();
  }, []);
  const handleMapSuppier = (event: SelectChangeEvent<string>) => {
    setselectSuppier(event.target.value);
  };

  //alter 
  const setopenSuccessCreateAccount = useOrderSuccessAlert(
    (state) => state.setopenSuccessCreateAccount
  );
  const setUnsuccessOpen = useErrorOrder(
    (state) => state.setUnsuccessOpen
  );



  
  return (
    <div>
      <p style={{ fontSize: 20 , fontFamily:"phetsarath ot"}}>ລາຍການສັ່ງຊື້</p>
      <div className="OrderListDisplay">
        <div className="OrderListDisplay-grid1">
          <div className="OrderListDisplay-grid1-content">
            <p>ຜູ້ສະໜອງ:</p>
            <Select
              className="OrderListDisplay-grid1-content-select"
              displayEmpty
              inputProps={{ "aria-label": "Without label" }}
              onChange={handleMapSuppier}
            >
              {supplierData.map((supplier) => (
                <MenuItem key={supplier.sup_id} value={supplier.sup_id}>
                  {supplier.sup_name}
                </MenuItem>
              ))}
            </Select>
          </div>
          <div className="OrderListDisplay-grid2-content">
            <p>ເວລາ:</p>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={["DatePicker"]}>
                <DatePicker
                  format="DD-MM-YYYY"
                  value={order_date ? dayjs(order_date, "YYYY-MM-DD") : null}
                  onChange={(date) => {
                    if (date && date.isValid()) {
                      setOrder_date(date.format("YYYY-MM-DD"));
                      console.log("----------", date.format("YYYY-MM-DD"));
                    } else {
                      setOrder_date("");
                    }
                  }}
                />
              </DemoContainer>
            </LocalizationProvider>
          </div>
          <div className="OrderListDisplay-grid1-content">
            <p>ສາຂາ:</p>
            <Select
              className="Pro-textfield-select"
              value={selectedBranch}
              onChange={handleMapbranch}
            >
              {Array.isArray(branches) && branches.length > 0 ? (
                branches.map((branch) => (
                  <MenuItem key={branch.branch_id} value={branch.branch_id}>
                    {branch.branch_name}
                  </MenuItem>
                ))
              ) : (
                <MenuItem disabled>No branches available</MenuItem>
              )}
            </Select>
          </div>
        </div>
        <div className="OrderListDisplay-grid2">
          <TableContainer sx={{ maxHeight: 210 }}>
            <Table>
              <TableBody>
                {selectedProducts.map((product, index) => (
                  <TableRow key={product.pro_id}>
                    <TableCell className='Map-selectedProducts' align="center">{product.pro_id}</TableCell>
                    <TableCell className='Map-selectedProducts' align="center">{product.pro_name}</TableCell>
                    <TableCell className='Map-selectedProducts-price' align="center"> {product.pro_cost}</TableCell>
                    <TableCell className='Map-selectedProducts-textfield'>
                      {" "}
                      <TextField
                                             
                        onChange={(e) =>
                          handleAmountChange(e.target.value, index)
                        }
                      ></TextField>
                    </TableCell>
                    <TableCell
                    >
                      <div className="Select-button-orderProduct">
                      <Button onClick={() => onDeleteProduct(product.pro_id)}>
                        ລົບ
                      </Button>
                      </div>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
        <div className="OrderListDisplay-grid3">
          <Button onClick={handleOrder}>ຕົກລົງ</Button>
         </div>
      </div>
      <OrderSuccessSnackbar/>
      <OrderError/>
    </div>
  );
}

export default OrderList;
