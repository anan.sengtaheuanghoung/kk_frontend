import axios from "axios";
import { getStoredToken } from "../Login-axios";

//this route is use for fetching service Report
interface Data {
    ser_id:string,
    cus_username:string,
    emp_name:string,
    ser_total:string,

}

   export const fetchDashboardReport = async (): Promise<Data[]> => {
    const authToken = getStoredToken();
    const headers = {
     Authorization: `Bearer ${authToken}`,
 };
     try {
       const response = await axios.get("http://localhost:8080/dashboard", {
         headers: headers,
       });
         return response.data.data as Data[];  
     } catch (error) {
       console.error("Error fetching dashoard Report:", error);
       throw error;
     }
};





//this route is use for fetching service Report
interface DData {
  emp_name:string,
  emp_pnumber:string,
  emp_photo:string | null ,
}
export const fetchDashboardEmp = async (): Promise<DData[]> => {
  const authToken = getStoredToken();
  const headers = {
   Authorization: `Bearer ${authToken}`,
};
  try {
    const response = await axios.get("http://localhost:8080/dashboard/employees", {
      headers: headers,
    });
     
    // Assuming the response structure is { employees: DData[] }
    return response.data.employees as DData[];
  } catch (error) {
    console.error("Error fetching dashboard Report:", error);
    throw error;
  }
};



//this route is use for fetching service Report
interface Indexcustomer {
 cus_id:string,
}
export const fetchIndexEmp = async (): Promise<Indexcustomer[]> => {
  const authToken = getStoredToken();
  const headers = {
   Authorization: `Bearer ${authToken}`,
};
  try {
    const response = await axios.get("http://localhost:8080/dashboard/customer/index", {
      headers: headers,
    });
     
    // Assuming the response structure is { employees: DData[] }
    return [{ cus_id: response.data.customerIndex }] as Indexcustomer[];
  } catch (error) {
    console.error("Error fetching dashboard Report:", error);
    throw error;
  }
};



//this route is use for fetching service Report
interface IndexApp {
  app_id:string,
 }
export const fetchIndexApp = async (): Promise<IndexApp[]> => {
  const authToken = getStoredToken();
  const headers = {
   Authorization: `Bearer ${authToken}`,
};
   try {
     const response = await axios.get("http://localhost:8080/dashboard/appointment/index", {
       headers: headers,
     });
     console.log("---AppIndex---", response.data);
     
      return [{ app_id: response.data.appointmentIndex }] as IndexApp[];
   } catch (error) {
     console.error("Error fetching dashboard Report:", error);
     throw error;
   }
 };




//this route is use for fetching service Report
interface IndexStock {
  stock:string,
}
export const fetchIndexStock = async (): Promise<IndexStock[]> => {
  const authToken = getStoredToken();
  const headers = {
   Authorization: `Bearer ${authToken}`,
};
   try {
     const response = await axios.get("http://localhost:8080/dashboard/product-and-service/index", {
       headers: headers,
     });
     console.log("---stock---", response.data);
     
      return [{ stock: response.data.productIndex }] as IndexStock[];
   } catch (error) {
     console.error("Error fetching dashboard Report:", error);
     throw error;
   }
};