import * as React from "react";
import Snackbar from "@mui/material/Snackbar";
import { Alert, Stack } from "@mui/material";
import { create } from "zustand";

interface useErrorSavingType {
    UnsuccessOpen: boolean;
    setUnsuccessOpen: (UnsuccessOpen: boolean) => void;
  }
  
  export const useErrorApp = create<useErrorSavingType>((set) => ({
    UnsuccessOpen: false,
    setUnsuccessOpen: (UnsuccessOpen) => set({ UnsuccessOpen }),
  }));
  
export const ErrorApp = () => {
  const {UnsuccessOpen, setUnsuccessOpen} = useErrorApp();


  const handleClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setUnsuccessOpen(false);
  };

  return (
    <div>
      <Snackbar
        open={UnsuccessOpen}
        autoHideDuration={1200}
        onClose={handleClose}
        message="Note archived"
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Stack sx={{ width: "100%" }} spacing={2}>
          <Alert severity="error" style={{ width: 470, fontSize: 20,fontFamily:"phetsarath ot" }}>ການຈອງບໍ່ສໍາເຫຼັດ!!(ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຖືກຕ້ອງ)</Alert>
        </Stack>
      </Snackbar>
    </div>
  );
}
