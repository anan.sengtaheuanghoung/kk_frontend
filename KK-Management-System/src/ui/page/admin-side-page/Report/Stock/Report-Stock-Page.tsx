import './Report-Stock-Page.css'
import SideBar from '../../../../style/AdminPage-sideBar'
import StockForm from './component/Stock-Form'
import StockDisplay from './component/Stock-Display'
import { useNavigate } from 'react-router-dom'
import { Button } from '@mui/material'
 function StockPage () {
  const navigate = useNavigate()

    return(
        <div className="StockPage">
        <div className="StockPage-first-grid">
          <SideBar />
        </div>
        <div className="StockPage-second-grid">
        <StockForm/>
        <div className="BookingForm-input-grid2 ">
         <Button onClick={() => {navigate(`/OrderProduct`)}}>ສັ່ງຊື້</Button>
        </div>
        <StockDisplay/>

      </div>
      </div>
    )
}
export default StockPage