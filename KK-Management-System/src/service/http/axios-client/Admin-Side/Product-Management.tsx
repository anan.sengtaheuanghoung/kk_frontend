import axios from "axios";
import { getStoredToken } from "../Login-axios";

//save product
interface UseProductData {
    pro_name: string;
    pro_price: string;
    pro_cost: string;
    proType_id: string;
    branch_id: string;
    amount:string;
    // price:string;
    photo: File | null;
  }

  export const SaveProduct = (
    pro_name: string,
    pro_price: string,
    pro_cost: string,
    proType_id: string,
    branch_id: string,
    amount:string,
    // price:string,
    pro_photo: File | null,
    onSuccess: () => void,
    onUnsuccess: () => void,
  ) => {
    const productData: UseProductData = {
      pro_name:pro_name,
      pro_price:pro_price,
      pro_cost:pro_cost,
      proType_id:proType_id,
      branch_id:branch_id,
      amount:amount,
      // price:price,
      photo:pro_photo,
      
    
    };
    const authToken = getStoredToken();
    const headers = {
      'Authorization': `Bearer ${authToken}`,
     };
  
  
    const formData = new FormData();
    formData.append("pro_name", productData.pro_name);
    formData.append("pro_price", productData.pro_price);
    formData.append("pro_cost", productData.pro_cost);
    formData.append("proType_id", productData.proType_id);
    formData.append("branch_id", productData.branch_id);
    formData.append("amount", productData.amount);
    // formData.append("price", price);  
    if (productData.photo) {
      formData.append("photo", productData.photo);
    }
    console.log("--------------------",productData.photo)
  
    axios
      .post("http://localhost:8080/product_and_service", formData, {
        headers:headers,
      })
      .then((response) => {
        console.log("successful import Product");
        console.log("if save success",response.data);
        onSuccess();
      })
      .catch((err) => {
        console.error("Error during save Product:", err);
        onUnsuccess();
      });
};
  

  



export const EditProduct = (
  pro_id:string,
  pro_name: string,
  pro_price: string,
  pro_cost: string,
  proType_id: string,
  newAmount:string,
  pro_photo: File | null,
  onSuccess: () => void,
  onUnsuccess: () => void,

) => {
 
  const formData = new FormData();
  formData.append("proid", pro_id);

  formData.append("pro_name", pro_name);
  formData.append("pro_price", pro_price);
  formData.append("pro_cost",pro_cost);
  formData.append("proType_id", proType_id);
  formData.append("newAmount", newAmount);
  formData.append("photo", pro_photo || "");  
  const authToken = getStoredToken();
  const headers = {
    Authorization: `Bearer ${authToken}`,
  };
  console.log(newAmount)
  axios
    .put(`http://localhost:8080/product_and_service/${pro_id}`, formData, {
      headers:headers,
    })
    .then((response) => {
      console.log("Edit successful");
      console.log(response.data);
      onSuccess ();

    })
    .catch((err) => {
      console.error("Error during edit:", err);
      onUnsuccess();
    });
};




//DeleteEMP
export const DeleteProduct = (pro_id: string,  onSuccess: () => void,
) => {

  const authToken = getStoredToken();
  const headers = {
    Authorization: `Bearer ${authToken}`,
  };
  axios
    .delete(`http://localhost:8080/product_and_service/${pro_id}`, {
      headers:headers,
    })
    .then((response) => {
      console.log("Deletion successful");
      console.log(response.data);
      onSuccess();
    })
    .catch((err) => {
      console.error("Error during deletion:", err);
    });
};