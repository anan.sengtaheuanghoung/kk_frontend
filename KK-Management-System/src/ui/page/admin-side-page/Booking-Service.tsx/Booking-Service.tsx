import { SetStateAction, useState } from 'react';
import SideBar from '../../../style/AdminPage-sideBar'
import './Booking-Service.css'
import AddBooking from './component/Add-booking'
import AddBookforUserdisplay from './component/Add-booking-display';

function BookingSevice () {
    const [searchQuery, setSearchQuery] = useState('');

  const handleSearch = (value: SetStateAction<string>) => {
    setSearchQuery(value);
  };
    return (
        <div className="BookingSevice-page">
        <div className="BookingSevice-first-grid">
          <SideBar />
        </div>
        <div className="BookingSevice-second-grid">
            <AddBooking onSearch={handleSearch}/>
            <AddBookforUserdisplay searchData={searchQuery}/>
        </div>
      </div>
    )
}


export default BookingSevice