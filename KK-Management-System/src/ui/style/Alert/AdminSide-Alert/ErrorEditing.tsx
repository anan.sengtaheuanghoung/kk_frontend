import * as React from "react";
import Snackbar from "@mui/material/Snackbar";
import { Alert, Stack } from "@mui/material";
import { create } from "zustand";

interface useErrorSavingType {
    ErrorEdit: boolean;
    setErrorEdit: (UnsuccessOpen: boolean) => void;
  }
  
  export const useErrorEdit = create<useErrorSavingType>((set) => ({
    ErrorEdit: false,
    setErrorEdit: (ErrorEdit) => set({ ErrorEdit }),
  }));
  
export const ErrorEdit = () => {
  const {ErrorEdit, setErrorEdit} = useErrorEdit();

  const handleClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setErrorEdit(false);
  };

  return (
    <div>
      <Snackbar
        open={ErrorEdit}
        autoHideDuration={1200}
        onClose={handleClose}
        message="Note archived"
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Stack sx={{ width: "100%" }} spacing={2}>
          <Alert severity="error" style={{ width: 470, fontSize: 20,fontFamily:"phetsarath ot" }}>ຂໍ້ມູນຜິດພາດ!!(ກະລຸນາປ້ອນຂໍ້ມູນໃຫ້ຖືກຕ້ອງ)
          </Alert>
        </Stack>
      </Snackbar>
    </div>
  );
}
