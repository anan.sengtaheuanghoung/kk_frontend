import './Stock-Display.css'
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button } from "@mui/material";
import { fetchStockReport } from '../../../../../../service/http/axios-client/Admin-Side/Report/Stock';
import { useEffect, useState } from 'react';
interface stockData {
  pro_id:string,
  pro_name:string,
  amount:string,
  pro_cost:string,
  pro_price:string,
  proType_name:string,
  pro_photo:string,

}
function StockDisplay () {
   //store state
   const [stockImport, setstockImport] = useState<stockData[]>([]);
   const [loading, setLoading] = useState<boolean>(true);
 
   const fetchBookingImport = async () => {
     try {
       const StockList: stockData[] = await fetchStockReport();
       setstockImport(StockList);
       setLoading(false);
     } catch (error) {
       console.error("Error fetching product data:", error);
     }
   };
  
   useEffect(() => {
     fetchBookingImport();
   }, []);
    return(
        <div className="StockDisplay">
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell  style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ຊື່ສິນຄ້າ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ຈໍານວນ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ລາຄາຕົ້ນທຶນ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ລາຄາສິນຄ້າ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ປະເພດສິນຄ້າ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ຮູບ</TableCell>

              </TableRow>
            </TableHead>
            <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={5}>Loading...</TableCell>
              </TableRow>                      
            ) : (
              stockImport.map((stock) => (
                <TableRow key={stock.pro_id}>
                  <TableCell className="Map-ReportOrder">
                    {stock.pro_name}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {stock.amount}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {stock.pro_cost}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {stock.pro_price}
                  </TableCell>    
                  <TableCell className="Map-ReportOrder">
                    {stock.proType_name}
                  </TableCell>  
                  <TableCell className="Map-ReportOrder">               
                    {typeof stock.pro_photo === "string" ? (
                      <img
                        src={stock.pro_photo}
                        alt="Employee Photo"
                        style={{
                          height: "65px",
                          width: "50px",
                          objectFit: "cover",
                        }}
                      />
                    ) : (
                      "No Photo"
                    )}
                  </TableCell>              
                </TableRow>
              ))
            )}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    )
}
export default StockDisplay