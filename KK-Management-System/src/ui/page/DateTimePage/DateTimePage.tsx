import Header from "../../style/Header";
import DateTimeSelection from "./component/DateTime-Selection";
import "./DateTimePage.css";
import Footer from "../../style/Footer";
 function DateTimePage() {
  
  return (
    <div className="DateTimePage">
      <Header  isLoggedIn={false} />
      <div className="DateTimeSelection">
        <DateTimeSelection />
      </div>
      {/* <div className="footer-DateTimePage">
        <Footer/>
      </div> */}
    </div>
  );
}
export default DateTimePage;
