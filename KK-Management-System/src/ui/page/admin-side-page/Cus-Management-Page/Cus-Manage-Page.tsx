import "./Cus-Manage-Page.css";
import SideBar from "../../../style/AdminPage-sideBar";
import CusManageForm from "./component/Cus-Manage-form";
import CusManageDisplay from "./component/Cus-Manage-display";
function CusManagePage() {
  return (
    <div className="cus-manage-page">
      <div className="cus-page-first-grid">
        <SideBar />
      </div>
      <div className="cus-page-second-grid">
        <CusManageForm />
        <CusManageDisplay />
      </div>
    </div>
  );
}
export default CusManagePage;
