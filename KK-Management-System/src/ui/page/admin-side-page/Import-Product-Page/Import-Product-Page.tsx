import './Import-Product-Page.css'
import SideBar from '../../../style/AdminPage-sideBar'
import ImportProductDisplay from './component/Import-Producrt-Display'
import ImportProductForm from './component/Import-Producrt-From'
import { SetStateAction, useState } from 'react';
function ImportProductPage () {
  const [searchQuery, setSearchQuery] = useState('');

  const handleSearch = (value: SetStateAction<string>) => {
    setSearchQuery(value);
  };
    return(
        <div className="ImportProductPage">
        <div className="ImportProductPage-first-grid">
          <SideBar />
        </div>
        <div className="ImportProductPage-second-grid">
        <ImportProductForm onSearch={handleSearch} />
        <ImportProductDisplay searchData={searchQuery} />

      </div>
      </div>
    )
}
export default ImportProductPage