import axios from "axios";

interface UseProtypeData {
    proType_name:string
  }
  
  export const SaveProType = ( proType_name:string,onSuccess: () => void,onUnsuccess: () => void,) => {
     const protypeData: UseProtypeData = {
        proType_name:proType_name,
    };
  
     axios
      .post("http://localhost:8080/product_type", protypeData,{        
        })  
      .then((response) => {
         console.log("Protype Save successful");
          console.log(response.data); 
          onSuccess();
  
      })
      .catch((err) => { 
        console.error("Error during SaveProductType:", err.message);
        onUnsuccess();
      });
  };




interface UpdatProtypeData {
  proType_name:string

}

export const EditProType = (
  proType:string,
  proType_name:string,
  onSuccess: () => void,
 onUnsuccess: () => void,
) => {
  const cusData: UpdatProtypeData = {
    proType_name:proType_name
  };

  axios
    .put(
      `http://localhost:8080/product_type/${proType}`,  
      cusData,
      {
        
      }
    )
    .then((response) => {
      console.log("Edit successful:", response.data);
      onSuccess();

    })
    .catch((err) => {
      console.error("Error during EditCus:", err.message);
      onUnsuccess();
    });
};





//DeleteProtype
export const DeleteProtype = (proType:string,
  onSuccess: () => void,
) => {
  axios
    .delete(`http://localhost:8080/product_type/${proType}`, {
    })
    .then((response) => {
      console.log("Deletion successful");
      console.log(response.data);
      onSuccess();
    })
    .catch((err) => {
      console.error("Error during deletion:", err);
    });
};


// export interface ProductType {
//   proType_id: string;
//   proType_name: string;
 
// }
// interface ApiResponse {
//   productType: ProductType[];
// }
// export const FetchProtype = async (): Promise<ProductType[]> => {
//   try {
//     const response = await axios.get<ApiResponse>("http://localhost:8080/branches");
//      return response.data.branches;  
//   } catch (error) {
//     console.error("Error during show data", error);
//     throw error;
//   }
// };


export interface ProductType {
  proType_id: string;  
  proType_name: string;  
}

export const FetchProtype = async (): Promise<ProductType[]> => {
  try {
    const response = await axios.get<ProductType[]>("http://localhost:8080/product_types");
    return response.data; // Return the raw data array
  } catch (error) {
    console.error("Error fetching product types:", error);

    throw new Error("Unable to fetch product types");
  }
};