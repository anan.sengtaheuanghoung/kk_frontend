import Header from "../../style/Header"
import Footer from "../../style/Footer"
import "./SignUp-Page.css"
import SignUpForm from "./component/SignUp-Form"
function SignUpPage () {
    return( 
    <div className="SignUpPage">
        <div>
            <Header isLoggedIn={false}   />
        </div>
        <div className="SignUpForm">
        <SignUpForm/>
        </div>
        {/* <Footer/> */}

    </div>
    )
}
export default SignUpPage

