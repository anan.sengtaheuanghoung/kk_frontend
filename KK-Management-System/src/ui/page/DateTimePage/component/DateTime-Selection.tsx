import "./DateTime-Selection.css";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DateCalendar } from "@mui/x-date-pickers/DateCalendar";
import { Button, colors } from "@mui/material";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { SubmitAppointment } from "../../../../service/http/axios-client/Appointment/CusAppointment";
import { BookingCode } from "../../../../service/http/axios-client/Appointment/BookingCode";
import { useState, useEffect, ChangeEvent } from "react";
import axios from "axios";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import dayjs, { Dayjs } from "dayjs";
import {
  getStoredToken,
  useUserProfileStore,
} from "../../../../service/http/axios-client/Login-axios";
import { useBookingCodeStore } from "../../../../service/http/axios-client/Appointment/BookingCode";

interface Params {
  branchId: string;
  employeeId: string;
  empName: string;
  [key: string]: string | undefined;
  branchName: string;
}

function DateTimeSelection() {
  // const {
  //   branchId = "",
  //   employeeId = "",
  //   empName = "",
  //   branchName = "",
  // } = useParams<Params>();


  // this is how to recive the value 
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const branchId = searchParams.get('branchId');
  const empId = searchParams.get('empId');
  const empName = searchParams.get('empName');
  const branchName = searchParams.get('branchName');
 


  const [getTime, setTime] = useState<string>("");
  const [getDate, setDate] = useState<string>("");
  const [bookedTimes, setBookedTimes] = useState<string[]>([]);

  const navigate = useNavigate();

  // Check the time button, if it was booked it will change to red
  useEffect(() => {
    const fetchBookedTimes = async () => {
      const authToken = getStoredToken();
      const headers = {
        Authorization: `Bearer ${authToken}`,
      };
      if (getDate) {
        try {
          const response = await axios.get(
            "http://localhost:8080/booked-times",
            {
              headers: headers,
              params: {
                date: getDate,
                employeeId: empId,
                branchId: branchId,
              },
            }
          );
          setBookedTimes(response.data.bookedTimes);
        } catch (error) {
          console.error("Error fetching booked times:", error);
        }
      }
    };
    fetchBookedTimes();
  }, [getDate, empId, branchId]);

  const handleTimeValue = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    const selectedTime = event.currentTarget.value;
    if (!bookedTimes.includes(selectedTime)) {
      setTime(selectedTime);
    }
  };

  //take cusId from login(provide data by zustand)
  const userProfileData = useUserProfileStore((state) => state.userData);
  const [cusid] = useState<string | undefined>(userProfileData?.cus_id);



 
  // Handle submit appointment
  const handleSubmit = async () => {
    if (empId && branchId && getTime && getDate) {
      try {
         await SubmitAppointment(empId, branchId, getTime, getDate);
         if (cusid) {
          await BookingCode(cusid); 
          navigate(`/UserBillPage`);
        }
       } catch (error) {
        console.error(
          "An error occurred during appointment submission:",
          error
        );
      }
    } else {
      console.error("Employee ID, branch ID, time, or date is missing.");
    }
  };








  // Set disable dateCalendar
  const isPastDate = (date: Dayjs): boolean => {
    const today = dayjs();
    return date.isBefore(today, "day");
  };
  // Determine the last date of the next month
  const maxDate = dayjs().add(1, "month").endOf("month");

  const isBeyondMaxDate = (date: Dayjs): boolean => {
    return date.isAfter(maxDate, "day");
  };
  // const getCurrentTime = (): Date => {
  //   return new Date();
  // };
  const formatTime = (time: string): string => {
    const [hour, minute] = time.split(":").map(Number);
    const ampm = hour >= 12 ? "PM" : "AM";
    const formattedHour = hour % 12 === 0 ? 12 : hour % 12;
    return `${formattedHour}:${minute.toString().padStart(2, "0")} ${ampm}`;
  };


  // const [currentTime, setCurrentTime] = useState<Date>(getCurrentTime());
  // useEffect(() => {
  //   const timer = setInterval(() => {
  //     setCurrentTime(getCurrentTime());
  //   }, 10000);

  //   return () => clearInterval(timer); // Cleanup on unmount
  // }, []);

  const isTimePast = (time: string, selectedDate: string): boolean => {
    const currentDate = dayjs();
    const buttonDate = dayjs(selectedDate)
      .set("hour", parseInt(time.split(":")[0]))
      .set("minute", parseInt(time.split(":")[1]))
      .set("second", parseInt(time.split(":")[2]));
    if (buttonDate.isAfter(currentDate)) {
      // If the selected date is in the future, no need to disable
      return false;
    }
    return true;
  };

  return (
    <div className="date-time-main-div">
      <div className="selection-detail">
        <div className="selection-content-first">
          <div className="deatail-first-div">
            <h3>ເລືອກ ວັນ ເເລະ ເວລາ</h3>
          </div>
          <div className="calendar-div">
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DateCalendar
                views={["year", "month", "day"]}
                className="datecalendar"
                onChange={(date) => {
                  const formattedDate = date?.format("YYYY-MM-DD");
                  if (formattedDate && !isPastDate(dayjs(formattedDate))) {
                    setDate(formattedDate);
                  }
                }}
                shouldDisableDate={(date) => {
                  const dayjsDate = date as Dayjs;
                  return isPastDate(dayjsDate) || isBeyondMaxDate(dayjsDate);
                }}
              />
            </LocalizationProvider>
          </div>
          <div className="deatail-third-div">
            <div className="third-div-title">
              <h3>ເລືອກເວລາຕັດຜົມ</h3>
            </div>

            <div className="third-div-grid">
              <div className="grid-style">
                <p>ເຊົ້າ</p>
                <div className="morning-grid-style-detail button-Style">
                  {["8:00:00", "9:00:00", "10:00:00", "11:00:00"].map(
                    (time) => {
                      const isDisabled =
                        bookedTimes.includes(time) || isTimePast(time, getDate);
                      return (
                        <Button
                          key={time}
                          variant="outlined"
                          value={time}
                          onClick={handleTimeValue}
                          disabled={isDisabled} // Disable button if time is booked or current time has passed
                          className={isDisabled ? "button-booked" : ""} // Conditionally apply booked class
                        >
                          {formatTime(time)}
                        </Button>
                      );
                    }
                  )}
                </div>
              </div>

              <div className="grid-style">
                <p>ສວາຍ</p>
                <div className="afternoon-grid-style-detail button-Style">
                  {[
                    "12:00:00",
                    "13:00:00",
                    "14:00:00",
                    "15:00:00",
                    "16:00:00",
                    "17:00:00",
                  ].map((time) => {
                    const isDisabled =
                      bookedTimes.includes(time) || isTimePast(time, getDate);
                    return (
                      <Button
                        key={time}
                        variant="outlined"
                        value={time}
                        onClick={handleTimeValue}
                        disabled={isDisabled} // Disable button if time is booked or current time has passed
                        className={isDisabled ? "button-booked" : ""} // Conditionally apply booked class
                      >
                        {formatTime(time)}
                      </Button>
                    );
                  })}
                </div>
              </div>

              <div className="grid-style">
                <p>ເເລງ</p>
                <div className="evening-grid-style-detail button-Style">
                  {["18:00:00", "19:00:00", "20:00:00"].map((time) => {
                    const isDisabled =
                      bookedTimes.includes(time) || isTimePast(time, getDate);
                    return (
                      <Button
                        key={time}
                        variant="outlined"
                        value={time}
                        onClick={handleTimeValue}
                        disabled={isDisabled} // Disable button if time is booked or current time has passed
                        className={isDisabled ? "button-booked" : ""} // Conditionally apply booked class
                      >
                        {formatTime(time)}
                      </Button>
                    );
                  })}
                </div>
              </div>
            </div>

            <div className="input-name-div">
              <Button
                variant="outlined"
                onClick={handleSubmit}
                disabled={!getTime || bookedTimes.includes(getTime)}
              >
                ຕົກລົງ
              </Button>
            </div>
          </div>
        </div>

        <div className="selection-content-second">
          <div className="selection-content-second-title">
            <p>ຊ່າງຕັດຜົມຂອງທ່ານ</p>
          </div>
          <div style={{ marginLeft: 30 }}>
            <p style={{ fontSize: 20 }}>ສາຂາ {branchName}</p>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                fontSize: 20,
                gap: 10,
              }}
            >
              <AccountCircleIcon />
              <p>{empName}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DateTimeSelection;
