import LoginForm from "./component/Login-form"
import "./Login-Page.css"
import Header from "../../style/Header"
import Footer from "../../style/Footer"
 
function LoginPage( ) {
  
    return (
        <div> 
        <div className="LoginPageMaindiv">            
            <div className="LoginForm"  >
                <LoginForm/>
            </div>             
        </div>
        {/* <div className="loginpage-footer">
            <Footer/>
        </div> */}
        </div>
    );
}

export default LoginPage;
