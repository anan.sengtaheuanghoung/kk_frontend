import "./Product-Management-Page.css"
import SideBar from "../../../style/AdminPage-sideBar"
import ProManageform from "./component/Pro-Manage-form"
import ProManageDisplay from "./component/Pro-Manage-display"
function ProManagePage () {
    return(
        <div className="pro-manage-page">
            <div className="pro-page-first-grid">
                <SideBar/> 

            </div>
            <div className="pro-page-second-grid">
                <ProManageform/>
                <ProManageDisplay/>
            </div>

        </div>
    )
}
export default ProManagePage