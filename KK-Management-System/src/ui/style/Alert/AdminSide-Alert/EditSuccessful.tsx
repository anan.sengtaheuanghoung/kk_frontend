import { Alert, Snackbar, Stack } from "@mui/material";
import { create } from "zustand";

interface useEditSuccessType {
    openEditSuccess: boolean;
    setopenEditSuccess: (open: boolean) => void;
}

export const useEditSuccess = create<useEditSuccessType>((set) => ({
    openEditSuccess: false,
    setopenEditSuccess: (openEditSuccess) => set({ openEditSuccess }),
}));

export const EditSuccess = () => {
  const { openEditSuccess , setopenEditSuccess } = useEditSuccess();

  const handleClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setopenEditSuccess(false);
  };
  return (
    <div>
      <Snackbar
        open={openEditSuccess}
        autoHideDuration={1200}
        onClose={handleClose}
        message="This Snackbar will be dismissed in 5 seconds."
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Stack sx={{ width: "100%" }} spacing={2}>
          <Alert
            severity="success"
            style={{ width: 200, fontSize: 20, fontFamily: "phetsarath ot" }}
          >
            ແກ້ໄຂສໍາເຫຼັດ
          </Alert>
        </Stack>
      </Snackbar>
    </div>
  );
};
