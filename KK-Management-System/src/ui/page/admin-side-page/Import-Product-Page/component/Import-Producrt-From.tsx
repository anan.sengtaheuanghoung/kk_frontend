import "./Import-Producrt-From.css";
 
import { Button, MenuItem, Select, SelectChangeEvent, TextField } from "@mui/material";
import React, { useState } from "react";
  

interface ImportProductFormProps {
  onSearch: (value: string) => void;  
}


function ImportProductForm({ onSearch }: ImportProductFormProps) {

  const [searchValue, setSearchValue] = useState('');

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setSearchValue(value);
    onSearch(value);  
  };
   return (
    <div>
      <div className="ImportProductForm">
        <div className="ImportProductForm-title">
          <h1>ນໍາເຂົ້າສິນຄ້າ </h1>
        </div>
        <div className="ImportProductForm-input">
          <div className="ImportProductForm-input-grid2 ">
            <p> ຄົ້ນຫາ:</p>
            <TextField
              value={searchValue}
              onChange={handleSearchChange}
            />
          </div>
          <div className="ImportProductForm-input-grid3">
            {/* <Button className="ImportProductForm-input-grid3-button">ຕົກລົງ</Button> */}
          </div>
        </div>
      </div>
    </div>
  );
}


export default ImportProductForm;
