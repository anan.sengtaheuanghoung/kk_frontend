import "./ProType-Management-Page.css"
import SideBar from "../../../style/AdminPage-sideBar"
import ProTypedisplay from "./component/ProType-display" 
import ProTypeform from "./component/ProType-form"

function ProTypePage () {
    return(  
        <div className="protype-manage-page">
              <div className="protype-page-first-grid">
                <SideBar/> 

            </div>
            <div className="protype-page-second-grid">
                <ProTypeform/>
                <ProTypedisplay/>



                
 
            </div>

        </div>
    )
}
export default ProTypePage