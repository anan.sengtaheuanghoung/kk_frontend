import "./Pro-Manage-form.css";
import { Button, MenuItem, Select, SelectChangeEvent } from "@mui/material";
import TextField from "@mui/material/TextField";
import React, { useEffect, useState } from "react";
import {
  Branch,
  FetchBranch,
} from "../../../../../service/http/axios-client/Appointment/CusAppointment";
import {
  FetchProtype,
  ProductType,
} from "../../../../../service/http/axios-client/Admin-Side/Product-Type-Management";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import {
  SaveProduct,
  EditProduct,
  DeleteProduct,
} from "../../../../../service/http/axios-client/Admin-Side/Product-Management";
import {
  SaveCompleteSnackbar,
  useSaveButtonAlert,
} from "../../../../style/Alert/AdminSide-Alert/SaveButtonAlert";
import {
  ErrorSaving,
  useErrorSaving,
} from "../../../../style/Alert/AdminSide-Alert/ErrorSaving";
import { useProductStore } from "./Pro-Manage-display";
import {
  EditSuccess,
  useEditSuccess,
} from "../../../../style/Alert/AdminSide-Alert/EditSuccessful";
import {
  ErrorEdit,
  useErrorEdit,
} from "../../../../style/Alert/AdminSide-Alert/ErrorEditing";
import {
  DeleteSuccess,
  useDeleteSuccess,
} from "../../../../style/Alert/AdminSide-Alert/DeleteSuccessful";
import { FetchBranchByUser } from "../../../../../service/http/axios-client/Admin-Side/EmployeeManagement";




function ProManageform() {
  //take state from zustand
  const { selectedData, setSelectedData } = useProductStore();

  //map out branch name
  const [branches, setBranches] = useState<Branch[]>([]);
  const [selectedBranch, setSelectedBranch] = useState<string>("");
 
  //map out Protype
  const [productTypes, setProductTypes] = useState<ProductType[]>([]);
  const [selectedProductType, setSelectedProductType] = useState<string>("");

  //this useEffect use to mapp a branch by each id
  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const branchesData = await FetchBranchByUser();
        setBranches(branchesData); // Correctly set the state
      } catch (error) {
        console.error("Error fetching branches:", error);
      }
    };

    fetchBranches();
  }, []);
  const handleMapbranch = (event: SelectChangeEvent<string>) => {
    setSelectedBranch(event.target.value);
  };

  //this useEffect use to mapp ProTypeName
  useEffect(() => {
    const fetchProductTypes = async () => {
      try {
        const data = await FetchProtype();
        setProductTypes(data);
      } catch (error) {
        console.error("Error fetching product types:", error);
      }
    };

    fetchProductTypes();
  }, []);

  // Event handler to track the selected product type
  const handleProductTypeChange = (event: SelectChangeEvent<string>) => {
    setSelectedProductType(event.target.value);
  };

  //alert that use from zustand
  const setopenSuccessCreateAccount = useSaveButtonAlert(
    (state) => state.setopenSuccessCreateAccount
  );
  const setUnsuccessOpen = useErrorSaving((state) => state.setUnsuccessOpen);
  const setopenEditSuccess = useEditSuccess(
    (state) => state.setopenEditSuccess
  );
  const setErrorEdit = useErrorEdit((state) => state.setErrorEdit);
  const setopenDeleteSuccess = useDeleteSuccess(
    (state) => state.setopenDeleteSuccess
  );

  //state for store a value
  const [proId, setId] = useState("");
  const [proName, setproName] = useState("");
  const [proPrice, setproPrice] = useState("");
  const [proCost, setproCost] = useState("");
  const [proPic, setPropic] = useState<File | null>(null);
  const [amount, setAmount] = useState("");
  const [price, setPrice] = useState("");

  
  //SAVE BUTTON
  const handleSave = () => {
    SaveProduct(
      proName,
      proPriceRaw,
      proCostRaw,
      selectedProductType,
      selectedBranch,
      amount,
      // price,
      proPic !== null ? proPic : null,
      () => {
        setSelectedData({
          proid: "",
          proName: "",
          proPrice: "",
          proCost: "",
          selectProtype: "",
          selectBranch: "",
          proPic: null,
          amount: "",
          status: "",
        });

        setopenSuccessCreateAccount(true);
      },
      () => {
        setUnsuccessOpen(true);
      }
    );
  };

  //Edit Button
  const handleEdit = () => {
    EditProduct(
      proId,
      proName,
      proPriceRaw,
      proCostRaw,
      selectedProductType,
      amount,
      proPic !== null ? proPic : null,
      () => {
        setSelectedData({
          proid: "",
          proName: "",
          proPrice: "",
          proCost: "",
          selectProtype: "",
          selectBranch: "",
          proPic,
          amount: "",
          status: "",
        });
        setopenEditSuccess(true);
      },
      () => {
        setErrorEdit(true);
      }
    );
  };

  //CLEAR BUTTON
  const handleClear = () => {
    setSelectedData({
      proid: "",
      proName: "",
      proPrice: "",
      proCost: "",
      selectProtype: "",
      selectBranch: "",
      proPic: null,
      amount: "",
      status: "",
    });
    setPropic(null);
  };
  //Delte button
  const handleDelete = () => {
    DeleteProduct(proId, () => {
      setSelectedData({
        proid: "",
        proName: "",
        proPrice: "",
        proCost: "",
        selectProtype: "",
        selectBranch: "",
        proPic:null,
        amount: "",
        status: "",
      });
      setopenDeleteSuccess(true);
    });
    setPropic(null)
  };

  //HANDLDE MAP DATA FROM TABLE ROW
  useEffect(() => {
    setId(selectedData.proid);
    setproName(selectedData.proName);
    setproPrice(selectedData.proPrice);
    setproCost(selectedData.proCost);
    setSelectedBranch(selectedData.selectBranch);
    setPropic(selectedData.proPic);
    setSelectedProductType(selectedData.selectProtype);
    setAmount(selectedData.amount);
    setPrice(selectedData.proPrice);
  }, [selectedData]);




   // State to store raw values without dots
   const [proPriceRaw, setProPriceRaw] = useState("");
    console.log("see  Proprice" , proPriceRaw)

   const [proCostRaw, setProCostRaw] = useState("");
   // Format number as user types
   const handlePriceChange = (e:any) => {
     const rawValue = e.target.value.replace(/\./g, ''); // Remove dots
     setProPriceRaw(rawValue);
     const formattedValue = formatNumber(e.target.value); // Format with dots
     setproPrice(formattedValue);
   };

    const handleCostChange = (e:any) => {
     const rawValue = e.target.value.replace(/\./g, ''); // Remove dots
     setProCostRaw(rawValue);
     const formattedValue = formatNumber(e.target.value); // Format with dots
     setproCost(formattedValue);
   };

   // Format number with dots as separators
   const formatNumber = (value:any) => {
     // Remove non-digit characters
     const numericValue = value.replace(/\D/g, '');
     // Add dots every three digits
     const formattedValue = numericValue.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
     return formattedValue;
   };


  return (
    <div className="ProManageForm">
      <div className="ProManageForm-title">
        <h1>ຈັດການສິນຄ້າ </h1>
      </div>
      <div className="ProManageForm-input">
        <div className="ProManageForm-input-grid1 ProManageForm-grid-direction">
          {/* <div className="ProManageForm-input-detail ">
            <p>ລະຫັດ </p>
            <div className="Pro-textfield ">
              <TextField disabled value={proId}></TextField>
            </div>
          </div> */}
          <div className="ProManageForm-input-detail">
            <p>ຊື່ສິນຄ້າ</p>
            <div className="Pro-textfield ">
              <TextField
                value={proName}
                onChange={(e) => setproName(e.target.value)}
              ></TextField>
            </div>
          </div>
          <div className="ProManageForm-input-detail">
            <p>ລາຄາສິນຄ້າ</p>
            <div className="Pro-textfield ">
              <TextField
                value={proPrice}
                onChange={handlePriceChange}
                onBlur={() => setproPrice(formatNumber(proPrice))}
                ></TextField>
            </div>
          </div>
          <div className="ProManageForm-input-detail">
            <p>ລາຄາຕົ້ນທຶນ</p>
            <div className="Pro-textfield ">
              <TextField
                value={proCost}
                onChange={handleCostChange}
                onBlur={() => setproCost(formatNumber(proCost))}
                ></TextField>
            </div>
          </div>
        </div>

        <div className="ProManageForm-input-grid2 ProManageForm-grid-direction">
          <div className="ProManageForm-input-detail">
            <p>ປະເພດສິນຄ້າ</p>
            <div className="Pro-textfield">
              <Select
                className="Pro-textfield-select"
                value={selectedProductType}
                onChange={handleProductTypeChange}
              >
                {productTypes.map((type) => (
                  <MenuItem key={type.proType_id} value={type.proType_id}>
                    {type.proType_name}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </div>
          <div className="ProManageForm-input-detail">
            <p>ສາຂາ</p>
            <div className="Pro-textfield">
              <Select
                className="Pro-textfield-select"
                value={selectedBranch}
                onChange={handleMapbranch}
              >
                {Array.isArray(branches) && branches.length > 0 ? (
                  branches.map((branch) => (
                    <MenuItem key={branch.branch_id} value={branch.branch_id}>
                      {branch.branch_name}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>No branches available</MenuItem>
                )}
              </Select>
            </div>
          </div>
          <div className="ProManageForm-input-detail">
            <p>ຈໍານວນ</p>
            <div className="Pro-textfield ">
              <TextField
                value={amount}
                onChange={(e) => setAmount(e.target.value)}
              ></TextField>
            </div>
          </div>
          {/* <div className="ProManageForm-input-detail">
            <p>ລາຄາ</p>
            <div className="Pro-textfield ">
              <TextField
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              ></TextField>
            </div>
          </div> */}
        </div>
        <div className="ProManageForm-input-grid3">
          <div>
            <p>ຮູບ</p>
            <div className="Pro-picturebox">
              {(typeof selectedData.proPic === "string" &&
                selectedData.proPic !== "") ||
              (proPic && typeof proPic !== "undefined") ? (
                <img
                  src={
                    typeof selectedData.proPic === "string"
                      ? selectedData.proPic
                      : URL.createObjectURL(proPic!)
                  }
                  alt="Employee Photo"
                  className="employee-photo"
                />
              ) : (
                <div>No photo available</div>
              )}
            </div>
          </div>
          <div>
            <Button
              component="label"
              role={undefined}
              variant="contained"
              tabIndex={-1}
              startIcon={<CloudUploadIcon />}
            >
              Upload file
              <input
                type="file"
                onChange={(e) =>
                  setPropic(e.target.files ? e.target.files[0] : null)
                }
                style={{ display: "none" }}
              />
            </Button>
          </div>
        </div>
      </div>
      <div className="ProManageForm-button">
        <Button onClick={handleSave}>ບັນທືກ</Button>
        <Button onClick={handleEdit}>ແກ້ໄຂ</Button>
        <Button onClick={handleClear}>ລ້າງ</Button>
        <Button onClick={handleDelete}>ລົບ</Button>
      </div>
      <SaveCompleteSnackbar />
      <ErrorSaving />
      <EditSuccess />
      <ErrorEdit />
      <DeleteSuccess />
    </div>
  );
}
export default ProManageform;
