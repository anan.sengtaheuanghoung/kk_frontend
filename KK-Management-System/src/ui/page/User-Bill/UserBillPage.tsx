import { useParams } from "react-router-dom";
import Footer from "../../style/Footer";
import Header from "../../style/Header";
import UserBillForm from "./component/UserBillForm";
import "./UserBillPage.css";

function UserBillPage() {
  
  return (
    <div className="UserBillPage">
    <div>
        <Header isLoggedIn={false}    />  
    </div>
    <div className="UserBillForm-div">
      <UserBillForm  />
     </div>
     {/* <div className="userbill-footer">
        <Footer    />  
    </div>
    */}
</div>
  );
}
export default UserBillPage;
