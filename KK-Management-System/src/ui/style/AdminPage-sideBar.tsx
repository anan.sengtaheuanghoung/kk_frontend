import "./AdminPage-sideBar.css";
import LOGO from "../../image/LOGO.png";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
 
  Typography,
} from "@mui/material";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { handleLogOut } from "../../service/http/axios-client/LogOut-axios";
import { useAuthStore } from "./Header";
import { useUserProfileStore } from "../../service/http/axios-client/Login-axios";

function SideBar() {
     
  const [expanded, setExpanded] = React.useState<string | false>(false);
  const setIsLoggedIn = useAuthStore((state) => state.setIsLoggedIn); 

  const handleChange =
      (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
        setExpanded(isExpanded ? panel : false);
  };

  const navigate = useNavigate();
  const LogOut = () =>{
      handleLogOut();
      localStorage.clear();
      navigate('/')
      setIsLoggedIn(false);
  }
                  
  //Access to get profile user data by zustand
  const userProfileData = useUserProfileStore(state => state.userData); 
  const [userDataLoaded, setUserDataLoaded] = React.useState(false);
  useEffect(() => {
    // Check if user data exists in local storage
    const userDataFromLocalStorage = localStorage.getItem('userData');
    if (userDataFromLocalStorage) {
      // Update Zustand store with user data from local storage
      useUserProfileStore.getState().setUserData(JSON.parse(userDataFromLocalStorage));
      setUserDataLoaded(true);
    }
  }, []);



  //take cusId from login(provide data by zustand)
  const [role] = useState<string | undefined>(userProfileData?.role);
  console.log("see role----", role);
 
 

  
  return (
    <div className="sidebar-main-div">
      <div className="sidebar">
        <div className="sidebar-first-grid">
          <h1> KICKS & KUTS</h1>
        </div>
        <div className="sidebar-second-grid">
          <img src={userProfileData?.emp_photo} alt="LOGO pic" />
          <p> {userProfileData?.emp_name}</p>
        </div>
        {(role !== 'ceo' && role !== 'employee')&&( 
        <div className="sidebar-third-grid">
          <p onClick={() =>{ navigate('/AdminDashBoardPage')}} >ໜ້າຫຼັກ</p>
        </div>
        )}
       
        <div className="sidebar-fourth-grid">
        {(role !== 'ceo' && role !== 'employee')&&( 

          <Accordion
            expanded={expanded === 'panel1'} onChange={handleChange('panel1')}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon  style={{color:"white"}}/>}
              aria-controls="panel1-content"
              id="panel1-header"
              className=" fourth-grid-first-header"
            >
              <Typography className="Title-font" >ຈັດການ</Typography>
            </AccordionSummary>
            
           
            <AccordionDetails className="fourth-grid-detail" >
              <Typography className="sidebar-Typography" ><a href="/EmpManagementPage">ຈັດການ ພະນັກງານ</a> </Typography>
              <Typography  className="sidebar-Typography"><a href="/CusManagePage">ຈັດການ ລູກຄ້າ</a></Typography>
              <Typography  className="sidebar-Typography"><a href="/ProManagePage">ຈັດການ ສິນຄ້າ</a></Typography>
              <Typography  className="sidebar-Typography"><a href="/ProTypePage">ຈັດການ ປະເພດຂອງສິນຄ້າ</a></Typography>
              <Typography  className="sidebar-Typography"><a href="/AppointmentManagePage">ຈັດການ ການຈອງ</a> </Typography>
              <Typography  className="sidebar-Typography"><a href="/SupManagePage">ຈັດການ  ຜູ້ສະໜອງ</a></Typography>
              {role === 'ceo' && (<Typography  className="sidebar-Typography"><a href="/BranchManagePage">ຈັດການ ສາຂາ</a></Typography> )}
            </AccordionDetails>            
          </Accordion>
        )}
               {role === 'ceo' && (
              <AccordionDetails className="fourth-grid-detail">
                <Typography className="sidebar-Typography">
                  <p onClick={() =>{ navigate('/BranchManagePage')}}>ຈັດການ ສາຂາ</p>
                  
                </Typography>
                <Typography className="sidebar-Typography" ><p  onClick={() =>{ navigate('/EmpManagementPage')}}  >ຈັດການ ພະນັກງານ</p> </Typography>

              </AccordionDetails>
            )}
          {(role !== 'ceo' && role !== 'employee')&& ( 
            <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon style={{color:"white"}} />}
                aria-controls="panel2-content"
                id="panel2-header"
                className="fourth-grid-first-header"
              >
                <Typography className="Title-font">ນໍາເຂົ້າ ເເລະ ສັ່ງຊື້</Typography>
              </AccordionSummary>
              <AccordionDetails className="fourth-grid-detail">
                <Typography className="sidebar-Typography"><a href="/OrderProduct">ສັ່ງຊື້ ສິນຄ້າ</a></Typography>
                <Typography className="sidebar-Typography"><a href="/ImportProductPage">ນໍາເຂົ້າ ສິນຄ້າ</a></Typography>
              </AccordionDetails>
            </Accordion>
            )}
          {(role !== 'ceo' && role !== 'employee')&&( 
          <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon style={{color:"white"}} />}
              aria-controls="panel2-content"
              id="panel2-header"
              className=" fourth-grid-first-header"
            >
              <Typography className="Title-font">ບໍລິການ </Typography>
            </AccordionSummary>
            <AccordionDetails className="fourth-grid-detail">
                
              <Typography  className="sidebar-Typography"><a href="/BookingSevice">ຈອງ</a> </Typography>
              <Typography  className="sidebar-Typography"><a href="/AddBookingPage">ໃຊ້ບໍລິການ</a></Typography>
            </AccordionDetails>
          </Accordion>
          )}
          {(role !== 'ceo' && role !== 'employee')&&( 
          <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon style={{color:"white"}} />}
              aria-controls="panel2-content"
              id="panel2-header"
              className=" fourth-grid-first-header"
            >
              <Typography className="Title-font">ລາຍງານ</Typography>
            </AccordionSummary>
            <AccordionDetails className="fourth-grid-detail"> 
              <Typography  className="sidebar-Typography"><a href="/ReportOrderPage">ລາຍງານ ສັ່ງຊື້</a></Typography>
              <Typography  className="sidebar-Typography"><a href="/ReportImportProduct">ລາຍງານ ນໍາເຂົ້າ</a></Typography>
              <Typography  className="sidebar-Typography"><a href="/ReportBillPage">ລາຍງານ ບິນ</a></Typography>
              <Typography  className="sidebar-Typography"><a href="/IncomePage">ລາຍງານ ລາຍຮັບ</a></Typography>
              <Typography  className="sidebar-Typography"><a href="/Expenesepage">ລາຍງານ ລາຍຈ່າຍ</a></Typography>
              <Typography  className="sidebar-Typography"><a href="/BookingPage">ລາຍງານ ຈອງ</a></Typography>
              <Typography  className="sidebar-Typography"><a href="/StockPage">ລາຍງານ ສິນຄ້າໃກ້ໝົດ</a></Typography>
            </AccordionDetails>      
          </Accordion>
          )}
          {role === 'employee' && (
            <AccordionDetails className="fourth-grid-detail">             
              <Typography  className="sidebar-Typography"><p onClick={() =>{ navigate('/BookingPage')}} >ລາຍງານ ຈອງ</p></Typography>
            </AccordionDetails>
            )}
        </div>
        <div className="sidebar-fifth-grid">
            <Button onClick={LogOut}> ອອກຈາກລະບົບ</Button>
        </div>
       
      </div>
    </div>
  );
}
export default SideBar;
