import "./ProType-form.css";
import TextField from "@mui/material/TextField";
import { Button } from "@mui/material";
import { useEffect, useState } from "react";
import { SaveProType } from "../../../../../service/http/axios-client/Admin-Side/Product-Type-Management";
import { SaveCompleteSnackbar,useSaveButtonAlert } from "../../../../style/Alert/AdminSide-Alert/SaveButtonAlert";
import { ErrorSaving,useErrorSaving } from "../../../../style/Alert/AdminSide-Alert/ErrorSaving";
import { useProTypeStore } from "./ProType-display";
import { EditProType } from "../../../../../service/http/axios-client/Admin-Side/Product-Type-Management";
import {EditSuccess, useEditSuccess } from "../../../../style/Alert/AdminSide-Alert/EditSuccessful";
import { ErrorEdit,useErrorEdit } from "../../../../style/Alert/AdminSide-Alert/ErrorEditing";
import { DeleteProtype } from "../../../../../service/http/axios-client/Admin-Side/Product-Type-Management";
import { DeleteSuccess,useDeleteSuccess } from "../../../../style/Alert/AdminSide-Alert/DeleteSuccessful";
function ProTypeform() {
  //take state from zustand
  const { selectedData, setSelectedData } = useProTypeStore();

  //State that store value 
  const [proType_id, setId] = useState("");
  const [ProTypeName,setProTypeName] = useState("")

  //alert fucntion that use from zustand
  const setopenSuccessCreateAccount = useSaveButtonAlert((state) => state.setopenSuccessCreateAccount);
  const setUnsuccessOpen = useErrorSaving((state) => state.setUnsuccessOpen);
  const setopenEditSuccess = useEditSuccess((state) => state.setopenEditSuccess);
  const setErrorEdit = useErrorEdit((state) => state.setErrorEdit);
  const setopenDeleteSuccess = useDeleteSuccess((state) => state.setopenDeleteSuccess);


  //Save Button
  const handleSave =() =>{
    SaveProType(
      ProTypeName,
      () =>{
        setSelectedData({
          proType_id: "",
          proType_name: ""
        });
        setopenSuccessCreateAccount(true);  
      },
      () => {
        setUnsuccessOpen(true);  
      }
    )
  

  }

  
  //EDIT BUTTON
  const handleEdit = () => {
    EditProType(
      proType_id,
      ProTypeName,
      () => {
        setSelectedData({
          proType_id: "",
          proType_name: ""
        });
        setopenEditSuccess(true);  
      },
      () => {
        setErrorEdit(true);  
      },
    );
  };

  //CLEAR BUTTON
   const handleClear = () => {
    setSelectedData({
      proType_id: "",
      proType_name: ""
    });
   };

  //DELETE BUTTON
  const handleDelete = () => {
    DeleteProtype(proType_id,
      () => {
        setSelectedData({
          proType_id: "",
          proType_name: ""
        });
        setopenDeleteSuccess(true);  
      },
    );

     
  };

   //HANDLDE MAP DATA FROM TABLE ROW
   useEffect(() => {
    console.log("Selected to see pic data :", selectedData);
    setId(selectedData.proType_id);
    setProTypeName(selectedData.proType_name);
  }, [selectedData]);



  return (
    <div className="ProtypeForm">
      <div className="ProtypeForm-title">
        <h1>ຈັດການປະເພດສິນຄ້າ </h1>
      </div>
      <div className="ProtypeForm-input">
        {/* <div className="ProtypeForm-input-detail  ProtypeForm-input-detail-margin">
          <p>ລະຫັດປະເພດສິນຄ້າ:</p>
          <TextField value={proType_id} disabled
                onChange={(e) => setId(e.target.value)}> </TextField>
        </div> */}
        <div className="ProtypeForm-input-detail  ProtypeForm-input-detail-margin">
          <p>ຊື່ປະເພດສິນຄ້າ:</p>
          <TextField  value={ProTypeName} onChange={(e)=> setProTypeName(e.target.value)}> </TextField>
        </div>
      </div>
      <div className="ProtypeManageForm-button">
        <Button onClick={handleSave}>ບັນທຶກ</Button>
        <Button onClick={handleEdit}>ເເກ້ໄຂ</Button>
        <Button onClick={handleClear}>ລ້າງ</Button>
        <Button onClick={handleDelete}>ລົບ</Button>    
      </div>
      <SaveCompleteSnackbar/>
      <ErrorSaving/>
      <EditSuccess/>
      <ErrorEdit/>
      <DeleteSuccess/>
    </div>
  );
}
export default ProTypeform;
