import { Button } from "@mui/material";
import "./UserBillForm.css";
import CanclePop from "./CancelPopup";
import { Link, Params, useLocation, useNavigate, useParams } from "react-router-dom";
import {  useBookingCodeStore } from "../../../../service/http/axios-client/Appointment/BookingCode";
import { useEffect } from "react";


function UserBillForm() {

  const bookingCodes = useBookingCodeStore((state) => state.bookingCodes);
  useEffect(() => {
    console.log('Booking Codes:', bookingCodes);
   },[bookingCodes])
  
  const navigate = useNavigate();
  const handleClick = () => {
    navigate("/");
  };

  
  //another one way to send data by param
  //ao kha jark param
  // const location = useLocation();
  // const searchParams = new URLSearchParams(location.search);
  // const appid = searchParams.get('appid');
 


  return (
    <div className="UserBillForm">
      <h1>{bookingCodes ? "ຂອບໃຈທີ່ໃຊ້ບໍລິການ" : ""}</h1>
      <h4>
      <h4>
      {bookingCodes  ? "ໂຄດ ຂອງທ່ານ: " + bookingCodes : <p  style={{ fontSize:20,marginLeft:70}}>ທ່ານຍັງບໍ່ການຈອງ: <Link to="/BranchPage" style={{color:"red"}}> ກະລຸນາຈອງຄິວກ່ອນ</Link></p>}
      </h4>
      </h4>
      <div className="UserBillForm-button-div">
        <button className="UserBillForm-button" type="button" onClick={handleClick}>
          ໜ້າຫຼັກ
        </button>
        {bookingCodes  ? <Button>
          <CanclePop  />
        </Button> : ""}
      </div>
    </div>
  );
}
export default UserBillForm;
