 import React, { useState, useRef } from 'react';
import { styled, css } from '@mui/system';
import { Modal as BaseModal } from '@mui/base/Modal';
import { useReactToPrint } from 'react-to-print';
import {
  Backdrop,
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';
import './Report-Bill-PopUp.css'
import LOGO from "../../../../../../image/LOGO.png";
import { fetchServiceBill } from '../../../../../../service/http/axios-client/Admin-Side/Report/Service';
const blue = {
  200: "#99CCFF",
  300: "#66B2FF",
  400: "#3399FF",
  500: "#007FFF",
  600: "#0072E5",
  700: "#0066CC",
};

const grey = {
  50: "#F3F6F9",
  100: "#E5EAF2",
  200: "#DAE2ED",
  300: "#C7D0DD",
  400: "#B0B8C4",
  500: "#9DA8B7",
  600: "#6B7A90",
  700: "#434D5B",
  800: "#303740",
  900: "#1C2025",
};

const Modal = styled(BaseModal)`
  position: fixed;
  z-index: 1300;
  inset: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledBackdrop = styled(Backdrop)`
  z-index: -1;
  position: fixed;
  inset: 0;
  background-color: rgb(0 0 0 / 0.5);
  -webkit-tap-highlight-color: transparent;
`;

const ModalContent = styled("div")(
  ({ theme }) => css`
    font-family: "IBM Plex Sans", sans-serif;
    font-weight: 500;
    text-align: start;
    position: relative;
    display: flex;
    flex-direction: column;
    gap: 8px;
    width: 500px;
    height: auto;
    overflow: auto;
    background-color: ${theme.palette.mode === "dark" ? grey[900] : "white"};
    border-radius: 8px;
    box-shadow: 0 4px 12px
      ${theme.palette.mode === "dark" ? "rgb(0 0 0 / 0.5)" : "rgb(0 0 0 / 0.2)"};
    padding: 24px;
    color: ${theme.palette.mode === "dark" ? grey[50] : grey[900]};

    & .modal-title {
      margin: 0;
      line-height: 1.5rem;
      margin-bottom: 8px;
    }

    & .modal-description {
      margin: 0;
      line-height: 1.5rem;
      font-weight: 400;
      color: ${theme.palette.mode === "dark" ? grey[400] : grey[800]};
      margin-bottom: 4px;
    }
  `
);

const TriggerButton = styled("button")(
  ({ theme }) => css`
    font-family: "phetsarath ot";
    font-weight: 400;
    font-size: 18px;
    line-height: 1.5;
    padding: 8px 16px;
    border-radius: 8px;
    transition: all 150ms ease;
    cursor: pointer;
    background:  #D9D9D9;
    color: black;
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);

    &:hover {
      background: "grey";
      border-color: ${theme.palette.mode === "dark" ? grey[600] : grey[300]};
    }

    &:active {
      background: ${theme.palette.mode === "dark" ? grey[700] : grey[100]};
    }

    &:focus-visible {
      box-shadow: 0 0 0 4px
        ${theme.palette.mode === "dark" ? blue[300] : blue[200]};
      outline: none;
    }
  `
);
interface RecieveData {
  SerId: string;
  employeeName: string;
  Date:string;
  branchName:string;
}
interface ServiceListBill{
  ser_id:string,
  admin_name:string,
  employee_name:string,
  app_id:string,
  pro_name:string,
  amount:string,
  price:string,
  pro_price:string,
  branch_name:string,
  ser_date:string,
  ser_total:string,
  total_price:string,



}
export default function ReportBillPopUP({SerId,employeeName,Date,branchName}:RecieveData) {
  const [open, setOpen] = useState(false);
  const contentRef = useRef<HTMLDivElement | null>(null);
  const [totalPrice, setTotalPrice] = useState<number>(0); // State for total price

  // const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handlePrint = useReactToPrint({
    content: () => contentRef.current,
    documentTitle: "Report Order", // Optional title for the printout
  });

  const [serviceBill, setOrderBill] = useState<ServiceListBill[]>([]);
  const [appId, setAppId] = useState(''); // State for app_id
  
  const handleChange = async () => {
    setOpen(true);
    try {
      const response = await fetchServiceBill(SerId);
      setAppId(response.result.app_id)
      setOrderBill(response.result); // Assuming result holds the array of service bills
      setTotalPrice(response.result[0]?.ser_total || '0'); // Extracting total price from the first item in the result array
    } catch (error) {
      console.error("Error fetching service bill:", error);
    }
  };
  return (
    <div>
      <TriggerButton onClick={handleChange}>ເລືອກ</TriggerButton> {/* Opens the modal */}
      <Modal
        open={open}
        onClose={handleClose}
        slots={{ backdrop: StyledBackdrop }}
      >
         
        <ModalContent  >
          <div className="Report-Bill-Maindiv"  ref={contentRef}>
            <div className="Bill-popup-title">
            <img src={LOGO}/>
            <p>KICKS & KUTS</p>

            </div>
            <div className="Bill-popup-showID">
              <div className="Bill-showid-grid1">
                <p>ລະຫັດ:{SerId} </p>
                
                <p>ຊ່າງຕັດຜົມ:{employeeName} </p>
              </div>
              <div className="Bill-showid-grid2">
              {/* {serviceBill.map((service) => ( */}
                {/* <p   >ລະຫັດການຈອງ:{appId}  </p> */}
              {/*   ))} */}
              </div>
            </div>
            <div className="Bill-popup-showList">
              <TableContainer  component={Paper}>
                <Table
                  sx={{ minWidth: 460 }}
                  size="small"
                  aria-label="a dense table"      
                >
                  <TableHead>
                    <TableRow>
                    <TableCell className="TableHead-style-forBill" >ລໍາດັບ</TableCell>
                      <TableCell className="TableHead-style-forBill" >ຊື່ສິນຄ້າ</TableCell>
                      <TableCell className="TableHead-style-forBill">ຈໍານວນ</TableCell>
                      <TableCell className="TableHead-style-forBill">ລາຄາ</TableCell>
                      <TableCell className="TableHead-style-forBill" >ລາຄາລວມ</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody className='Bill-table-body'>
                  {serviceBill.map((service, index) => (
                      <TableRow
                        key={service.ser_id}
                        sx={{
                          "&:last-child td, &:last-child th": { border: 0 },
                        }}
                      >
                        <TableCell align="center" >{index + 1}</TableCell>

                        <TableCell align="center">
                          {service.pro_name}
                        </TableCell>
                        <TableCell align="center">
                          {service.amount}
                        </TableCell>
                        <TableCell align="center">
                          {service.price}
                        </TableCell>
                        <TableCell align="center">{service.total_price}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
            <div className="Bill-popup-branch">
              <div className='bill-popup-total'>
                <p>ລາຄາລວມ:{totalPrice}</p>
              </div>
              <p className="">ສາຂາ:{branchName} </p>
              <p className="">ວັນທີ:{Date} </p>
            </div>
          </div>
          <div className="bill-print-button">
            <Button onClick={handlePrint}>Print</Button>  
          </div>
        </ModalContent>
      </Modal>
    </div>
  );
}