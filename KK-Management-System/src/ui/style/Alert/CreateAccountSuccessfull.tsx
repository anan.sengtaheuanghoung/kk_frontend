import { Alert, Snackbar, Stack } from "@mui/material";
import { create } from "zustand";

interface SnackbarState {
openSuccessCreateAccount: boolean;
setopenSuccessCreateAccount: (open: boolean) => void;
}

export const useSuccessCreateAccount = create<SnackbarState>((set) => ({
    openSuccessCreateAccount: false,
    setopenSuccessCreateAccount: (openSuccessCreateAccount) => set({ openSuccessCreateAccount }),
}));

export const CreateAccount = () => {
  const { openSuccessCreateAccount , setopenSuccessCreateAccount } = useSuccessCreateAccount();

  const handleClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setopenSuccessCreateAccount(false);
  };

  return (
    <div>
      <Snackbar
        open={openSuccessCreateAccount}
        autoHideDuration={1200}
        onClose={handleClose}
        message="This Snackbar will be dismissed in 5 seconds."
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Stack sx={{ width: "100%" }} spacing={2}>
          <Alert
            severity="success"
            style={{ width: 200, fontSize: 20, fontFamily: "phetsarath ot" }}
          >
            ສ້າງບັນຊີສໍາເຫຼັດ
          </Alert>
        </Stack>
      </Snackbar>
    </div>
  );
};
