import './Import-Producrt-Display.css'
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button, styled } from "@mui/material";
import { FetchOrderList } from '../../../../../service/http/axios-client/Admin-Side/Order-and-Import/Import';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
 
 

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));


interface orderListProduct {
  order_id:string,
  order_date:string,
  sup_name:string,
  sup_pnumber:string,
}

interface ImportProductDisplayProps {
  searchData: string; // Define the type for the searchData prop
}
function ImportProductDisplay ({ searchData }: ImportProductDisplayProps) {

  const [orderList, setOrderList] = useState<orderListProduct[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [searchQuery, setSearchQuery] = useState('');

  const fetchOrderProducts = async () => {
    try {
      const products: orderListProduct[] = await FetchOrderList();
      setOrderList(products);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching product data:", error);
    }
  };

  useEffect(() => {
    fetchOrderProducts();
  }, []);

  useEffect(() => {
    setSearchQuery(searchData);
  }, [searchData]); // Update searchQuery when searchData changes
 
  const filteredOrderList = orderList.filter(product =>
    product.order_id.toLowerCase().includes(searchQuery.toLowerCase()) ||
    product.order_date.toLowerCase().includes(searchQuery.toLowerCase()) ||
    product.sup_name.toLowerCase().includes(searchQuery.toLowerCase()) ||
    product.sup_pnumber.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const navigate = useNavigate();
  
    return(
        <div className="ImportProductDisplay">
        <TableContainer  sx={{ maxHeight: 530 }} >
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell    className='Map-orderList-tablecell' align="center" >ລະຫັດ</TableCell>
                <TableCell    className='Map-orderList-tablecell' align="center">ວັນທີ</TableCell>
                <TableCell   className='Map-orderList-tablecell' align="center">ຊື່ຜູ້ສະໜອງ </TableCell>
                <TableCell   className='Map-orderList-tablecell' align="center">ເບີໂທ</TableCell>
                <TableCell   className='Map-orderList-tablecell'align="center"> </TableCell>
                <TableCell   className='Map-orderList-tablecell' align="center"> </TableCell>
                </TableRow>
            </TableHead>
       
            <TableBody >
             {loading ? (
                  <StyledTableRow>
                    <StyledTableCell colSpan={5}>Loading...</StyledTableCell>
                  </StyledTableRow>
                ) : (
                  filteredOrderList.map((product) => (
                    <StyledTableRow key={product.order_id}>
                      <StyledTableCell className='Map-orderList' align="center">{product.order_id}</StyledTableCell>
                      <StyledTableCell className='Map-orderList' align="center" >{product.order_date}</StyledTableCell>
                      <StyledTableCell className='Map-orderList' align="center">{product.sup_name}</StyledTableCell>
                      <StyledTableCell className='Map-orderList' align="center">{product.sup_pnumber}</StyledTableCell>
                      <StyledTableCell align="center" style={{width:0}}>
                    <Button className="ImportProductDisplay-edit-button" onClick={() => {
                      navigate(`/InputImportProduct/${product.order_id}`)
                                }} >ຖັດໄປ</Button>
                  </StyledTableCell>
                  <StyledTableCell align="center" style={{width:10}}>
                    {/* <Button className="ImportProductDisplay-edit-button">ເບິ່ງ</Button> */}
                  </StyledTableCell>
                    </StyledTableRow>
                  ))
                )}
            </TableBody>
            
          </Table>
        </TableContainer>
      </div>
    )
}
export default ImportProductDisplay