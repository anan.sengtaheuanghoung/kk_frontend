import './Sup-Management-Page.css'
import SideBar from '../../../style/AdminPage-sideBar'
import SupDisPlay from './component/Sup-Display'
import SupForm from './component/Sup-Form'
function SupManagePage () {
    return(
        <div className="sup-manage-page">
            <div className="sup-page-first-grid">
                <SideBar/> 
            </div>
            <div className="sup-page-second-grid">
                <SupForm/>
                <SupDisPlay/>

                

            </div>

        </div>
    )
}
export default SupManagePage