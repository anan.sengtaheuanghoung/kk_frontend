import "./Report-Order-Form.css";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers-pro";
import { AdapterDayjs } from "@mui/x-date-pickers-pro/AdapterDayjs";
import { DateRangePicker } from "@mui/x-date-pickers-pro/DateRangePicker";
import { Button, TextField, debounce } from "@mui/material";
import React, { useCallback, useState } from "react";
import { Dayjs } from "dayjs";

interface OrderFormProps {
  onSearch: (value: string) => void;
  onDateChange: (startDate: Dayjs | null, endDate: Dayjs | null) => void;
}

function ReportOrderForm({ onSearch, onDateChange }: OrderFormProps) {
  //handle search by text
  const [searchValue, setSearchValue] = useState("");
  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setSearchValue(value);
    onSearch(value);
  };

  //handle date Range picker
  const [startDate, setStartDate] = useState<Dayjs | null>(null);
  const [endDate, setEndDate] = useState<Dayjs | null>(null);

  const handleStartDateChange = (date: Dayjs | null) => {
    setStartDate(date);
    onDateChange(date, endDate);
  };

  const handleEndDateChange = (date: Dayjs | null) => {
    setEndDate(date);
    onDateChange(startDate, date);
  };

  return (
    <div>
      <div className="ReportOrder-Form">
        <div className="ReportOrder-title">
          <p>ລາຍງານສັ່ງຊື້ສິນຄ້າ </p>
        </div>
        <div className="ReportOrder-input">
          <div className="ReportOrder-grid1 ">
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={["DatePicker"]}>
                <DatePicker
                  label="ຈາກວັນທີ"
                  value={startDate}
                  onChange={handleStartDateChange}
                  format="DD/MM/YYYY"
                
                />
              </DemoContainer>
            </LocalizationProvider>
            _
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={["DatePicker"]}>
                <DatePicker
                  value={endDate}
                  onChange={handleEndDateChange}
                  label="ເຖິງວັນທີ"
                  format="DD/MM/YYYY"
                 
                />
              </DemoContainer>
            </LocalizationProvider>
          </div>
          <div className="ReportOrder-grid2 ">
            <div className="ReportOrder-grid2-content ">
              <p> ຄົ້ນຫາ:</p>
              <TextField
                value={searchValue}
                onChange={handleSearchChange}
              ></TextField>
            </div>
            <div className="ReportOrder-grid2-content "></div>
          </div>
          <div className="ReportOrder-grid3">
            {/* <Button>ຕົກລົງ</Button> */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default ReportOrderForm;

{
  /* <LocalizationProvider  dateAdapter={AdapterDayjs}>
              <DemoContainer  components={["DateRangePicker"]}>
                <DateRangePicker
                 className="reportOrder-dateRange"
                  localeText={{ start: "From-Date", end: "To-Date" }}
                  slotProps={{
                    textField: ({ position }) => ({
                      color: position === "start" ? "success" : "warning",
                      focused: true,
                      
                    }),
                  }}
                   
                  defaultValue={[dayjs("12-04-2002"), null]}
                />
              </DemoContainer>
            </LocalizationProvider> */
}
