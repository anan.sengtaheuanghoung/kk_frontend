import '../../Admin-Booking-Page/component/Admin-Booking-Form.css'
import { Button, MenuItem, Select, SelectChangeEvent, TextField } from "@mui/material";
import React, { ChangeEvent, useState } from "react";
import { useNavigate } from 'react-router-dom';
interface serviceFormProps {
  onSearch: (value: string) => void;  
}
function AddBooking ( { onSearch }: serviceFormProps ){

  const [searchValue, setSearchValue] = useState('');
  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setSearchValue(value);
    onSearch(value);  
  };

    //Navigator
    const navigate = useNavigate()

    return (
      <div>
        <div className="AddBookingForm">
          <div className="AddBookingForm-title">
            <p>ຈອງຄິວຕັດຜົມ</p>
          </div>
          <div className="AddBookingForm-input">
            <div className="AddBookingForm-grid1 ">
            <div  className="AddBookingForm-grid2-content ">
              <p>ໂຄດ:</p>
              <TextField   
               value={searchValue}
               onChange={handleSearchChange}
              fullWidth></TextField>
              </div>
              <div  className="AddBookingForm-grid2-content ">
       
              <div className="AddBookingForm-grid3-button">
                <Button onClick={() => {
                  navigate(`/AdminAppointmentPage`)
                }}>ຈອງ</Button>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
}
export default AddBooking