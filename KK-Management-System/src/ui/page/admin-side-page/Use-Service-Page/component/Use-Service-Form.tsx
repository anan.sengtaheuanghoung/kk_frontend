import { Button } from "@mui/material";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import "./Use-Service-Form.css";
import UseServiceFormPopUp from "./Use-Service-Form-pop-up";
import { FetchUseServicedata } from "../../../../../service/http/axios-client/Admin-Side/Service/Service";
import { useEffect, useState } from "react";
import {
  Unstable_NumberInput as BaseNumberInput,
  NumberInputProps,
} from "@mui/base/Unstable_NumberInput";
import { styled } from "@mui/system";
import RemoveIcon from "@mui/icons-material/Remove";
import AddIcon from "@mui/icons-material/Add";
import * as React from "react";
import { Params, useNavigate, useParams } from "react-router-dom";
import { ServicePost } from "../../../../../service/http/axios-client/Admin-Side/Service/Service";
import { useBillSuccessAlert ,BillSuccessSnackbar } from "../../../../style/Alert/AdminSide-Alert/Bill-Successful";
import { useErrorbill , BillError} from "../../../../style/Alert/AdminSide-Alert/Bill-Unsuccess";
interface Product {
  pro_id: string;
  pro_name: string;
  pro_price: string;
  pro_photo: string;
}

const NumberInput = React.forwardRef(function CustomNumberInput(
  props: NumberInputProps,
  ref: React.ForwardedRef<HTMLDivElement>
) {
  return (
    <BaseNumberInput
      slots={{
        root: StyledInputRoot,
        input: StyledInput,
        incrementButton: StyledButton,
        decrementButton: StyledButton,
      }}
      slotProps={{
        incrementButton: {
          children: <AddIcon fontSize="small" />,
          className: "increment",
        },
        decrementButton: {
          children: <RemoveIcon fontSize="small" />,
        },
      }}
      {...props}
      ref={ref}
    />
  );
});

const today = new Date();
const month = today.getMonth() + 1;
const year = today.getFullYear();
const date = today.getDate();
const currentDate = month + "/" + date + "/" + year;

function getDate() {
  const today = new Date();
  const month = today.getMonth() + 1;
  const year = today.getFullYear();
  const date = today.getDate();
  // Adding leading zero if month or date is less than 10
  const formattedMonth = month < 10 ? `0${month}` : month;
  const formattedDate = date < 10 ? `0${date}` : date;
  const changeFomat = `${year}-${formattedMonth}-${formattedDate}`;
   
  
  return {
    formattedDMY: `${formattedDate}/${formattedMonth}/${year}`,
    formattedYMD: changeFomat
  };
}
const dateFormats = getDate();
console.log("......",dateFormats.formattedYMD); // Output: "2024-06-1 
 

// Interface for component props
interface UseServiceFormProps {
  searchTerm: string; // Added searchTerm prop
}




function UseServiceFrom({ searchTerm }: UseServiceFormProps) {
  //state that use to handle fetching data
  const [ServiceList, setServiceList] = useState<Product[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const navigate = useNavigate()
  //state that show date
  const [currentDate, setCurrentDate] = useState(getDate());

  const [serviceQuantities, setServiceQuantities] = useState<{
    [key: string]: number;
  }>({});

  const [selectedServices, setSelectedServices] = useState<Product[]>([]);
 
  // Function to handle when a service item is clicked
  const handleServiceItemClick = (service: Product) => {
    const isSelected = selectedServices.some(
      (s) => s.pro_id === service.pro_id
    );
    if (!isSelected) {
      setSelectedServices([...selectedServices, service]);
      setServiceQuantities({ ...serviceQuantities, [service.pro_id]: 1 });
    } else {
      setSelectedServices(
        selectedServices.filter((s) => s.pro_id !== service.pro_id)
      );

      setServiceQuantities((prevQuantities) => {
        const updatedQuantities = { ...prevQuantities };
        delete updatedQuantities[service.pro_id];
        return updatedQuantities;
      });
    }
  };

  const handleDeleteButtonClick = (selectedService: Product) => {
    // Remove the selected service from the selectedServices array
    setSelectedServices(
      selectedServices.filter(
        (service) => service.pro_id !== selectedService.pro_id
      )
    );
  };

  const fetchUseService = async () => {
    try {
      const FetchingServiceDetail: Product[] = await FetchUseServicedata();
      setServiceList(FetchingServiceDetail);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching product data:", error);
    }
  };
  useEffect(() => {
    fetchUseService();
  }, []);



  if (error) {
    return <div>{error}</div>;
  }
  const handleQuantityChange = (proId: string, value: number | null) => {
    if (value !== null) {
      setServiceQuantities((prevQuantities) => ({
        ...prevQuantities,
        [proId]: value,
      }));
    }
  };



  
  
  const totalPrice = selectedServices.reduce((acc, selectedService) => {
    const price = Number(selectedService.pro_price.replace(/\./g,'')); // Coerce string to number using unary plus
    
    const quantity = serviceQuantities[selectedService.pro_id] || 1;  
    return acc + price * quantity;
  }, 0);


  const formattedTotalPrice = totalPrice.toLocaleString('en-US').replace(/,/g, '.');
   
  

 

  //Use Alert
  const setopenSuccessCreateAccount = useBillSuccessAlert(
    (state) => state.setopenSuccessCreateAccount
  );
  const setUnsuccessOpen = useErrorbill(
    (state) => state.setUnsuccessOpen
  );

  //USE PARAM
  const { appId, cusId, branchId, empId } = useParams<Params>();
  //handle post


  const handleServicePost = () => {
    const formattedService = selectedServices.map((selectedService, index) => ({
      price: String(Number(selectedService.pro_price.replace('.', ''))), // Convert to string
      amount: serviceQuantities[selectedService.pro_id] || 1,
      pro_id: String(selectedService.pro_id),
    }));
  
    ServicePost(
      totalPrice,
      dateFormats.formattedYMD,
      appId,
      cusId,
      branchId,
      empId,
      formattedService,
      () => {
        setopenSuccessCreateAccount(true);
        setTimeout(() => {
          navigate('/ReportBillPage');
        }, 1000); 
      },
      () => {
        setUnsuccessOpen(true)
      }
    );    
  };
  

  // Filter the service list based on the search term
  const filteredServiceList = ServiceList.filter((service) =>
    service.pro_name.toLowerCase().includes(searchTerm.toLowerCase())
  ); // Added filtering logic
  if (loading) {
    return <div>Loading...</div>;
  }
  if (error) {
    return <div>{error}</div>;
  }

  return (
    <div className="UseServiceFrom-main-div">
      <div className="UseServiceFrom-detail">
        <div className="UseServiceFrom-content-first">
          <div className="UseServiceFrom-content-gridfirst">
            <div className="UseServiceFrom-gridfirst-text">
              <p>ສິນຄ້າ ເເລະ ບໍລິການ</p>
            </div>
            {/* <div className="UseServiceFrom-gridfirst-Button">
              <Button>ບໍລິການ</Button>
            </div>
            <div className="UseServiceFrom-gridfirst-Button">
              <Button>ສິນຄ້າ</Button>
            </div>
            <div className="UseServiceFrom-gridfirst-icon ">
              <ShoppingCartIcon />
            </div> */}
          </div>
          <div className="UseServiceFrom-content-gridsecond">
            {filteredServiceList.map(service => (
              <div
                key={service.pro_id}
                className="UseServiceForm-second-gridbox"
                onClick={() => handleServiceItemClick(service)}
              >
                <div className="second-gridbox-img">
                  <img src={service.pro_photo} />
                </div>
                <div className="second-gridbox-label">
                  <p>ລາຍການ: {service.pro_name}</p>
                  <p>ລາຄາ: {service.pro_price}</p>
                </div>
              </div>
            ))}
          </div>
        </div>








        <div className="UseServiceFrom-content-second">
          <div className="UseServiceFrom-content-second-title">
            <p>ໃບບິນໃຊ້ບໍລິການ </p>
            <p>{dateFormats.formattedDMY}</p>
          </div>
          <div className="UseServiceFrom-content-second-detail">
            {selectedServices.map((selectedService, index) => (
              <div
                className="UseServiceFrom-content-second-useContent"
                key={selectedService.pro_id}
              >
                <div className="UseServiceFrom-useContent-Img">
                  <img src={selectedService.pro_photo} alt="Service" />
                </div>
                <div className="UseServiceFrom-useContent-useService">
                  <div className="UseServiceFrom-useContent-useService-P">
                    <p>ລາຍການ: {selectedService.pro_name}</p>
                    <p>ລາຄາ: {selectedService.pro_price}</p>
                  </div>

                  <NumberInput
                    aria-label="Quantity Input"
                    value={serviceQuantities[selectedService.pro_id] || 1}
                    onChange={(event, value) =>
                      handleQuantityChange(selectedService.pro_id, value)
                    }
                    className="NumberInput-Uservice"
                  />
                </div>
                <div className="UseServiceFrom-useContent-increaseNumber"></div>
                <div className="UseServiceFrom-useContent-delete">
                  <Button
                    onClick={() => handleDeleteButtonClick(selectedService)}
                  >
                    X
                  </Button>
                </div>
              </div>
            ))}
          </div>
          <div className="UseServiceFrom-content-second-submit">
            <div className="second-submit1">
              <p> ລາຄາ: {formattedTotalPrice} ກີບ</p>
              <Button onClick={handleServicePost}> ຕົກລົງ </Button>
              {/* <UseServiceFormPopUp /> */}
            </div>
          </div>
        </div>
      </div>
      <BillSuccessSnackbar/>
      <BillError/>
    </div>
  );
}

export default UseServiceFrom;

const blue = {
  100: "#daecff",
  200: "#b6daff",
  300: "#66b2ff",
  400: "#3399ff",
  500: "#007fff",
  600: "#0072e5",
  700: "#0059B2",
  800: "#004c99",
};

const grey = {
  50: "#F3F6F9",
  100: "#E5EAF2",
  200: "#DAE2ED",
  300: "#C7D0DD",
  400: "#B0B8C4",
  500: "#9DA8B7",
  600: "#6B7A90",
  700: "#434D5B",
  800: "#303740",
  900: "#1C2025",
};

const StyledInputRoot = styled("div")(
  ({ theme }) => `
  font-family: 'IBM Plex Sans', sans-serif;
  font-weight: 400;
  color: ${theme.palette.mode === "dark" ? grey[300] : grey[500]};
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
`
);

const StyledInput = styled("input")(
  ({ theme }) => `
  font-size: 0.875rem;
  font-family: inherit;
  font-weight: 400;
  line-height: 1.375;
  color: ${theme.palette.mode === "dark" ? grey[300] : grey[900]};
  background: ${theme.palette.mode === "dark" ? grey[900] : "#fff"};
  border: 1px solid ${theme.palette.mode === "dark" ? grey[700] : grey[200]};
  box-shadow: 0px 2px 4px ${
    theme.palette.mode === "dark" ? "rgba(0,0,0, 0.5)" : "rgba(0,0,0, 0.05)"
  };
  border-radius: 8px;
  margin: 0 8px;
  padding: 10px 12px;
  outline: 0;
  min-width: 0;
  width: 4rem;
  text-align: center;

  &:hover {
    border-color: ${blue[400]};
  }

  &:focus {
    border-color: ${blue[400]};
    box-shadow: 0 0 0 3px ${
      theme.palette.mode === "dark" ? blue[700] : blue[200]
    };
  }

  &:focus-visible {
    outline: 0;
  }
`
);

const StyledButton = styled("button")(
  ({ theme }) => `
  font-family: 'IBM Plex Sans', sans-serif;
  font-size: 0.875rem;
  box-sizing: border-box;
  line-height: 1.5;
  border: 1px solid;
  border-radius: 999px;
  border-color: ${theme.palette.mode === "dark" ? grey[800] : grey[200]};
  background: ${theme.palette.mode === "dark" ? grey[900] : grey[50]};
  color: ${theme.palette.mode === "dark" ? grey[200] : grey[900]};
  width: 32px;
  height: 32px;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  transition-property: all;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 120ms;

  &:hover {
    cursor: pointer;
    background: ${theme.palette.mode === "dark" ? blue[700] : blue[500]};
    border-color: ${theme.palette.mode === "dark" ? blue[500] : blue[400]};
    color: ${grey[50]};
  }

  &:focus-visible {
    outline: 0;
  }

  &.increment {
    order: 1;
  }
`
);
