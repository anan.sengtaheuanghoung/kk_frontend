import Header from "../../style/Header";
import "./HomePage2.css";
import HomePage2Detail from "./component/HomePage2Detail";
function HomePage2() {
 

 

  return (
    <div className="HomePage2"  >
      <Header isLoggedIn={false}    />
      <div className="content">
        <HomePage2Detail/>
      </div>
       
    </div>
  );
}
export default HomePage2;
