 
import React, { useEffect, useState } from "react";
import "./OrderPro-StockList.css";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button, Paper } from "@mui/material";
import OrderProductPopup from "./Order-Product-Popup";
import { FetchProductList } from "../../../../../service/http/axios-client/Admin-Side/Order-and-Import/Orders";

interface Product {
  pro_id: number;
  pro_name: string;
  pro_cost: number;
  amount: number;
}

function StockList({
  onProductSelect,
}: {
  onProductSelect: (product: Product) => void;
}) {
  const [productList, setProductList] = useState<Product[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  const fetchProducts = async () => {
    try {
      const products: Product[] = await FetchProductList();
      setProductList(products);
      setLoading(false);
    } catch (error) {
      
      console.error("Error fetching product data:", error);
    }
  };
  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div className="StockListDisplay-maindiv">
      <p>ສິນຄ້າໃນຮ້ານ</p>
      <div className="StockListDisplay">
        <Paper>
          <TableContainer sx={{ maxHeight: 350 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell className="StockList-TableCell-Style" align="center">
                    ລະຫັດ
                  </TableCell>
                  <TableCell className="StockList-TableCell-Style" align="center">
                    ຊື່
                  </TableCell>
                  <TableCell className="StockList-TableCell-Style" align="center">
                    ລາຄາ
                  </TableCell>
                  <TableCell className="StockList-TableCell-Style" align="center" >
                    ຈໍານວນ
                  </TableCell>
                  <TableCell className="StockList-TableCell-Style"></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {loading ? (
                  <TableRow>
                    <TableCell colSpan={5}>Loading...</TableCell>
                  </TableRow>
                ) : (
                  productList.map((product) => (
                    <TableRow key={product.pro_id}>
                      <TableCell align="center" className="Map-ProductList">
                        {product.pro_id}
                      </TableCell>
                      <TableCell className="Map-ProductList" align="center">
                        {product.pro_name}
                      </TableCell>
                      <TableCell className="Map-ProductList" align="center">
                        {product.pro_cost}
                      </TableCell>
                      <TableCell className="Map-ProductList" align="center">
                        {product.amount}
                      </TableCell>
                      <TableCell>
                        <div className="Select-button-orderProduct">
                          <Button onClick={() => onProductSelect(product)}>
                            ເລືອກ
                          </Button>
                        </div>
                      </TableCell>
                    </TableRow>
                  ))
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </div>
    </div>
  );
}

export default StockList;
