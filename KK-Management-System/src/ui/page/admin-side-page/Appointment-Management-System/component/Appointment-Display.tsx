
import "./Appointment-Display.css";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import { Button, TableHead } from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { FetchManageAppointment } from "../../../../../service/http/axios-client/Admin-Side/Admin-Appointment-Route/Admin-Appointment-management";
import { deleteManageAppointment } from "../../../../../service/http/axios-client/Admin-Side/Admin-Appointment-Route/Admin-Appointment-management";
import dayjs, { Dayjs } from "dayjs";
interface Product {
  app_id: string;
  cus_username: string;
  app_date: string;
  app_datetime: string;
  emp_name: string;
  app_status: string;
  branch_id:string;
}
interface ReportImportDisplayProps {
  searchData: string; 
  startDate: Dayjs | null;
  endDate: Dayjs | null;
}
function AddBookingDisplay({ searchData, startDate, endDate }:ReportImportDisplayProps) {
  const [AppointmentManageList, setAppointmentManageList] = useState<Product[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  const fetchManageAppointment = async () => {
    try {
      const appointmentList: Product[] = await FetchManageAppointment();
      setAppointmentManageList(appointmentList);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching appointment data:", error);
    }
  };
  useEffect(() => {
    fetchManageAppointment();
  }, [ fetchManageAppointment ]);



  //use navigate
  const navigate = useNavigate();
   // Handle delete appointment
   const handleDeleteApp =  (appId: string) => {
    try {
       deleteManageAppointment(appId);
       fetchManageAppointment();
    } catch (error) {
      console.error("Error deleting appointment:", error);
    }
  };


  //search function
  const filteredImportList = AppointmentManageList.filter(Applist => {
    const ImportDate = dayjs(Applist.app_id, 'DD/MM/YYYY');
    const impTotalString = Applist.cus_username.toString(); // Convert to string
    const matchesSearchQuery = Applist.app_date.toLowerCase().includes(searchData.toLowerCase()) ||
    Applist.app_datetime.toLowerCase().includes(searchData.toLowerCase()) ||
      impTotalString.toLowerCase().includes(searchData.toLowerCase()) || // Convert to lowercase before search
      Applist.emp_name.toLowerCase().includes(searchData.toLowerCase());
  
    const matchesDateRange = 
      (!startDate || ImportDate.isSame(startDate, 'day') || ImportDate.isAfter(startDate)) &&
      (!endDate || ImportDate.isSame(endDate, 'day') || ImportDate.isBefore(endDate, 'day'));
  
    return matchesSearchQuery && matchesDateRange;
  });



  
  return (
    <div className="ExpeneseDisplay">
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell
                style={{ fontSize: 25, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ລະຫັດ
              </TableCell>
              <TableCell
                style={{ fontSize: 25, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ຊື່
              </TableCell>
              <TableCell
                style={{ fontSize: 25, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ວັນທີ
              </TableCell>
              <TableCell
                style={{ fontSize: 25, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ເວລາ
              </TableCell>
              <TableCell
                style={{ fontSize: 25, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ຊ່າງຕັດຜົມ
              </TableCell>
              <TableCell
                style={{ fontSize: 25, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ສະຖານະ
              </TableCell>
              <TableCell
                style={{ fontSize: 25, fontFamily: "phetsarath ot " }}
                align="center"
              >
                 
              </TableCell>
              <TableCell align="center"> </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={5}>ຍັງບໍ່ມີການຈອງ...</TableCell>
              </TableRow>
            ) : (
              filteredImportList.map((appointment) => (
                <TableRow key={appointment.app_id}>
                  <TableCell className="Map-ProductList"  align="center">
                    {appointment.app_id}
                  </TableCell>
                  <TableCell className="Map-ProductList" align="center">
                    {appointment.cus_username}
                  </TableCell>
                  <TableCell className="Map-ProductList"align="center">
                    {appointment.app_date}
                  </TableCell>
                  <TableCell className="Map-ProductList" align="center">
                    {appointment.app_datetime}
                  </TableCell>
                  <TableCell className="Map-ProductList" align="center">
                    {appointment.emp_name}
                  </TableCell>
                  <TableCell className="Map-ProductList" align="center">
                    {appointment.app_status}
                  </TableCell>
                  <TableCell align="center" style={{ width: "90px" }}>
                    <button
                      className="edit-button"
                      onClick={() => {
                        navigate(
                          `/CalendarPage/${appointment.app_id}/${appointment.cus_username}`
                        );
                      }}
                    >
                      ເເກ້ໄຂ
                    </button>
                  </TableCell>
                  <TableCell align="center" style={{ width: "30px" }}>
                    <button className="edit-button-xx" onClick={() => handleDeleteApp(appointment.app_id)}>X</button>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default AddBookingDisplay;
