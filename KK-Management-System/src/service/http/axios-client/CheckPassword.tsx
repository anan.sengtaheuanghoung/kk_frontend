import axios from "axios";
import { useNavigate } from "react-router-dom";

export const CheckPassword = (username:string,  onSuccess: () => void,onError: () => void,) => {

    const userData = {
        username: username,      
      };
    
      axios
        .post("http://localhost:8080/forgot-password/request", userData)
        .then(() => {
          console.log("user name founded");
          onSuccess();
          
        })
        .catch((err) => {
          console.error("not founded", err);
          onError();

        });
}



export const ChangePassword = (username: string, newPassword:string, onNavigate: { (): void; (): void; },) => {
    const userData = {
      username: username,
      newPassword:  newPassword,    
      };
      axios
        .post("http://localhost:8080/forgot-password/set-new-password", userData)
        .then(() => {
          console.log("successfull changing password", );
          onNavigate();
          
        })
        .catch((err) => {
          console.error("not founded", err);
         });
}

