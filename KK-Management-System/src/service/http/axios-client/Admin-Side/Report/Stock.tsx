import axios from "axios";
import { getStoredToken } from "../../Login-axios";

//this route is use for fetching service Report
interface stockData {
    pro_id:string,
    pro_name:string,
    amount:string,
    pro_cost:string,
    pro_price:string,
    proType_name:string,
    pro_photo:string,

}
   const authToken = getStoredToken();
   const headers = {
    Authorization: `Bearer ${authToken}`,
};
   export const fetchStockReport = async (): Promise<stockData[]> => {
     try {
       const response = await axios.get("http://localhost:8080/stocks/report", {
         headers: headers,
       });
       console.log( response.data.report) 
       return response.data.report as stockData[];  
     } catch (error) {
       console.error("Error fetching stock Report:", error);
       throw error;
     }
   };

