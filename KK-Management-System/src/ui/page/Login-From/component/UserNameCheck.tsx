import { TextField } from "@mui/material";
import "./UserNameCheck.css";
import { CheckPassword } from "../../../../service/http/axios-client/CheckPassword";
import { useNavigate } from "react-router-dom";
import { create } from "zustand";
import forgotpassword from "../../../../image/forgotpassword.png";
import {
  WrongUsernameAlert,
  useWrongUserNameStore,
} from "../../../style/Alert/WrongUserName-Alert";
interface UsernameStoreState {
  changeusername: string;
  setUsername: (newUsername: string) => void;
}

// Create the store with the specified type
export const useUsernameStore = create<UsernameStoreState>((set) => ({
  changeusername: "",
  setUsername: (newUsername: string) => set({ changeusername: newUsername }),
}));

export default function UserNameCheck() {
  // Create the store with the specified type
  const { changeusername, setUsername } = useUsernameStore();
  const navigate = useNavigate(); // Obtain the navigate function using useNavigate hook
  const setUnsuccessOpen = useWrongUserNameStore(
    (state) => state.setUnsuccessOpen
  );

  const handleCheckPassword = () => {
    CheckPassword(
      changeusername,
      () => {
        navigate("/ConfirmPassword");
      },
      () => {
        setUnsuccessOpen(true);
      }
    );
    console.log(changeusername)
  };
  return (
    <div className="UserNameCheck-main-div">
      <div className="UserNameCheck-grid">
        <div className="UserNameCheck-first-grid">
          <img
            src={forgotpassword}
            style={{ width: "400px", height: "400px" }}
          />
        </div>

        <div className="UserNameCheck-form">
          <div className="UserNameCheck-form-title">
            <h1>ກະລຸນາປ້ອນຊື່ຂອງທ່ານ</h1>
          </div>
          <div className="UserNameCheck-form-text">
            <p>ຊື່ຜູ້ໃຊ້:</p>
            <TextField
              placeholder="Username"
              type="text"
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="UserNameCheck-form-button">
            <button onClick={() => {navigate(`/LoginPage`)}} >ກັບຄືນ</button>
            <button onClick={handleCheckPassword}>ຕໍ່ໄປ</button>
          </div>
        </div>
      </div>
      <WrongUsernameAlert />
    </div>
  );
}
