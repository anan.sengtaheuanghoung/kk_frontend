import axios from "axios";
import { getStoredToken } from "../Login-axios";


export interface FetchBranchByEachUser {
  branch_id: string;
  branch_name: string;
}



export const FetchBranchByUser = async (): Promise<FetchBranchByEachUser[]> => {
  const authToken = getStoredToken();
  const headers = {
    'Authorization': `Bearer ${authToken}`,
    'Content-Type': 'application/json',
  };

  try {
    const response = await axios.get('http://localhost:8080/branches-admin', { headers });

    // If response is an array, return it directly
    if (Array.isArray(response.data)) {
      return response.data; // For "ceo"
    }

    // If response is a single object, wrap it in an array
    if (typeof response.data === 'object') {
      return [response.data]; // For "admin"
    }

    throw new Error("Unexpected response structure"); // Handle unexpected cases
  } catch (error) {
    console.error("Error fetching branches:", error);
    throw error;
  }
};



//save Emp
interface UseEmpData {
  name: string;
  username: string;
  // email: string;
  phonenumber: string;
  password: string;
  address: string;
  branch_id: string;
  photo: File | null;
}

export const SaveEmp = (
  name: string,
  username: string,
  // email: string,
  phonenumber: string,
  password: string,
  address: string,
  branch_id: string,
  photo: File | null,
  onSuccess: () => void,
  onUnsuccess: () => void,
) => {
  const empData: UseEmpData = {
    name: name,
    username: username,
    // email: email,
    phonenumber: phonenumber,
    password: password,
    address: address,
    branch_id: branch_id,
    photo: photo,
  };

  const formData = new FormData();
  formData.append("name", empData.name);
  formData.append("username", empData.username);
  // formData.append("email", empData.email);
  formData.append("phonenumber", empData.phonenumber);
  formData.append("password", empData.password);
  formData.append("address", empData.address);
  formData.append("branch_id", empData.branch_id);
  
  if (empData.photo) {
    formData.append("photo", empData.photo);
  }
 
  const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
    console.log("emp",headers)
  axios
    .post("http://localhost:8080/management/employee/signup",formData, {
      headers:headers,
    })
    .then((response) => {
      console.log("successful");
      console.log("if save success",response.data);
      onSuccess();
    })
    .catch((err) => {
      console.error("Error during save:", err);
      onUnsuccess();
    });
};


 
//edit button
export const EditEmp = (
  emp_id: string, // Add employee ID as a parameter
  name: string,
  username: string,
  // email: string,
  phonenumber: string,
  password: string,
  branch_id: string,
  address:string,
  pic: File | null, // Include the photo file parameter
  onSuccess: () => void,
  onUnsuccess: () => void,

) => {
 
  const formData = new FormData();
  formData.append("name", name);
  formData.append("username", username);
  // formData.append("email", email);
  formData.append("phonenumber", phonenumber);
  formData.append("password", password);
  formData.append("address", address);

  formData.append("branch_id", branch_id);
  formData.append("photo", pic || "");  

  axios
    .put(`http://localhost:8080/management/employee/update/${emp_id}`, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      console.log("Edit successful");
      console.log(response.data);
      onSuccess ();

    })
    .catch((err) => {
      console.error("Error during edit:", err);
      onUnsuccess();
    });
};



//DeleteEMP
export const DeleteEmp = (emp_id: string,  onSuccess: () => void,
) => {
  axios
    .delete(`http://localhost:8080/management/employee/delete/${emp_id}`, {
      headers: {
        "Content-Type": "application/json", // Change content type to JSON
      },
    })
    .then((response) => {
      console.log("Deletion successful");
      console.log(response.data);
      onSuccess();
    })
    .catch((err) => {
      console.error("Error during deletion:", err);
    });
};

