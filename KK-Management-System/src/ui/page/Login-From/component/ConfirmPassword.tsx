import { TextField } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import { ChangePassword } from "../../../../service/http/axios-client/CheckPassword";
import "./ConfirmPassword.css";
import { useState } from "react";
import { useUsernameStore } from "./UserNameCheck";
import resetpassword from "../../../../image/resetpassword.png";

import {
  SuccessChangingPass,
  useSuccessChangingPassStore,
} from "../../../style/Alert/Successful-Changing-Pass";
export default function ConfirmPassword() {

  const [password, setPassword] = useState("");
  const { changeusername } = useUsernameStore();
  const setOpen = useSuccessChangingPassStore((state) => state.setOpen);
  const navigate = useNavigate();

  const handleChangePassword = () => {
    ChangePassword(changeusername, password, () => {
      setOpen(true);
      setTimeout(() => {       
        navigate("/LoginPage");
      }, 1500);
 
    });
  };


  return (
    <div className="UserNameCheck-main-div">
      <div className="UserNameCheck-grid">
        <div className="UserNameCheck-first-grid"> 
        <img
            src={resetpassword}
            style={{ width: "350px", height: "350px" }}
          />
        </div>
      
      <div className="UserNameCheck-form">
        <div className="UserNameCheck-form-title">
          <h1>ກະລຸນາປ້ອນລະຫັດໃໝ່ຂອງທ່ານ</h1>
        </div>
        <div className="UserNameCheck-form-text">
          <p> ລະຫັດໃໝ່:</p>
          <TextField
            onChange={(e) => setPassword(e.target.value)}
            placeholder="New Password"
            type="text"
          />
        </div>
        {/* <div className="UserNameCheck-form-text">
          <p>ຢືນຢັນລະຫັດ</p>
          <TextField placeholder=" " type="text" />
        </div> */}
        <div className="UserNameCheck-form-button">
          <button onClick={() => {navigate(`/UserNameCheck`)}}>ກັບຄືນ</button>
             <button onClick={handleChangePassword}>ຕໍ່ໄປ</button>
         </div>
      </div>
      </div>
      <SuccessChangingPass />
    </div>
  );
}
