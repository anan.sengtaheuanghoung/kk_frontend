import "./Income-Display.css";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button } from "@mui/material";
import IncomePopUP from "./Income-popup";
import { fetchIncomeReport } from "../../../../../../service/http/axios-client/Admin-Side/Report/income";
import { useEffect, useState } from "react";
import { useIncomeStore } from "./Income-form";
import { create } from "zustand";

interface IncomeData {
   branch_name: string;
  pro_id: string;
  pro_name: string;
  amount: string;
  pro_price: string;
  total_price: string;
  ser_total: string;
   ser_date: string;
}

interface ReportState {
  reportOrder: IncomeData[];
  setReportOrder: (reportOrder: IncomeData[]) => void;
}

export const useIncomeMapoutData = create<ReportState>((set) => ({
  reportOrder: [],
  setReportOrder: (reportOrder) => set({ reportOrder }),
}));


const IncomeDisplay: React.FC = () => {

  const { reportOrder, setReportOrder } = useIncomeMapoutData(); // Use Zustand store
  const [loading, setLoading] = useState<boolean>(true);
  const { startDate, endDate } = useIncomeStore();

  useEffect(() => {
    const fetchReportData = async () => {
      try {
        const incomeList: IncomeData[] = await fetchIncomeReport(
          startDate,
          endDate
        );
        setReportOrder(incomeList); // Set the reportOrder in Zustand store
        setLoading(false);
      } catch (error) {
        console.error("Error fetching product data:", error);
      }
    };

    if (startDate && endDate) {
      fetchReportData();
    }
  }, [startDate, endDate, setReportOrder]);

  return (
    <div className="IncomeDisplay">
      <TableContainer sx={{ maxHeight: 530 }} >
        <Table stickyHeader aria-label="sticky table"  >
          <TableHead>
            <TableRow>
              <TableCell
                style={{ fontSize: 23, fontFamily: "phetsarath ot" }}
                align="center"
              >
                ລະຫັດ
              </TableCell>
              <TableCell
                style={{ fontSize: 23, fontFamily: "phetsarath ot" }}
                align="center"
              >
                ຊື່ສິນຄ້າ
              </TableCell>
              <TableCell
                style={{ fontSize: 23, fontFamily: "phetsarath ot" }}
                align="center"
              >
                ລາຄາ
              </TableCell>
              <TableCell
                style={{ fontSize: 23, fontFamily: "phetsarath ot" }}
                align="center"
              >
                ຈໍານວນ
              </TableCell>
              <TableCell
                style={{ fontSize: 23, fontFamily: "phetsarath ot" }}
                align="center"
              >
                ລາຄາທັງໝົດ
              </TableCell>
            </TableRow>
          </TableHead>

          <div></div>

          <TableBody>
            {loading ? (
              <TableRow className="Loading">
                <p>ກະລຸນາເລືອກວັນທີເພື່ອສະເເດງລາຍຮັບ !!!</p>
              </TableRow>
            ) : (
              reportOrder.map((income, index) => (
                <TableRow>
                  <TableCell className="Map-ReportOrder">{index + 1}</TableCell>
                  <TableCell className="Map-ReportOrder">
                    {income.pro_name}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {income.pro_price}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {income.amount}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {income.total_price}
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};
export default IncomeDisplay;
