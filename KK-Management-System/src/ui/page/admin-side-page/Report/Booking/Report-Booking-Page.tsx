import './Report-Booking-Page.css'
import SideBar from '../../../../style/AdminPage-sideBar'
import BookingDisplay from './component/Booking-Display'
import BookingForm from './component/Booking-Form'
import { useNavigate } from 'react-router-dom'
import { Button } from '@mui/material'
import { useUserProfileStore } from '../../../../../service/http/axios-client/Login-axios'
import { useState } from 'react'
function BookingPage () {
    const navigate = useNavigate()
    const userProfileData = useUserProfileStore(state => state.userData); 
      //take cusId from login(provide data by zustand)
    const [role] = useState<string | undefined>(userProfileData?.role);
 

    return(
        <div className="BookingPage">
        <div className="BookingPage-first-grid">
          <SideBar />
        </div>
        <div className="BookingPage-second-grid">
        <BookingForm/>
        <div className="BookingForm-input-grid2 ">
        {(role !== 'employee')&&( 

         <Button onClick={() => {navigate(`/AddBookingPage`)}}>ຈອງ</Button>
        )}
        </div>
        <BookingDisplay/>

 
      </div>
      </div>
    )
}
export default BookingPage