import React, { useState, useRef } from "react";
import { styled, css } from "@mui/system";
import { Modal as BaseModal } from "@mui/base/Modal";
import { useReactToPrint } from "react-to-print";
import {
  Backdrop,
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import "./Report-Import-Popup.css";
import { fecthImportBill } from "../../../../../../service/http/axios-client/Admin-Side/Report/ReportIncome";
import LOGO from "../../../../../../image/LOGO.png";

const blue = {
  200: "#99CCFF",
  300: "#66B2FF",
  400: "#3399FF",
  500: "#007FFF",
  600: "#0072E5",
  700: "#0066CC",
};

const grey = {
  50: "#F3F6F9",
  100: "#E5EAF2",
  200: "#DAE2ED",
  300: "#C7D0DD",
  400: "#B0B8C4",
  500: "#9DA8B7",
  600: "#6B7A90",
  700: "#434D5B",
  800: "#303740",
  900: "#1C2025",
};

const Modal = styled(BaseModal)`
  position: fixed;
  z-index: 1300;
  inset: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledBackdrop = styled(Backdrop)`
  z-index: -1;
  position: fixed;
  inset: 0;
  background-color: rgb(0 0 0 / 0.5);
  -webkit-tap-highlight-color: transparent;
`;

const ModalContent = styled("div")(
  ({ theme }) => css`
    font-family: "IBM Plex Sans", sans-serif;
    font-weight: 500;
    text-align: start;
    position: relative;
    display: flex;
    flex-direction: column;
    gap: 8px;
    width: 500px;
    height: auto;
    overflow: auto;
    background-color: ${theme.palette.mode === "dark" ? grey[900] : "white"};
    border-radius: 8px;
    box-shadow: 0 4px 12px
      ${theme.palette.mode === "dark" ? "rgb(0 0 0 / 0.5)" : "rgb(0 0 0 / 0.2)"};
    padding: 24px;
    color: ${theme.palette.mode === "dark" ? grey[50] : grey[900]};

    & .modal-title {
      margin: 0;
      line-height: 1.5rem;
      margin-bottom: 8px;
    }

    & .modal-description {
      margin: 0;
      line-height: 1.5rem;
      font-weight: 400;
      color: ${theme.palette.mode === "dark" ? grey[400] : grey[800]};
      margin-bottom: 4px;
    }
  `
);

const TriggerButton = styled("button")(
  ({ theme }) => css`
    font-family: "phetsarath ot";
    font-weight: 400;
    font-size: 18px;
    line-height: 1.5;
    padding: 8px 16px;
    border-radius: 8px;
    transition: all 150ms ease;
    cursor: pointer;
    background: #d9d9d9;
    color: black;
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);

    &:hover {
      background: "grey";
      border-color: ${theme.palette.mode === "dark" ? grey[600] : grey[300]};
    }

    &:active {
      background: ${theme.palette.mode === "dark" ? grey[700] : grey[100]};
    }

    &:focus-visible {
      box-shadow: 0 0 0 4px
        ${theme.palette.mode === "dark" ? blue[300] : blue[200]};
      outline: none;
    }
  `
);

interface RecieveData {
  importID: string;
  branchName: string;
  Date:string;
}

interface ImportListBill {
  imp_id: string;
  imp_date: string;
  branch_name: string;
  pro_id: string;
  pro_name: string;
  amount: string;
  price: string;
  proType_name: string;
  total_price:string;
}



const formatNumber = (num: number): string => {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

export default function ReportImportPopUP({ importID ,branchName,Date}: RecieveData) {
  const [open, setOpen] = useState(false);
  const contentRef = useRef<HTMLDivElement | null>(null);
  const [totalPrice, setTotalPrice] = useState<number>(0); // State for total price
 
   
  const handleClose = () => setOpen(false);

  const handlePrint = useReactToPrint({
    content: () => contentRef.current,
    documentTitle: "Report Order", // Optional title for the printout
  });

  const [importBill, setOrderBill] = useState<ImportListBill[]>([]);
  const handleChange = async () => {
    setOpen(true);
    try {
      const response = await fecthImportBill(importID);
      setOrderBill(response);

      // Calculate total price
      const total = response.reduce((sum: number, bill: ImportListBill) => {
        // Remove periods from total_price and parse as float
        const cleanedPrice = parseFloat(bill.total_price.replace(/\./g, ''));
        return sum + cleanedPrice;
      }, 0);
      setTotalPrice(total);
    } catch (error) {
      console.error("Error fetching import bill:", error);
    }
  };

  return (
    <div>
      <TriggerButton onClick={handleChange}>ເລືອກ</TriggerButton>{" "}
      {/* Opens the modal */}
      <Modal
        open={open}
        onClose={handleClose}
        slots={{ backdrop: StyledBackdrop }}
      >
        <ModalContent>
        <div className="order-popup-title">
              <img  style={{width:200,height:200}}src={LOGO}/>
            </div>
          <div className="Report-Import-Maindiv" ref={contentRef}>
            <div className="Import-popup-title">
              <p>ໃບບິນນໍາເຂົ້າສິນຄ້າ</p>
            </div>
            <div className="Import-popup-showID">
              <div className="Import-showid-grid1">
                <p> ລະຫັດ: {importID}</p>
                <p className="">ວັນທີ: {Date} </p>
              </div>
              <div className="Import-showid-grid2">
                <p className="">ສາຂາ: {branchName}</p>
              </div>
            </div>
            <div className="Import-popup-showList">
              <TableContainer component={Paper}>
                <Table
                  sx={{ minWidth: 460 }}
                  size="small"
                  aria-label="a dense table"
                  
                >
                  <TableHead>
                    <TableRow>
                      <TableCell className="TableHead-style-for-import" >ລໍາດັບ</TableCell>
                      <TableCell className="TableHead-style-for-import">ຊື່ສິນຄ້າ</TableCell>
                      <TableCell className="TableHead-style-for-import">ຈໍານວນ</TableCell>
                      <TableCell className="TableHead-style-for-import">ປະເພດສິນຄ້າ</TableCell>
                      <TableCell className="TableHead-style-for-import">ລາຄາ</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody className="Import-table-body">
                    {importBill.map((importbill, index) => (
                      <TableRow
                        key={importbill.imp_id}
                        sx={{
                          "&:last-child td, &:last-child th": { border: 0 },
                        }}
                      >
                        <TableCell align="center" >{index + 1}</TableCell>

                        <TableCell align="center">
                          {importbill.pro_name}
                        </TableCell>
                        <TableCell align="center">
                          {importbill.amount}
                        </TableCell>
                        <TableCell align="center">
                          {importbill.proType_name}
                        </TableCell>
                        <TableCell align="center">{importbill.price}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
            <div className="Import-popup-branch">
              <div className="import-popup-total">
              
              <p >ລາຄາລວມ:{formatNumber(totalPrice)}ກີບ </p>
               </div>
              
            </div>
          </div>
          <div className="import-print-button">
            <Button onClick={handlePrint}>Print</Button>
          </div>
        </ModalContent>
      </Modal>
    </div>
  );
}
