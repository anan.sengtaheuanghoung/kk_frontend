import './Admin-Booking-Page.css'
import SideBar from '../../../style/AdminPage-sideBar'
import AddBookingForm from './component/Admin-Booking-Form'
import AddBookingDisplay from './component/Admin-Booking-Display'
import { SetStateAction, useEffect, useState } from 'react';
 
 

function AddBookingPage () {
  const [searchQuery, setSearchQuery] = useState('');

  const handleSearch = (value: SetStateAction<string>) => {
    setSearchQuery(value);
  };
    return (
        <div className="AddBookingPage-page">
        <div className="AddBookingPage-first-grid">
          <SideBar />
        </div>
        <div className="AddBookingPage-second-grid">
        <AddBookingForm onSearch={handleSearch} />
        <AddBookingDisplay  searchData={searchQuery}/>
         </div>
      </div>
    )
}


export default AddBookingPage