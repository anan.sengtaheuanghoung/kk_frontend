import "./Use-Service-Page.css";
import SideBar from "../../../style/AdminPage-sideBar";
import UseServiceFrom from "./component/Use-Service-Form";
import { TextField } from "@mui/material";
import { useState } from "react";
 

function UseServicePage() {
 
  const [searchTerm, setSearchTerm] = useState("");

  return (
    <div className="UseServicePage-page">
      <div className="UseServicePage-first-grid">
        <SideBar />
      </div>
      <div className="UseServicePage-second-grid">
        <p>ບໍລິການ</p>
        <div className="UseServicePage-grid1">
          <p>ຄົ້ນຫາ:</p>
          <TextField
            className="UseServicePage-grid1-textfield"
            sx={{
              "& fieldset": { border: "none" },         
             }}
            value={searchTerm} 
            onChange={(e) => setSearchTerm(e.target.value)} 
          />
        </div>
        <UseServiceFrom searchTerm={searchTerm} />
      </div>
    </div>
  );
}
export default UseServicePage;
