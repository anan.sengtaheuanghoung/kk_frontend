import "./Branch-selection.css";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import ChooseBarber from "./ChooseBarber";
import { FetchBranch } from "../../../../service/http/axios-client/Appointment/CusAppointment";
import { useEffect, useState } from "react";
import { Branch } from "../../../../service/http/axios-client/Appointment/CusAppointment";
import { useNavigate } from "react-router-dom";

interface SelectedEmployee {
  branchId: string;
  empName: string;
  empId: string;
  branchName:string;
}

function BranchSelection() {
  const [branches, setBranches] = useState<Branch[]>([]);
  const [isVisible, setIsVisible] = useState(true);
  const [selectedEmployee, setSelectedEmployee] = useState<SelectedEmployee>({
    branchId: "",
    empName: "",
    branchName:"",
    empId:"",
  });
  const navigate = useNavigate();
  const [selectedBranchName, setSelectedBranchName] = useState<string>("");
  const handleButtonClick = () => {
    setSelectedEmployee({ branchId: "", branchName:"", empName: "" ,empId:""});
    setSelectedBranchName("");
    setIsVisible(false);
  };

  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const response = await FetchBranch();
        setBranches(response);
      } catch (error) {
        console.error("Error fetching branches:", error);
      }
    };
    fetchBranches();
  }, []);

  const onSelectEmployee = (
    branchId: string,
    branchName: string,
    empName: string,
    empId :string
  ) => {
    setSelectedEmployee({ branchId,branchName , empName,empId,});
    setSelectedBranchName(branchName);
    setIsVisible(true);
  };


  //one of a new way to send param
  //this is how to set the params 
  const params = new URLSearchParams({
    branchId: selectedEmployee.branchId,
    empId: selectedEmployee.empId,
    empName: selectedEmployee.empName,
    branchName: selectedEmployee.branchName
  })

  return (
    <div className="Branch-maindiv">
      <div className="Branch-selection-deatail">
        <div className="Branch-first-content">
          <div className="Branch-first-content-detail">
            <h1> ເລືອກສາຂາ </h1>
          </div>
          <div className="Branch-second-content-detail">
            {branches.map((branch, index) => (
              <div className="second-content-firstgrid" key={index}>
                <div className="second-content-firstgrid-grid1">
                  <AccountCircleIcon />
                  <h2>{branch.branch_name}</h2>
                </div>
                <div className="second-content-firstgrid-grid2">
                  <h1>
                    <ChooseBarber
                      branchId={branch.branch_id}
                      branchName={branch.branch_name}
                      onSelect={onSelectEmployee}
                    />
                  </h1>
                </div>{" "}
              </div>
            ))}
          </div>
        </div>
        <div className="Branch-second-content ">
          <div style={{ borderBottom: "2px solid white" }}>
            <p style={{ fontSize: 20, marginLeft: 20 }}> ຊ່າງຕັດຜົມຂອງທ່ານ</p>
          </div>

          {selectedBranchName && selectedEmployee.empId && isVisible && (
            <div>
              <div>
                <h5 style={{ fontSize: 20 ,marginLeft:20 }}>ສາຂາ: {selectedBranchName}</h5>
              </div>
              <div  className="popUp-employeesName">
                <div className="popUp-employeesName-grid1">
                <a>
                    <AccountCircleIcon />
                </a>
                <p>{selectedEmployee.empName}</p>
                </div>
                <div className="popUp-employeesName-grid2">
                <button
                  style={{
                    display: "flex",
                    alignItems: "center",
                    gap: "10px",
                   }}
                  onClick={handleButtonClick}
                >
                  X
                </button>
                </div>
              </div>
              <div className="Submit-button">
                <button
                  style={{
                    display: "flex",
                    alignItems: "center",
                    gap: "10px",
                    marginLeft: "35%",
                  }}
                  onClick={() => {
                    // navigate(`/DateTimePage/${selectedEmployee.branchId}/${selectedEmployee.empId}/${selectedEmployee.empName}/${selectedEmployee.branchName}`)
                    navigate(`/DateTimePage?${params.toString()}`);
                  }}
                >
                  ເລືອກເວລາ
                </button>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
export default BranchSelection;
