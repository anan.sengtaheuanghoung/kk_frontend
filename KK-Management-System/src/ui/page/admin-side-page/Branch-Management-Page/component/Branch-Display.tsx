// import { TextField } from "@mui/material";

// function SupDisPlay () {
//     return(
//         <div className="SupManageDisplay">
//         <div className="SupManageDisplay-grid1 ">
//           <p>ຄົ້ນຫາ:</p>
//           <TextField className="SupManageDisplay-grid1-textfield  "
//           sx={{
//               "& fieldset": { border: 'none' },
//             }}
//         />
//         </div>
//         <div className="SupManageDisplay-grid2"></div>
//       </div>
//     )
// }
// export default SupDisPlay

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  styled,
  tableCellClasses,
} from "@mui/material";
import React, { ReactNode, useEffect, useState } from "react";
import axios from "axios";
import { create } from "zustand";
import "./Branch-Display.css";
import { getStoredToken } from "../../../../../service/http/axios-client/Login-axios";
const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

interface useBranchStoreState {
  selectedData: {
    branch_id: string;
    branch_name: string;
    branch_pnumber: string;
    branch_address: string;
  };
  setSelectedData: (data: {
    branch_id: string;
    branch_name: string;
    branch_pnumber: string;
    branch_address: string;
  }) => void;
}

// Zustand store
export const usebranchStore = create<useBranchStoreState>((set) => ({
  selectedData: {
    branch_id: "",
    branch_name: "",
    branch_pnumber: "",
    branch_address: "",
  },
  setSelectedData: (data: any) => set({ selectedData: data }),
}));

interface Column {
  id: keyof Data;
  label: string;
  minWidth?: number;
  align?: "left" | "center" | "right";
}

const columns: readonly Column[] = [
  { id: "ID", label: "ລະຫັດ" },
  { id: "Name", label: "ຊື່" },
  { id: "PhoneNumber", label: "ເບີໂທ" },
  { id: "Address", label: "ທີ່ຢູ່" },
];

interface Data {
  branch_id: string;
  branch_name: string;
  branch_pnumber: string;
  branch_address: string;
  ID: number;
  Name: string;
  PhoneNumber: string;
  Address: string;
}

function ProTypedisplay() {
  const [rows, setRows] = useState<Data[]>([]);
  const { selectedData, setSelectedData } = usebranchStore();
  const [searchQuery, setSearchQuery] = useState("");

  const handleRowClick = (row: {
    branch_id: any;
    branch_name: any;
    branch_pnumber: any;
    branch_address: any;
  }) => {
    setSelectedData({
      branch_id: row.branch_id,
      branch_name: row.branch_name,
      branch_pnumber: row.branch_pnumber,
      branch_address: row.branch_address,
    });
  };

  //fect data to show on table
  const fetchBranchData = () => {
    const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
    axios
      .get("http://localhost:8080/management/branches", {
        headers: headers,
      })
      .then((response) => {
        setRows(response.data);
      })
      .catch((error) => {
        console.error("Error fetching protypedata data:", error);
      });
  };

  useEffect(() => {
    fetchBranchData();
  }, [selectedData]);

  const filteredRows = rows.filter(
    (row) =>
      typeof row.branch_name === "string" &&
      row.branch_name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <div className="SupManageDisplay">
      <div className="SupManageDisplay-grid1">
        <p>ຄົ້ນຫາ:</p>
        <TextField
          className="SupManageDisplay-grid1-textfield"
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
          sx={{
            "& fieldset": { border: "none" },
          }}
        />
      </div>
      <div className="SupManageDisplay-grid2">
        <TableContainer sx={{ maxHeight: 320 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{
                      minWidth: column.minWidth,
                      fontSize: 24,
                      width: 70,
                      fontFamily: "phetsarath ot",
                    }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredRows.map((row) => (
                <StyledTableRow
                  key={row.branch_id}
                  onClick={() => handleRowClick(row)}
                >
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.branch_id}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.branch_name}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.branch_pnumber}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.branch_address}
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}

export default ProTypedisplay;
