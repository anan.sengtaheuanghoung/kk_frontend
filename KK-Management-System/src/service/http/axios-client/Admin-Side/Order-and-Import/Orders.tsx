import axios from "axios";
import { getStoredToken } from "../../Login-axios";

interface Product {
    pro_id: number;
    pro_name: string;
    pro_cost: number;
    amount: number;
}
const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };

export const FetchProductList = async (): Promise<Product[]> => {
    try {
      const response = await axios.get("http://localhost:8080/stock-list", {
        headers: headers,
      });
      return response.data as Product[]; // Assuming the response data is an array of products
    } catch (error) {
      console.error("Error fetching product data:", error);
      throw error;
    }
};



  
export const SaveNewAmount = (productId: string, amount: number) => {
    const requestData = {
      productId: productId,
      amount: amount
    };
  
    axios
      .post("http://localhost:8080/order-amount", requestData, {   
        headers: headers,     
      })  
      .then((response) => {
        console.log("Save Order Product Amount successful");
        console.log(response.data); 
      })
      .catch((err) => { 
        console.error("Error during SaveProductType:", err.message);
      });
  };



interface ProductData {
  order_date: string;
  sup_id: string;
  branch_id: string;
  products: {
    amount: string;
    pro_id: string;
  }[];
}

export const OrderProductPost = (
  order_date: string,
  sup_id: string,
  branch_id: string,
  products: { amount: string; pro_id: string }[],
  onSuccess: () => void,
  onUnsuccess: () => void,
 
) => {
  const orderData: ProductData = {
    order_date: order_date,
    sup_id: sup_id,
    branch_id: branch_id,
    products: products,
  };

  axios
    .post(`http://localhost:8080/orders`, orderData,{
      headers:headers
    })
    .then((response) => {
      
      onSuccess()
       console.log("Order successful:", response.data);
     })
    .catch((error) => {
      onUnsuccess()
      console.error("Error during order:", error.message);
     });
};

