

import { MenuItem, Select, SelectChangeEvent, styled } from "@mui/material";
import "./Emp-Management-form.css";
import TextField from "@mui/material/TextField";
import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import {
  SaveEmp,
  EditEmp,
  DeleteEmp,
  FetchBranchByUser,
  FetchBranchByEachUser,
} from "../../../../../service/http/axios-client/Admin-Side/EmployeeManagement";
import { useEmpStore } from "./Emp-Management-display";
import {
  SaveCompleteSnackbar,
  useSaveButtonAlert,
} from "../../../../style/Alert/AdminSide-Alert/SaveButtonAlert";
import {
  ErrorSaving,
  useErrorSaving,
} from "../../../../style/Alert/AdminSide-Alert/ErrorSaving";
import {
  EditSuccess,
  useEditSuccess,
} from "../../../../style/Alert/AdminSide-Alert/EditSuccessful";
import {
  DeleteSuccess,
  useDeleteSuccess,
} from "../../../../style/Alert/AdminSide-Alert/DeleteSuccessful";
import {
  ErrorEdit,
  useErrorEdit,
} from "../../../../style/Alert/AdminSide-Alert/ErrorEditing";







function EmpManageForm() {

  
  //take state from zustand
  const { selectedData, setSelectedData } = useEmpStore();
  //map out branch name
  const [branches, setBranches] = useState<FetchBranchByEachUser[]>([]);
  const [selectedBranch, setSelectedBranch] = useState<string>("");
  //alert that use from zustand
  const setopenSuccessCreateAccount = useSaveButtonAlert(
    (state) => state.setopenSuccessCreateAccount
  );
  const setUnsuccessOpen = useErrorSaving((state) => state.setUnsuccessOpen);
  const setopenEditSuccess = useEditSuccess(
    (state) => state.setopenEditSuccess
  );
  const setopenDeleteSuccess = useDeleteSuccess(
    (state) => state.setopenDeleteSuccess
  );
  const setErrorEdit = useErrorEdit((state) => state.setErrorEdit);



  //this useEffect use to mapp a branch by each id
  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const branchesData = await FetchBranchByUser();
        setBranches(branchesData); // Correctly set the state
      } catch (error) {
        console.error("Error fetching branches:", error);
      }
    };
    fetchBranches();
  }, []);
  const handleMapbranch = (event: SelectChangeEvent<string>) => {
    setSelectedBranch(event.target.value);
  };
  


  //state that store value
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [address, setAddress] = useState("");
  const [pic, setPic] = useState<File | null>(null);
  console.log("see pic data if it change-------",pic)
  

  //SAVE BUTTON
  const handleSave = () => {
    SaveEmp(
      name,
      username,
      // email,
      phoneNumber,
      password,
      address,
      selectedBranch,
      pic !== null ? pic : null,
      () => {
        setSelectedData({
          id: "",
          name: "",
          email: "",
          phoneNumber: "",
          username: "",
          password: "",
          address: "",
          pic: null,
          branch_id: "selectedBranch",
        });

        setopenSuccessCreateAccount(true);
      },
      () => {
        setUnsuccessOpen(true);
      }
    );
    console.log(" --------",selectedBranch)
   };

  //EDIT BUTTON
  const handleEdit = () => {
    EditEmp(
      id,
      name,
      username,
      // email,
      phoneNumber,
      password,
      selectedBranch,
      address,
      pic !== null ? pic : null,
      () => {
        setSelectedData({
          id,
          name,
          email,
          phoneNumber,
          username,
          password,
          address,
          pic,
          branch_id: selectedBranch,
        });
        setopenEditSuccess(true);
      },
      () => {
        setErrorEdit(true);
      }
    );
  };

  //CLEAR BUTTON
  const handleClear = () => {
    setSelectedData({
      id: "",
      name: "",
      username: "",
      email: "",
      password: "",
      phoneNumber: "",
      address: "",
      pic: null,
      branch_id: "",
    });
    setPic(null);
  };

  //DELETE BUTTON
  const handleDelete = () => {
    DeleteEmp(id, () => {
      setSelectedData({
        id: "",
        name: "",
        username: "",
        email: "",
        password: "",
        phoneNumber: "",
        address: "",
        pic: null,
        branch_id: "",
      });
      setopenDeleteSuccess(true);
    });
    setPic(null);
  };

  //HANDLDE MAP DATA FROM TABLE ROW
  useEffect(() => {
    setId(selectedData.id);
    setName(selectedData.name);
    setEmail(selectedData.email);
    setPhoneNumber(selectedData.phoneNumber);
    setAddress(selectedData.address);
    setUsername(selectedData.username);
    setPassword(selectedData.password);
    setSelectedBranch(selectedData.branch_id);
    setPic(selectedData.pic);
  }, [selectedData]);
  
  
  return (
    <div className="EmpManageForm" >
      <div className="EmpManageForm-title">
        <h1>ຈັດການພະນັກງານ </h1>
      </div>
      <div className="EmpManageForm-input">
        <div className="EmpManageForm-input-grid1 EmpManageForm-grid-direction">
          {/* <div className="EmpManageForm-input-detail ">
            <p>ລະຫັດ </p>
            <div className="emp-textfield ">
              <TextField
                disabled
                value={id}
                onChange={(e) => setId(e.target.value)}
              ></TextField>
            </div>
          </div> */}
          <div className="EmpManageForm-input-detail">
            <p>ຊື່</p>
            <div className="emp-textfield ">
              <TextField
                value={name}
                onChange={(e) => setName(e.target.value)}
              ></TextField>
            </div>
          </div>
          <div className="EmpManageForm-input-detail">
            <p>ເບີໂທ</p>
            <div className="emp-textfield ">
              <TextField
                value={phoneNumber}
                onChange={(e) => setPhoneNumber(e.target.value)}
              ></TextField>
            </div>
          </div>
          <div className="EmpManageForm-input-detail">
            <p>ທີ່ຢູ່</p>
            <div className="emp-textfield ">
              <TextField
                value={address}
                onChange={(e) => setAddress(e.target.value)}
              ></TextField>
            </div>
          </div>
        </div>
        <div className="EmpManageForm-input-grid2 EmpManageForm-grid-direction">
          <div className="EmpManageForm-input-detail">
            <p>ຊື່ຜູ້ໃຊ້</p>
            <div className="emp-textfield ">
              <TextField
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              ></TextField>
            </div>
          </div>
          {/* <div className="EmpManageForm-input-detail">
            <p>ອີເມວ</p>
            <div className="emp-textfield ">
              <TextField
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></TextField>
            </div>
          </div> */}
          <div className="EmpManageForm-input-detail">
            <p>ລະຫັດຜ່ານ</p>
            <div className="emp-textfield ">
              <TextField
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              ></TextField>
            </div>
          </div>
          <div className="EmpManageForm-input-detail">
            <p>ສາຂາ</p>
            <div className="emp-textfield "  >
              <Select
                value={selectedBranch}
                onChange={handleMapbranch}
                className="emp-textfield-select"
              >
                {Array.isArray(branches) && branches.length > 0 ? (
                  branches.map((branch) => (
                    <MenuItem key={branch.branch_id} value={branch.branch_id}>
                      {branch.branch_name}
                    </MenuItem>
                  ))
                ) : (
                  <MenuItem disabled>No branches available</MenuItem>
                )}
              </Select>
            </div>
          </div>
        </div>
        <div className="EmpManageForm-input-grid3">
          <div>
            <p>ຮູບ</p>
            <div className="picturebox">
              {(typeof selectedData.pic === "string" &&
                selectedData.pic !== "") ||
              (pic && typeof pic !== "undefined") ? (
                <img
                  src={
                    typeof selectedData.pic === "string"
                      ? selectedData.pic
                      : URL.createObjectURL(pic!)
                  }
                  alt="Employee Photo"
                  className="employee-photo"
                />
              ) : (
                <div>No photo available</div>
              )}
            </div>
          </div>
          <div>
            <Button
              component="label"
              role={undefined}
              variant="contained"
              tabIndex={-1}
              startIcon={<CloudUploadIcon />}
            >
              Upload file
              <input
                 type="file"
                onChange={(e) =>
                  setPic(e.target.files ? e.target.files[0] : null)
                }
                style={{ display: "none" }}
                
              />
            </Button>
          </div>
        </div>
      </div>
      <div className="EmpManageForm-button">
        <Button onClick={handleSave}>ບັນທືກ</Button>
        <Button onClick={handleEdit}>ແກ້ໄຂ</Button>
        <Button onClick={handleClear}>ລ້າງ</Button>
        <Button onClick={handleDelete}>ລົບ</Button>
      </div>
      <SaveCompleteSnackbar />
      <ErrorSaving />
      <EditSuccess />
      <DeleteSuccess />
      <ErrorEdit />
    </div>
  );
}

export default EmpManageForm;
