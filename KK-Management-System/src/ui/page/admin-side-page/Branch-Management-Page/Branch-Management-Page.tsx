import './Branch-Management-Page.css'
import SideBar from '../../../style/AdminPage-sideBar'
 import BranchForm from './component/Branch-Form'
 import BranchDisPlay from './component/Branch-Display'
function BranchManagePage () {
    return(
        <div className="Branch-manage-page">
            <div className="Branch-page-first-grid">
                <SideBar/> 
            </div>
            <div className="Branch-page-second-grid">
                <BranchForm/>
                <BranchDisPlay/>
                

                

            </div>

        </div>
    )
}
export default  BranchManagePage