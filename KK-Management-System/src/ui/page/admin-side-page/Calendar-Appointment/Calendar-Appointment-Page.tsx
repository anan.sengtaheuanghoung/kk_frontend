import './Calendar-Appointment-Page.css'
import SideBar from '../../../style/AdminPage-sideBar'
import CalendarSelection from './component/Calendar-Selection'
function CalendarPage () {
    return( 
        <div className="calendar-page">
        <div className="calendar-first-grid">
          <SideBar />
        </div>
        <div className="calendar-second-grid">
            <CalendarSelection/>
        </div>
      </div>
    )
}
export default CalendarPage