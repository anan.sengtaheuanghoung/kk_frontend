// import { createBrowserRouter } from "react-router-dom";

// const routers = createBrowserRouter ([
//     {
//       element: <Root />,
//       children: [
//         {
//           path: "events/:id",
//           element: <Event />,
//           loader: eventLoader,
//         },
//       ],
//     },
//     {
//         path: "/",
//       element: <Root />,
//       children: [
//         {
//           path: "events/:id",
//           element: <Event />,
//           loader: eventLoader,
//         },
//       ],
//     }
//   ])



// import { BrowserRouter as Router, Routes, Route, Navigate, BrowserRouter } from 'react-router-dom';

// // Components for different pages
// import HomePage2 from '../../page/HomePage2/HomePage2';
// import BarberSelection from '../../page/Barber-Selection-Page/component/Barber-Selection';
// import IncomePage from '../../page/admin-side-page/Report/Income/Income-page';
//   import ReactDOM from 'react-dom';

// // Custom function to create BrowserRouter based on user role
// const createCustomBrowserRouter = (userRole: string) => {
//   const routes = [
//     {
//       path: '/',
//       element: <HomePage2 />,
//      },
//     {
//       path: 'user',
//       element: <BarberSelection/>,
//      },
//   ];

//   // Add admin routes if the user is an admin
//   if (userRole === 'tom') {
//     routes.push({
//       path: 'admin',
//       element: <IncomePage />,
//      });
//   }

//   return (
//     <BrowserRouter>
//       <Routes>
//         {routes.map((route, index) => (
//           <Route
//             key={index}
//             path={route.path}
//             element={route.element}
//             // Add any additional props or guards here if needed
//           />
//         ))}
//         {/* Redirect to root if route not found */}
//         <Route path="*" element={<Navigate to="/" replace />} />
//       </Routes>
//     </BrowserRouter>
//   );
// };

// // Example usage
// const userRole = 'admin'; // or 'user'
// const router = createCustomBrowserRouter(userRole);

// ReactDOM.render(router, document.getElementById('root'));
