import DashBoardDisplay from './component/DashBoardDisplay' 
import './AdminDashBoard-Page.css'
import SideBar from '../../../style/AdminPage-sideBar'
import EmpManageForm from '../Emp-Management-Page/component/Emp-Management-form'
export default function AdminDashBoardPage () {

    return(
        <div className="dashboard-page">
      <div className="dashboard-grid">
        <SideBar/>
       </div>

      <div className="dashboard-grid">
        <h1>ຍິນດີຕ້ອນຮັບ</h1>
        <DashBoardDisplay/>    
       </div>
    </div>
    )

}
