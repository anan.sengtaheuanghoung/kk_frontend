import axios from "axios";
import { getStoredToken } from "../../Login-axios";

//this route is use for fetching service Report
interface bookingData {
    app_id:string,
    cus_username:string,
    app_date:string,
    app_datetime:string,
    emp_name:string,
    app_status:string,

}

   export const fetchBookingReport = async (): Promise<bookingData[]> => {
    const authToken = getStoredToken();
    const headers = {
     Authorization: `Bearer ${authToken}`,
    };
     try {
       const response = await axios.get("http://localhost:8080/booking/report", {
         headers: headers,
       });
       console.log( response.data.report) 
       return response.data.report as bookingData[];  
     } catch (error) {
       console.error("Error fetching Service Report:", error);
       throw error;
     }
   };

