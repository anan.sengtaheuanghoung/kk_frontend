import "./Input-Import-Product-Page.css";
import SideBar from "../../../style/AdminPage-sideBar";
import InputProductForm from "./component/Input-Product-Form";
import InputProductDisplay from "./component/Input-Product-Display";
import { useState } from "react";
import { useParams } from "react-router-dom";

interface Product {
  pro_id: number;
  pro_name: string;
  pro_cost: number;
  amount: number;
}
function InputImportProduct() {
  const [productList, setSelectedProducts] = useState<Product[]>([]);

  const handleProductSelect = (product: Product) => {
    const productExists = productList.some(p => p.pro_id === product.pro_id);
    if (!productExists) {
      setSelectedProducts([...productList, product]);
    }
  };
  const handleDeleteProduct = (index: number) => {
    const updatedProducts = [...productList];
    updatedProducts.splice(index, 1); // Remove the product at the specified index
    setSelectedProducts(updatedProducts);
  };


  const {getOrderid} = useParams<{getOrderid: string}>();
  console.log( getOrderid)

  return (
    <div className="InputImportProduct-page">
      <div className="InputImportProduct-first-grid">
        <SideBar />
      </div>
      <div className="InputImport-second-grid">
        <p>ນໍາເຂົ້າສິນຄ້າ</p>
        <InputProductForm onProductSelect={handleProductSelect} />
        <InputProductDisplay
          productList={productList}
          onDeleteProduct={handleDeleteProduct}
        />
      </div>
    </div>
  );
}
export default InputImportProduct;
