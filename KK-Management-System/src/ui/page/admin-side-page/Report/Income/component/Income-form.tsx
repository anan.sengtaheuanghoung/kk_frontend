import "./Income-form.css";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers-pro";
import { AdapterDayjs } from "@mui/x-date-pickers-pro/AdapterDayjs";
 import { create } from "zustand";
import IncomePopUP from "./Income-popup";
import { useState } from "react";
import dayjs from "dayjs";

interface StoreState {
  startDate: string;
  endDate: string;
  setStartDate: (date: string) => void;
  setEndDate: (date: string) => void;
}

export const useIncomeStore = create<StoreState>((set) => ({
  startDate: '',
  endDate: '',
  setStartDate: (date: string) => set({ startDate: date }),
  setEndDate: (date: string) => set({ endDate: date }),
}));

function IncomeForm( ) { 
  const {  setStartDate, setEndDate } = useIncomeStore();
  const handleStartDateChange = (date: Date | null) => {
    if (date) {
      setStartDate(dayjs(date).format('YYYY-MM-DD')); // Format as needed
    }
  };
  const handleEndDateChange = (date: Date | null) => {
    if (date) {
      setEndDate(dayjs(date).format('YYYY-MM-DD')); // Format as needed
    }
  };    
  return (
    <div>
      <div className="income-Form">
        <div className="incomeForm-title">
          <p>ລາຍງານລາຍຮັບ </p>
        </div>
        <div className="incomeForm-input">
          <div className="incomeForm-input-grid1 ">
          <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={["DatePicker"]}>
                <DatePicker
                  label="ຈາກວັນທີ"
                  format="DD/MM/YYYY"
                  onChange={handleStartDateChange}

                />
              </DemoContainer>
            </LocalizationProvider>_
             
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={["DatePicker"]}>
                <DatePicker format="DD/MM/YYYY"
                  label="ເຖິງວັນທີ"

                  onChange={handleEndDateChange}
/>
              </DemoContainer>
            </LocalizationProvider>
           </div>
          <div className="incomeForm-input-grid2 ">
            
            <IncomePopUP />
    
          </div>
           
        </div>
      </div>
    </div>
  );
}
export default IncomeForm;
