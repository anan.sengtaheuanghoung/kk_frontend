 import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import { Button, TableHead } from "@mui/material";
import { FetchAppointment } from "../../../../../service/http/axios-client/Admin-Side/Service/Appointment-by-admin";
import { useEffect, useState } from "react";
import { PostToChangeStatus } from "../../../../../service/http/axios-client/Admin-Side/Service/Appointment-by-admin";
import { useNavigate } from "react-router-dom";
import { State, create } from "zustand";

interface Product {
  app_id: string;
  cus_username: string;
  app_date: string;
  app_datetime: string;
  emp_name: string;
  app_status: string;
  cus_id: string;
  emp_id: string;
  branch_id: string;
}
interface serviceDisplayProps {
  searchData: string; // Define the type for the searchData prop
}

interface MapRowCountState {
  mapRowCount: number;
  setMapRowCount: (count: number) => void;
}

// Create a Zustand store
export const useMapRowCountStore = create<MapRowCountState>((set) => ({
  mapRowCount: 0, // Initial value of mapRowCount
  setMapRowCount: (count) => set({ mapRowCount: count }), // Function to update mapRowCount
}));;



function AddBookforUserdisplay({ searchData }: serviceDisplayProps  ) {
  const [AppointmentList, setAppointmentList] = useState<Product[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [searchQuery, setSearchQuery] = useState('');
  const navigate = useNavigate();
 
  const fetchAppointment = async () => {
    try {
      const appointmentList: Product[] = await FetchAppointment();
      setAppointmentList(appointmentList);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching product data:", error);
    }
  };
  useEffect(() => {
    fetchAppointment();
  }, [fetchAppointment]);

  const handleUseService = async (appointmentId: string) => {
    try {
      await PostToChangeStatus(appointmentId);
     } catch (error) {
      console.error("Error changing appointment status:", error);
    }
  };



  
  useEffect(() => {
    setSearchQuery(searchData);
  }, [searchData]); // Update searchQuery when searchData changes
 
  const filteredServiceList = AppointmentList.filter(appointment =>
    appointment.app_id.toLowerCase().includes(searchQuery.toLowerCase()) 
  );

  
  // Initialize the state with the length of filteredServiceList
  const [mapRowCount, setMapRowCount] = useState(filteredServiceList.length);
     useEffect(() => {
    // Update the state with the new length of filteredServiceList
    setMapRowCount(filteredServiceList.length);
  }, [filteredServiceList]);



  return (
    <div className="ExpeneseDisplay">
      <TableContainer sx={{ maxHeight: 525 }}>
        <Table  stickyHeader aria-label="sticky table" >
          <TableHead>
            <TableRow>
              <TableCell
                style={{ fontSize: 20, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ລະຫັດ
              </TableCell>
              <TableCell
                style={{ fontSize: 20, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ຊື່
              </TableCell>
              <TableCell
                style={{ fontSize: 20, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ວັນທີ
              </TableCell>
              <TableCell
                style={{ fontSize: 20, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ເວລາ
              </TableCell>
              <TableCell
                style={{ fontSize: 20, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ຊ່າງຕັດຜົມ
              </TableCell>
              <TableCell
                style={{ fontSize: 20, fontFamily: "phetsarath ot " }}
                align="center"
              >
                ສະຖານະ
              </TableCell>
             </TableRow>
          </TableHead>
          <TableBody>
          {loading ? (
              <TableRow>
                <TableCell  style={{fontSize:18,fontFamily:"phetsarath ot",color:"red"}} colSpan={8}>ຍັງບໍ່ມີການຈອງ.....</TableCell>
              </TableRow>
            ) : (
              filteredServiceList.map((appointment) => (
                <TableRow key={appointment.app_id}>
                  <TableCell align="center" className="Map-ProductList">{appointment.app_id}</TableCell>
                  <TableCell align="center" className="Map-ProductList">{appointment.cus_username}</TableCell>
                  <TableCell align="center" className="Map-ProductList">{appointment.app_date}</TableCell>
                  <TableCell  align="center" className="Map-ProductList">{appointment.app_datetime}</TableCell>
                  <TableCell  align="center"className="Map-ProductList">{appointment.emp_name}</TableCell>
                  <TableCell align="center" className="Map-ProductList">{appointment.app_status}</TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
     </div>
  );
}

export default AddBookforUserdisplay;
