import { Button, FormControl, TextField } from "@mui/material";
import "./SignUp-Form.css";
import { useState } from "react";
import { handleUserSignUp } from "../../../../service/http/axios-client/SignUp-axios";
import { useNavigate } from "react-router-dom";
import { CreateAccount,useSuccessCreateAccount } from "../../../style/Alert/CreateAccountSuccessfull";
function SignUpForm() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate(); // Obtain the navigate function using useNavigate hook
  const setopenSuccessCreateAccount = useSuccessCreateAccount((state) => state.setopenSuccessCreateAccount);

  const HandleSignUp =  () =>{
    handleUserSignUp( username,email, phoneNumber, password,
      () => {
        setTimeout(() => {
          navigate('/LoginPage');
        }, 1500);
      },
      () => {
        setopenSuccessCreateAccount(true); // Display error Snackbar
      },
    )
  } 
  return (
    <div>
      <FormControl className="SignUp-form-stye">
        <h1>ສ້າງບັນຊີ</h1>
        <div className="SignUP-form-Input">
          <h4>ຊື່</h4>
          <TextField
            placeholder="Username"
            type="text"
            onChange={(e) => setUsername(e.target.value)}
            style={{ width: 300, marginTop: -20 }}
          />
        </div>
        <div className="SignUP-form-Input">
          <h4>ອີເມວ</h4>
          <TextField
            placeholder="Email"
            type="email"
            onChange={(e) => setEmail(e.target.value)}
            style={{ width: 300, marginTop: -20 }}
          />
        </div>
        <div className="SignUP-form-Input">
          <h4>ເບີໂທ</h4>
          <TextField
            placeholder="Phone Number"
            type="text"
            onChange={(e) => setPhoneNumber(e.target.value)}
            style={{ width: 300, marginTop: -20 }}
          />
        </div>
        <div className="SignUP-form-Input">
          <h4>ລະຫັດຜ່ານ</h4>
          <TextField
            placeholder="Password"
            type="password"
            onChange={(e) => setPassword(e.target.value)}
            style={{ width: 300, marginTop: -20 }}
          />
        </div>
        <Button variant="contained" onClick={ HandleSignUp}>ສ້າງບັນຊີ</Button> {/* Call handleSignUp function */}
      </FormControl>
      <CreateAccount/>
    </div>
  );
}

export default SignUpForm;
