import  { useEffect, useState } from 'react';
import { Link, useNavigate } from "react-router-dom";
import LOGO from "../../image/LOGO.png";
import "./Header.css";
 import ProfilePopper from './component/Profile-icon';
import { create } from 'zustand';
import { SlidetoDiv5 ,SlideSecondBox,SlideToApp,AboutUs, Choice} from '../page/HomePage2/component/HomePage2Detail';
interface HeaderProps {
  isLoggedIn: boolean;
 }


// interface AuthState {
//   isLoggedIn: boolean;
//   setIsLoggedIn: (value: boolean) => void;
// }
// export const useAuthStore = create<AuthState>((set) => ({
//   isLoggedIn: false,
//   setIsLoggedIn: (value: any) => set({ isLoggedIn: value }),
// }));

interface AuthState {
  isLoggedIn: boolean;
  setIsLoggedIn: (value: boolean) => void;
   
}

export const useAuthStore = create<AuthState>((set) => ({
  // Check localStorage to initialize isLoggedIn state
  isLoggedIn: !!localStorage.getItem('token'), // true if token exists, false otherwise
  setIsLoggedIn: (value) => {
    set({ isLoggedIn: value });

    // Update localStorage based on login state
    if (value) {
      localStorage.setItem('isLoggedIn', 'true');
    } else {
      localStorage.removeItem('isLoggedIn');
      localStorage.removeItem('token'); // Clear token if logged out
    }
  },
}));




function Header({ }: HeaderProps) {
 
  const isLoggedIn = useAuthStore((state) => state.isLoggedIn);
   // You can now use SlidetoDiv5 directly in your Header component
  

  const navigate = useNavigate();
   return (
    <header>
      <div className='mainheader'>
      <a className="option-element"  onClick={() => navigate(`/`)}>ໜ້າຫຼັກ</a>
      <a className="option-element" onClick={SlideSecondBox} >ບໍລິການ</a>
      <a className="option-element" onClick={Choice}>ຊ່ອງທາງຕິດຕໍ່</a>
      <div className="logo-box option-element">
        <div>
          <img src={LOGO} alt="My Image" />
          <p className='txt-logo' >KICKS & KUTS</p>
        </div>
      </div>
      <a className="option-element" onClick={SlideToApp}>ຈອງ</a>
      <a className="option-element" onClick={AboutUs}>ກ່ຽວກັບເຮົາ</a>
      <a className="option-element" onClick={SlidetoDiv5} >ຊ່າງຕັດຜົມ</a>
      </div>
      <div className="button-position">
        {isLoggedIn ? (
         <a > <ProfilePopper  /></a>  
        ) : (
             <button  onClick={() => navigate(`/LoginPage `)} >ເຂົ້າສູ່ລະບົບ</button> 
         )}
      </div>
    </header>
  );
}

export default Header;
