 import'./Report-Bill-Page.css'
  import SideBar from '../../../../style/AdminPage-sideBar'
 import ReportBillForm from './component/Report-Bill-Form'
 import ReportBillDisplay from './component/Report-Bill-Display'
import { Dayjs } from 'dayjs';
import { SetStateAction, useState } from 'react';
 
function ReportBillPage () {
    //handle text search 
   const [searchQuery, setSearchQuery] = useState('');
   const handleSearch = (value: SetStateAction<string>) => {
     setSearchQuery(value);
   };
 
   //handle date Rangepicker 
   const [startDate, setStartDate] = useState<Dayjs | null>(null);
   const [endDate, setEndDate] = useState<Dayjs | null>(null);
   const handleDateChange = (start: Dayjs | null, end: Dayjs | null) => {
     setStartDate(start);
     setEndDate(end);
   };
    return(
        <div className="Report-Bill-page">
        <div className="Report-Bill-first-grid">
          <SideBar />
        </div>
        <div className="Report-Bill-second-grid">
            <ReportBillForm onSearch={handleSearch} onDateChange={handleDateChange}/>
            <ReportBillDisplay searchData={searchQuery}  startDate={startDate} endDate={endDate}/>
        
        </div>
     </div>
    )
}
export default ReportBillPage