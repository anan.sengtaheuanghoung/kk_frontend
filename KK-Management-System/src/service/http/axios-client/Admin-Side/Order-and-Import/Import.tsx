import axios from "axios";
import { getStoredToken } from "../../Login-axios";

interface Product {
    order_id:string,
    order_date:string,
    sup_name:string,
    sup_pnumber:string,
}
const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
export const FetchOrderList = async (): Promise<Product[]> => {
    
    try {
      const response = await axios.get("http://localhost:8080/order-list", {
        headers: headers,
      });
      return response.data as Product[]; // Assuming the response data is an array of products
    } catch (error) {
      console.error("Error fetching product data:", error);
      throw error;
    }
};



// interface ProductDetail {
//   order_id:string,
// }

// export const FetchOrderDetail = async (): Promise<ProductDetail[]> => {
    
//     try {
//       const response = await axios.get("http://localhost:8080/order-details/:order_id", {
//         headers: headers,
//       });
//       return response.data as ProductDetail[];  
//     } catch (error) {
//       console.error("Error fetching product data:", error);
//       throw error;
//     }
// };




interface ProductDetail {
  pro_id: number;
  pro_name: string;
  pro_cost: number;
  amount: number;
  order_id: string;
}

export const FetchOrderDetail = async (order_id: string): Promise<ProductDetail[]> => {
  try {
    const response = await axios.get(`http://localhost:8080/order-details/${order_id}`, {
     headers:headers
    });
    return response.data as ProductDetail[];
  } catch (error) {
    console.error("Error fetching product data:", error);
    throw error;
  }
};







//import product
interface ProductData {
  imp_date: string;
  imp_total:string;
  order_id: string | undefined;
  branch_id: string;
  import_details: { amount: string; pro_id: string , price:string}[]
}

export const importProductPost = (
  imp_date: string,
  imp_total:string,
  order_id:string | undefined,
  branch_id: string,
  import_details: { amount: string; pro_id: string , price:string}[],
  onSuccess: () => void,
  onUnsuccess: () => void,
) => {
  const orderData: ProductData = {
    imp_date: imp_date,
    imp_total: imp_total,
    order_id:order_id,
    branch_id: branch_id,
    import_details: import_details,
  };
  console.log("this is import data",orderData)


  axios
    .post(`http://localhost:8080/imports`, orderData,{
      headers:headers
    })
    .then((response) => {
      onSuccess();
      console.log("import successful:", response.data);
     })
    .catch((error) => {
      onUnsuccess();
      console.error("import during order:", error.message);
     });
};

