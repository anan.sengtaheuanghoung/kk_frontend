import "./Admin-Datetime-Appointment-Page.css";
import SideBar from "../../../style/AdminPage-sideBar";
import AdminAppointmentForm from "./component/Admin-Datetime-Appointment-form";
function AdminAppointmentPage() {
  return (
    <div className="AdminAppoint-manage-page">
      <div className="AdminAppoint-page-first-grid">
        <SideBar />
      </div>
      <div className="AdminAppoint-page-second-grid">
        <AdminAppointmentForm/>
 
      </div>
    </div>
  );
}
export default AdminAppointmentPage;
