import { Button } from "@mui/material";
import "./HomePaege2Detail.css";
import viewpix from "../../../../image/viewpix.png";
import LOGO from "../../../../image/LOGO.png";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import { useEffect, useState } from "react";
import { useUserProfileStore } from "../../../../service/http/axios-client/Login-axios";
import { BookingCode, StatusChecking } from "../../../../service/http/axios-client/Appointment/BookingCode";
import { useAlreadyBooked } from "../../../style/Alert/AlreadyBooked";
import { BookedAlert } from "../../../style/Alert/AlreadyBooked";
import WhatApp from "../../../../image/Whatapp.png"
import Facebook from "../../../../image/facebook.png"
import branch2 from "../../../../image/branch2Pic.png"
import Instagram from "../../../../image/instagram.png"
import barber1 from "../../../../image/barber1.png"
import barber2 from "../../../../image/barber2.png"
import barber3 from "../../../../image/barber3.png"


export const SlidetoDiv5 = () => {
  const fifthBox = document.querySelector('.fifth-box')
  if (fifthBox) {
    fifthBox.scrollIntoView({ behavior: 'smooth' });
  }
};
export const SlideSecondBox = () => {
  const secondbox = document.querySelector('.second-box')
  if (secondbox) {
    secondbox.scrollIntoView({ behavior: 'smooth' });
  }
};
export const SlideToApp = () => {
  const Thirdbox = document.querySelector('.third-box')
  if (Thirdbox) {
    Thirdbox.scrollIntoView({ behavior: 'smooth' });
  }
};
export const AboutUs = () => {
  const FirstBox = document.querySelector('.first-box')
  if (FirstBox) {
    FirstBox.scrollIntoView({ behavior: 'smooth' });
  }
};
export const Choice = () => {
  const Sixthbox = document.querySelector('.sixth-box')
  if (Sixthbox) {
    Sixthbox.scrollIntoView({ behavior: 'smooth' });
  }
};






function HomePage2Detail( ) {
  //alert 
  const setUnsuccessOpen = useAlreadyBooked((state) => state.setUnsuccessOpen);
  //navigate
  const navigate  = useNavigate()
  //take cusId from login(provide data by zustand)
  const userProfileData = useUserProfileStore((state) => state.userData);
  const [cusid] = useState<string | undefined>(userProfileData?.cus_id);
   
  


  // Define the handleChecking function that calls StatusChecking with cusid
  const handleChecking = () => {
    if (cusid) {
      StatusChecking(cusid,
        () => {
          setUnsuccessOpen(true);
          setTimeout(() => {  
            BookingCode(cusid)
            navigate("/UserBillPage");
          }, 2000);  
        },
        () => {
          navigate("/BranchPage")
        }       
      );
    } else {
      console.error("Customer ID is undefined");
    }
  };

  // useEffect(() => {
  //   // Any effect that might be needed, for example:
  //   if (cusid) {
  //     console.log(`Customer ID: ${cusid}`);
  //   }
  // }, [cusid]);


  

  return (
    <div>
      <div className="first-box">
        <div className="firstBox-content ">
 
          <div className="grid-first">
            <p style={{ fontSize: 70, color: "white" ,fontFamily:"phetsarath ot"}}>
                ຍິນດີຕ້ອນຮັບສູ່ <br/> KICKS & KUTS Barber
            </p>
            {/* <p style={{ fontSize: 20, color: "white" }}>
              Sit a met consectetur adipiscing elit, sed a boy kin eat to fish
              <br />
              monkey money father mother and me eating chicken drinking the
              coffee
            </p> */}
            {/* <Link to={"/BarberSelectionPage"}>
            <Button
              variant="contained"
              disableElevation
              style={{ backgroundColor: "red" }}
            >
              Learn More
            </Button>
            </Link> */}
          </div>
          <div className="grid-first grid-first-img">
            <img src={viewpix} alt="" />
          </div>
        </div>
      </div>
      <div className="second-box">
        <div className="secondbox-content ">
          <div>
            <p style={{ fontSize: 50, textDecoration: "underline" ,fontFamily:"phetsarath ot "}}>
                ບໍລິການຂອງພວກເຮົາ
            </p>
          </div>
          <div className="grid-second">
            <div className="grid-second-style">
              <div>
                <p className="title-service">ຕັດຜົມ (ຜູ້ໃຫຍ່) </p>
                <p>
                  Haircut.
                  <br />
                  Finished off with a straight razor neck shave.
                </p>
              </div>

              <p className="title-service">125.000 KIP</p>
            </div>
            <div className="grid-second-style">
              <div>
                <p className="title-service">ຕັດຜົມ (ເດັກນ້ອຍ)</p>
                <p>
                  Haircut.
                  <br />
                  Serve with care cuts.
                </p>
              </div>

              <p className="title-service" >110.000 KIP</p>
            </div>
            <div className="grid-second-style">
              <div >
                <p className="title-service" >ຍ້ອມຜົມ</p>
                <p>
                  Dye Hair
                  <br />
                  Hightligths hair by your style.
                </p>
              </div>

              <p  className="title-service" >250.000 KIP</p>
            </div>
            <div className="grid-second-style">
              <div>
                <p className="title-service" >ສະຜົມ</p>
                <p>
                  Shampoo
                  <br />
                  Water till it clean.
                </p>
              </div>

              <p className="title-service" >50.000 KIP</p>
            </div>
            <div className="grid-second-style">
              <div>
                <p className="title-service">ແຖໜວດ </p>
                <p>
                  Shave (Beard)
                  <br />
                  Fresh and Clean
                </p>
              </div>

              <p className="title-service" >85.000 KIP</p>
            </div>
            <div className="grid-second-style">
              <div>
                <p className="title-service">ເຄື່ອງດື່ມ</p>
                <p>
                  Drinks
                  <br />
                  Stay Refreshed 
                </p>
              </div>

              <p className="title-service" >80.000 KIP</p>
            </div>
          </div>
        </div>
      </div>
      <div className="third-box">
        <div className="thirdbox-content ">
          <div className="grid-third grid-third-img">
            <img className="branch2-img" src={branch2} />
          </div>
          <div className="grid-third">
            <h1 style={{ fontSize: 45, color: "white" ,fontFamily:"phetsarath ot"}}>
            ທ່ານພ້ອມໃຊ້ບໍລິການຂອງເຮົາແລ້ວບໍ ?            </h1>
            {/* {renderButton()} */}
               <button className="grid-third-button" onClick={handleChecking}  >ຈອງຄິວຕັດຜົມ</button>
           </div>
        </div>
      </div>
      <div className="fourth-box">
        <div className="fourthbox-content">
          <div className="grid-fourth">
            <div className=" grid-fourth-style ">
              <h1>KICKS & KUTS</h1>
            </div>
            <div className=" grid-fourth-style ">
              <img src={LOGO} />
            </div>
          </div>
          <div className="grid-fourth">
            <div className=" grid-fourth-style grid-fourthgrid-fourth-txt ">
              <p style={{textDecoration: "underline"}}> ສະຖານທີ: </p> <br />
            </div>
            <div className=" grid-fourth-style grid-fourth-style-detail">
              <p   >
                ສາຂາໜອງບອນ ເມືອງໄຊເສດຖາ,</p>
              <p>ສາຂາຫາຍໂສກ ເມືອງຈັນທະບູລີ</p>
            </div>
          </div>
          <div className="grid-fourth">
            <div className=" grid-fourth-style grid-fourthgrid-fourth-txt">
              <p style={{textDecoration: "underline"}}> ເວລາເປີດທຳການ: </p> <br />
            </div>
            <div className=" grid-fourth-style grid-fourth-style-detail ">
              
              <p   >
              ເປີດ: ວັນຈັນ - ວັນອາທິດ (ປິດທຸກວັນພຸດ),</p>
              <p>ເວລາ: 10:00 – 19:00 (ວັນອາທິດປິດ 18:00) </p>
            </div>
          </div>
        </div>
        <div></div>
      </div>
      <div className="fifth-box">
        <div className="fifthbox-content ">
          <div>
            <p style={{ fontSize: 50, textDecoration: "underline",fontFamily:"phetsarath ot" }}>
              ຊ່າງຕັດຜົມ
            </p>
          </div>
          <div className="grid-fifth">
            <div className="grid-fifth-style">
              <img className="barber-img" src={barber1} />
              <h3>
                Just like the good ol'days. <br /> A series of hot towels followed by a
                straight razor and aftershave.
              </h3>
            </div>
            <div className="grid-fifth-style">
              <img  className="barber-img" src={barber2} />
              <h3>
                Just like the good ol'days. <br /> A series of hot towels followed by a
                straight razor and aftershave.
              </h3>
            </div>
            <div className="grid-fifth-style">
              <img  className="barber-img" src={barber3} />
              <h3>
                Just like the good ol'days. <br /> A series of hot towels followed by a
                straight razor and aftershave.
              </h3>
            </div>
          </div>
        </div>
      </div>
      <div className="sixth-box">
      <div className="sixthbox-content ">
          <div>
            <p   style={{ fontSize: 50, textDecoration: "underline",fontFamily:"phetsarath ot" }}>
            ຊ່ອງທາງຕິດຕໍ່
            </p>
          </div>
          <div className="grid-sixth">
            <div className="grid-sixth-style">
              <div >
               <img className="img-div" src={WhatApp} alt="" />
              </div>
              <div className="contact-detail">
                 <p>020 56997722</p>
                
              </div>
            </div>
            <div className="grid-sixth-style">
            <div >
               <img className="img-div-facebook" src={Facebook} alt="" />
              </div>
              <div className="contact-detail">
                <p>KICKS & KUTS </p>             
              </div>
            </div>
            <div className="grid-sixth-style">
            <div >
               <img className="img-div-ins" src={Instagram} alt="" />
              </div>
              <div className="contact-detail">
                <p>kicksandkut_lao</p>
                 
              </div>
            </div>
          </div>
        </div>
      </div>
      <BookedAlert/>
    </div>
  );
}
export default HomePage2Detail;
 