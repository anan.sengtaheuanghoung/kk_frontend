

import axios from "axios";
import { getStoredToken } from "../Login-axios";

 interface SupplierData {
  sup_name: string;
  sup_pnumber: string;
}

 //Save button
export const SaveSupplier = (
  sup_name: string,
  sup_pnumber: string,
  onSuccess: () => void,
  onError: (error: any) => void,
) => {
  const supplierData: SupplierData = {
      sup_name: sup_name,
      sup_pnumber: sup_pnumber,
      sup_id: ""
  };
  const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
  axios
    .post("http://localhost:8080/supplier", supplierData, {
      headers:headers,
    })
    .then((response) => {
      console.log("Supplier added successfully");
      console.log(response.data);
      onSuccess();  
    })
    .catch((error) => {
      console.error("Error adding supplier:", error);
      onError(error);  
    });
};



interface SupplierData {
    sup_id:string;
    sup_name: string;
    sup_pnumber: string;
  }
  
   //Save button
  export const EditSupplier = (
    sup_id:string,
    sup_name: string,
    sup_pnumber: string,

    onSuccess: () => void,
    onError: (error: any) => void,
  ) => {
    const supplierEditData: SupplierData = {
      sup_id:sup_id,
      sup_name: sup_name,
      sup_pnumber: sup_pnumber,
    };
    const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
    axios
      .put(`http://localhost:8080/supplier/${sup_id}`, supplierEditData, {
        headers:headers,
      })
      .then((response) => {
        console.log("Supplier added successfully");
        console.log(response.data);
        onSuccess();  
      })
      .catch((error) => {
        console.error("Error adding supplier:", error);
        onError(error);  
      });
  };



  //DeleteProtype
export const DeleteSupplier = (sup_id:string,
    onSuccess: () => void,
  ) => {
    const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };

    axios
      .delete(`http://localhost:8080/supplier/${sup_id}`, {
        headers:headers,
      })
      .then((response) => {
        console.log("Deletion successful");
        console.log(response.data);
        onSuccess();
      })
      .catch((err) => {
        console.error("Error during deletion:", err);
      });
  };

