import './Order-Product-Page.css'
import SideBar from '../../../style/AdminPage-sideBar'
import StockList from './component/OrderPro-StockList'
import OrderList from './component/OrderPro-OrderList'
import { useState } from 'react';
interface Product {
    pro_id: number;
    pro_name: string;
    pro_cost: number;
    amount: number;
  }
function OrderProduct () {
  const [selectedProducts, setSelectedProducts] = useState<Product[]>([]);

  const handleProductSelect = (product: Product) => {
    const productExists = selectedProducts.some(p => p.pro_id === product.pro_id);
    if (!productExists) {
      setSelectedProducts([...selectedProducts, product]);
    }
  };
  const handleDeleteProduct = (index: number) => {
    const updatedProducts = [...selectedProducts];
    updatedProducts.splice(index, 1); // Remove the product at the specified index
    setSelectedProducts(updatedProducts);
  }
    return(
        <div className="order-pro-page">
        <div className="order-pro-first-grid">
            <SideBar/> 

        </div>
        <div className="order-pro-second-grid">
            <p>ສັ່ງຊື້ສິນຄ້າ</p>
            <StockList onProductSelect={handleProductSelect}/>
            <OrderList  selectedProducts={selectedProducts} onDeleteProduct={handleDeleteProduct} />
        </div>

    </div>
    )
}
export default OrderProduct