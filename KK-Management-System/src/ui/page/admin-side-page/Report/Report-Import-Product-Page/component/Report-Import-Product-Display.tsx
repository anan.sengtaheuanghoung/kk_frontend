import './Report-Import-Product-Display.css'
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button } from "@mui/material";
import ReportImportPopUP from './Report-Import-Popup';
import dayjs, { Dayjs } from 'dayjs';
import { fetchImportReport } from '../../../../../../service/http/axios-client/Admin-Side/Report/ReportIncome';
import { useEffect, useState } from 'react';
interface ImportReportData {
  imp_id:string,
    imp_date:string,
    imp_total:string,
    branch_name:string,
}


interface ReportImportDisplayProps {
  searchData: string; 
  startDate: Dayjs | null;
  endDate: Dayjs | null;
}

function ReportImportProductDisplay ({ searchData, startDate, endDate }:ReportImportDisplayProps) {

  //store state
  const [ReportImport, setReportImport] = useState<ImportReportData[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  const fetchReportImport = async () => {
    try {
      const ImportList: ImportReportData[] = await fetchImportReport();
      setReportImport(ImportList);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching product data:", error);
    }
  };
  useEffect(() => {
    fetchReportImport();
  }, []);

  const filteredImportList = ReportImport.filter(importReport => {
    const ImportDate = dayjs(importReport.imp_date, 'DD/MM/YYYY');
    const impTotalString = importReport.imp_total.toString(); // Convert to string
    const matchesSearchQuery = importReport.imp_id.toLowerCase().includes(searchData.toLowerCase()) ||
      importReport.imp_date.toLowerCase().includes(searchData.toLowerCase()) ||
      impTotalString.toLowerCase().includes(searchData.toLowerCase()) || // Convert to lowercase before search
      importReport.branch_name.toLowerCase().includes(searchData.toLowerCase());
  
    const matchesDateRange = 
      (!startDate || ImportDate.isSame(startDate, 'day') || ImportDate.isAfter(startDate)) &&
      (!endDate || ImportDate.isSame(endDate, 'day') || ImportDate.isBefore(endDate, 'day'));
    return matchesSearchQuery && matchesDateRange;
  });
  
  

    return(
        <div className="ImportProDisplay">
      <TableContainer sx={{ maxHeight: 530 }}>
        <Table>
          <TableHead>
            <TableRow>
            <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ລະຫັດ</TableCell>
                <TableCell  style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ເວລາ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ລາຄາລວມ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ສາຂາ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center"> </TableCell>
             </TableRow>
          </TableHead>
          <TableBody>
          {loading ? (
              <TableRow>
                <TableCell colSpan={5}>Loading...</TableCell>
              </TableRow>                      
            ) : (
              filteredImportList.map((importReport) => (
                <TableRow key={importReport.imp_id}>
                  <TableCell className="Map-ReportOrder">
                    {importReport.imp_id}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {importReport.imp_date}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {importReport.imp_total}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {importReport.branch_name}
                  </TableCell>                  
                  <TableCell className="Map-ReportOrder"  >
                    <ReportImportPopUP  importID={importReport.imp_id} branchName={importReport.branch_name} Date={importReport.imp_date}/>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>             
        </Table>
      </TableContainer>
    </div>
    )
}
export default ReportImportProductDisplay