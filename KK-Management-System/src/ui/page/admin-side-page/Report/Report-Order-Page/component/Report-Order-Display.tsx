import "./Report-Order-Display.css";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button } from "@mui/material";
import ReportOrderPopUP from "./ReportOrder-Popup";
import { useEffect, useState } from "react";
import { fetchOrderReport } from "../../../../../../service/http/axios-client/Admin-Side/Report/ReportOrder";
import dayjs, { Dayjs } from "dayjs";

interface OrderReportData {
  order_id: string;
  order_date: string;
  sup_name: string;
  branch_name: string;
}
interface ReportOrderDisplayProps {
  searchData: string; 
  startDate: Dayjs | null;
  endDate: Dayjs | null;
}


function ReportOrderDisplay({ searchData, startDate, endDate }:ReportOrderDisplayProps) {

  //store state
  const [ReportOrder, setReportOrder] = useState<OrderReportData[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  const fetchReportOrder = async () => {
    try {
      const OrderList: OrderReportData[] = await fetchOrderReport();
      setReportOrder(OrderList);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching product data:", error);
    }
  };

  useEffect(() => {
    fetchReportOrder();
  }, []);

  const filteredOrderReportList = ReportOrder.filter(order => {
    const orderDate = dayjs(order.order_date, 'DD/MM/YYYY');
    const matchesSearchQuery = order.order_id.toLowerCase().includes(searchData.toLowerCase()) ||
      order.order_date.toLowerCase().includes(searchData.toLowerCase()) ||
      order.sup_name.toLowerCase().includes(searchData.toLowerCase()) ||
      order.branch_name.toLowerCase().includes(searchData.toLowerCase());

      const matchesDateRange = 
    (!startDate || orderDate.isSame(startDate, 'day') || orderDate.isAfter(startDate)) &&
    (!endDate  || orderDate.isSame(endDate, 'day') || orderDate.isBefore(endDate, 'day' ) );


    return matchesSearchQuery && matchesDateRange;
  });

  
  return (
    <div className="ReportOrderDisplay">
      <TableContainer sx={{ maxHeight: 530 }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell
                style={{ fontSize: 23, fontFamily: "phetsarath ot" }}
                align="center"
              >
                ລະຫັດສັ່ງຊື້
              </TableCell>
              <TableCell
                style={{ fontSize: 23, fontFamily: "phetsarath ot" }}
                align="center"
              >
                ວັນທີ
              </TableCell>
              <TableCell
                style={{ fontSize: 23, fontFamily: "phetsarath ot" }}
                align="center"
              >
                ຊື່ຜູ້ສະໜອງ
              </TableCell>
              <TableCell
                style={{ fontSize: 23, fontFamily: "phetsarath ot" }}
                align="center"
              >
                ສາຂາ
              </TableCell>
              <TableCell
                style={{ fontSize: 23, fontFamily: "phetsarath ot" }}
                align="center"
              >
                
              </TableCell>
             </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={5}>Loading...</TableCell>
              </TableRow>
            ) : (
              filteredOrderReportList.map((order) => (
                <TableRow key={order.order_id}>
                  <TableCell className="Map-ReportOrder">
                    {order.order_id}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {order.order_date}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {order.sup_name}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {order.branch_name}
                  </TableCell>                  
                  <TableCell className="Map-ReportOrder"  >
                    <ReportOrderPopUP  orderID={order.order_id} supName={order.sup_name} branchName={order.branch_name} Date={order.order_date}/>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
export default ReportOrderDisplay;

