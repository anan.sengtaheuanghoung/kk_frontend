import { Alert, Snackbar, Stack } from "@mui/material";
import { create } from "zustand";

interface SnackbarState {
  open: boolean;
  setOpen: (open: boolean) => void;
}

export const useSnackbarStore = create<SnackbarState>((set) => ({
  open: false,
  setOpen: (open) => set({ open }),
}));

export const SuccessSnaekbar = () => {
  const { open, setOpen } = useSnackbarStore();

  const handleClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <div>
      <Snackbar
        open={open}
        autoHideDuration={1200}
        onClose={handleClose}
        message="This Snackbar will be dismissed in 5 seconds."
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Stack sx={{ width: "100%" }} spacing={2}>
          <Alert
            severity="success"
            style={{ width: 240, fontSize: 20, fontFamily: "phetsarath ot" }}
          >
            ການເຂົ້າສູ່ລະບົບສໍາເລັດ
          </Alert>
        </Stack>
      </Snackbar>
    </div>
  );
};
