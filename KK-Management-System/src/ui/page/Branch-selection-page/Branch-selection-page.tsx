import Header from "../../style/Header";
import BranchSelection from "./component/Branch-selection";
import "./Branch-selection-page.css";
function BranchPage() {
  return (
    <div>
      <Header isLoggedIn={false} />
      <div className="Branch-page">
        <BranchSelection />
      </div>
    </div>
  );
}
export default BranchPage;
