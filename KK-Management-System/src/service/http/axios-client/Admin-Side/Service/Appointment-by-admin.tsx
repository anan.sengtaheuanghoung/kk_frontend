import axios from "axios";
import { getStoredToken } from "../../Login-axios";



interface Product {
    app_id:string;
    cus_username:string;
    app_date:string;
    app_datetime:string;
    emp_name:string;
    app_status:string;
    cus_id:string;
    emp_id:string;
    branch_id:string;
}
const authToken = getStoredToken();
const headers = {
    Authorization: `Bearer ${authToken}`,
};








export const FetchAppointment = async (): Promise<Product[]> => {
    try {
      const response = await axios.get("http://localhost:8080/appointments", {
        headers: headers,
      });
       return response.data as Product[]; // Assuming the response data is an array of products
    } catch (error) {
      console.error("Error fetching product data:", error);
      throw error;
    }
};










 

export const PostToChangeStatus = (id: string) => {
    axios
        .put(`http://localhost:8080/appointments/${id}/toggleStatus`)
        .then((response) => {
            console.log("Appointment status update successful");
            console.log(response.data);
        })
        .catch((err) => {
            console.error("Error during appointment status update:", err.message);
        });
};





// //this route is use for fetching employee
// interface EmployeeData {
//  emp_id:string,
//  emp_name:string,
// }
// export const fetchEmployeeData = async (): Promise<EmployeeData[]> => {
  
//   try {
//     const response = await axios.get("http://localhost:8080/management/edit-employee", {
//       headers: headers,
//     });
     
//     return response.data ;  
//   } catch (error) {
//     console.error("Error fetching employees data: ", error);
//     throw error;
//   }
// };







interface EmployeeData {
  emp_id:string,
  emp_name:string,
  branch_id:string,
 }

export const fetchEmployeeData = async (): Promise<EmployeeData[]> => {
  const authToken = getStoredToken();
  const headers = {
    'Authorization': `Bearer ${authToken}`,
   };

  try {
    const response = await axios.get('http://localhost:8080/management/edit-employee', { headers });

    // If response is an array, return it directly
    if (Array.isArray(response.data)) {
      console.log("see data ",response.data)
      return response.data; // For "ceo"
    }

    // If response is a single object, wrap it in an array
    if (typeof response.data === 'object') {
      return [response.data]; // For "admin"
    }

    throw new Error("Unexpected response structure"); // Handle unexpected cases
  } catch (error) {
    console.error("Error fetching branches:", error);
    throw error;
  }
};










//import product
interface appointmentData {
  cus_username:string | undefined;
  emp_name: string;
  app_date:string;
  app_datetime:string;
 
}
export const AppointmentByAdmin = (
  cus_username:string | undefined,
  emp_name:string,
  app_date:string,
  app_datetime:string,
  onSuccess: () => void,
  onUnsuccess: () => void,
 
) => {
  const AppointDataDetail: appointmentData = {
      cus_username: cus_username,
      emp_name: emp_name,
      app_date:app_date,
      app_datetime:app_datetime,
  
  };
  console.log(AppointDataDetail)
  
  axios
    .post(`http://localhost:8080/admin-appointment`,AppointDataDetail,{
      headers:headers
    })
    .then((response) => {
      onSuccess();
      
       console.log(" appointment  by admin successful:", response.data);
     })
    .catch((error) => {
      onUnsuccess();
      console.error(" appointment by admin  fail", error.message);
     });
};


