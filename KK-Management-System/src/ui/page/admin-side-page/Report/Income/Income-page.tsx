import './Income-page.css'
import SideBar from '../../../../style/AdminPage-sideBar'
import IncomeForm from './component/Income-form'
import IncomeDisplay from './component/Income-Display'
import { useState } from 'react';


function IncomePage () {

    return( 
        <div className="income-page">
        <div className="income-first-grid">
          <SideBar />
        </div>
        <div className="income-second-grid">
        <IncomeForm />
        <IncomeDisplay />

      </div>
      </div>
    )
}
export default IncomePage