import './Sup-Display.css'
// import { TextField } from "@mui/material";

// function SupDisPlay () {
//     return(
//         <div className="SupManageDisplay">
//         <div className="SupManageDisplay-grid1 ">
//           <p>ຄົ້ນຫາ:</p>
//           <TextField className="SupManageDisplay-grid1-textfield  "
//           sx={{
//               "& fieldset": { border: 'none' },
//             }}
//         />
//         </div>
//         <div className="SupManageDisplay-grid2"></div>
//       </div>
//     )
// }
// export default SupDisPlay


import {
    
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  styled,
  tableCellClasses,
} from "@mui/material";
 import React, {  ReactNode, useEffect, useState } from "react";
import axios from "axios";
import { create } from "zustand";
import { getStoredToken } from '../../../../../service/http/axios-client/Login-axios';
 
const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));





interface useSupplierStoreState {
  selectedData: {
    sup_id:string,
    sup_name:string,
    sup_pnumber:string,
  
  },
  setSelectedData: (data: {
    sup_id:string,
    sup_name:string,
    sup_pnumber:string,

  }) => void;
}




// Zustand store
export const useSupStore = create<useSupplierStoreState>((set) => ({
  selectedData: {
    sup_id:"",
    sup_name:"",
    sup_pnumber:"",
   
  },
  setSelectedData: (data: any) => set({ selectedData: data }),
}));



interface Column {
  id: keyof Data;  
  label: string;
  minWidth?: number;
  align?: "left" | "center" | "right"; 
}


const columns: readonly Column[] = [
  { id: "ID", label: "ລະຫັດ" },
  { id: "Name", label: "ຊື່" },
  { id: "PhoneNumber", label: "ເບີໂທ" },

 
];

interface Data {
  
   
  sup_id:string;
    sup_name:string;
    sup_pnumber:string;
  ID: number;
  Name: string;
  PhoneNumber:string;
   
  
 
}

function  ProTypedisplay() {
  const [rows, setRows] = useState<Data[]>([]);
  const { selectedData, setSelectedData } = useSupStore();
  const [searchQuery, setSearchQuery] = useState("");
 

  const handleRowClick = (row: {sup_id:any;sup_name:any ,sup_pnumber:any}) => {
    setSelectedData({
      sup_id: row.sup_id,
      sup_name: row.sup_name,
      sup_pnumber:row.sup_pnumber
     });

  };

  //fect data to show on table
 const fetchSupplierData = () => {
   const authToken = getStoredToken();
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };

  axios
    .get("http://localhost:8080/suppliers", {
      headers:headers,
    })
    .then((response) => {
      setRows(response.data);
    })
    .catch((error) => {
      console.error("Error fetching protypedata data:", error);
    });
};
useEffect(() => {
  fetchSupplierData();
}, [selectedData]);

const filteredRows = rows.filter(
  (row) =>
    typeof row.sup_name === "string" &&
    row.sup_name.toLowerCase().includes(searchQuery.toLowerCase())
);
 
  return (
    <div className="SupManageDisplay">
      <div className="SupManageDisplay-grid1">
        <p>ຄົ້ນຫາ:</p>
        <TextField className="SupManageDisplay-grid1-textfield" 
         value={searchQuery}
         onChange={(e) => setSearchQuery(e.target.value)}
         sx={{
          "& fieldset": { border: 'none' },
        }}
         />

      </div>
      <div className="SupManageDisplay-grid2">
        <TableContainer sx={{ maxHeight: 340 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{
                      minWidth: column.minWidth,
                      fontSize: 24,
                      width: 70,
                      fontFamily:"phetsarath ot",
                    }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
            {filteredRows.map((row) => (
                 <StyledTableRow     key={row.sup_id} onClick={() => handleRowClick(row)}>
                  <StyledTableCell  style={{fontSize:17}} >{row.sup_id}</StyledTableCell>
                  <StyledTableCell style={{fontSize:17}}>{row.sup_name}</StyledTableCell>
                  <StyledTableCell style={{fontSize:17}}>{row.sup_pnumber}</StyledTableCell>

                </StyledTableRow>
                ))}  

             </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}


export default ProTypedisplay