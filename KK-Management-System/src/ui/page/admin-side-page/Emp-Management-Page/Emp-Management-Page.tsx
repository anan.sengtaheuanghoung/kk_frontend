import SideBar from "../../../style/AdminPage-sideBar";
import "./Emp-Management-Page.css";
import EmpManageForm from "./component/Emp-Management-form";
import EmpManagedisplay from "./component/Emp-Management-display";
function EmpManagementPage() {
  return (
    <div className="emp-management-page">
      <div className="emp-first-grid">
        <SideBar />
      </div>

      <div className="emp-second-grid">
        <EmpManageForm/>
        <EmpManagedisplay/>
         

      </div>
    </div>
  );
}
export default EmpManagementPage;
