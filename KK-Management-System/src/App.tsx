import { Routes, Route,} from "react-router-dom";
import "./App.css";
import LoginPage from "./ui/page/Login-From/Login-Page";
import Header from "./ui/style/Header";
import Footer from "./ui/style/Footer";
import HomePage2 from "./ui/page/HomePage2/HomePage2";
import SignUpPage from "./ui/page/SignUp-Page/SignUp-Page";
import BarberSelectionPage from "./ui/page/Barber-Selection-Page/Barber-Seletion-Page";
import DateTimePage from "./ui/page/DateTimePage/DateTimePage";
import BranchPage from "./ui/page/Branch-selection-page/Branch-selection-page";
import SideBar from "./ui/style/AdminPage-sideBar";
import EmpManagementPage from "./ui/page/admin-side-page/Emp-Management-Page/Emp-Management-Page";
import ProManagePage from "./ui/page/admin-side-page/Pro-Management-Page/Product-Management-Page";
import ProTypePage from "./ui/page/admin-side-page/ProductType-Management-Page/ProType-Management-Page";
import CusManagePage from "./ui/page/admin-side-page/Cus-Management-Page/Cus-Manage-Page";
import SupManagePage from "./ui/page/admin-side-page/Sup-Management-Page/Sup-Management-Page";
import BranchManagePage from "./ui/page/admin-side-page/Branch-Management-Page/Branch-Management-Page";
import AppointmentManagePage from "./ui/page/admin-side-page/Appointment-Management-System/Appointment-Manage-Page";
import AdminAppointmentPage from "./ui/page/admin-side-page/Admin-Datetime-Appointment-Page/Admin-Datetime-Appointment-Page";
import ReportImportProduct from "./ui/page/admin-side-page/Report/Report-Import-Product-Page/Report-Import-Product";
import ReportOrderPage from "./ui/page/admin-side-page/Report/Report-Order-Page/Report-Order-Page";
import ReportBillPage from "./ui/page/admin-side-page/Report/Report-Bill-Page/Report-Bill-Page";
import OrderProduct from "./ui/page/admin-side-page/Order-Product-Page/Order-Product-Page";
import IncomePage from "./ui/page/admin-side-page/Report/Income/Income-page";
import Expenesepage from "./ui/page/admin-side-page/Report/Expenese/Report-Expenese-Page";
import BookingPage from "./ui/page/admin-side-page/Report/Booking/Report-Booking-Page";
import StockPage from "./ui/page/admin-side-page/Report/Stock/Report-Stock-Page";
import CalendarPage from "./ui/page/admin-side-page/Calendar-Appointment/Calendar-Appointment-Page";
import ImportProductPage from "./ui/page/admin-side-page/Import-Product-Page/Import-Product-Page";
import AddBookingPage from "./ui/page/admin-side-page/Admin-Booking-Page/Admin-Booking-Page";
import InputImportProduct from "./ui/page/admin-side-page/Input-Import-Product/Input-Import-Product-Page";
import UseServicePage from "./ui/page/admin-side-page/Use-Service-Page/Use-Service-Page";
import UserBillPage from "./ui/page/User-Bill/UserBillPage";
import AdminDashBoardPage from "./ui/page/admin-side-page/DashBoard/AdminDashBoard-Page";
import { RoleChecker } from "./service/check-user-role/user-role";
import UserNameCheck from "./ui/page/Login-From/component/UserNameCheck";
import ConfirmPassword from "./ui/page/Login-From/component/ConfirmPassword";
import { SuccessSnaekbar } from "./ui/style/component/Successfull-Snaekbar";
import {UnSuccessful} from "./ui/style/component/UnSuccessFull-sideBar";
import BookingSevice from "./ui/page/admin-side-page/Booking-Service.tsx/Booking-Service";

const App: React.FC = () => {
  const token = localStorage.getItem('token') || "";

  return (
    <Routes>
      {/* Public routes */}
      <Route path="/" element={<HomePage2 />} />
        
      <Route path="/Header" element={<Header isLoggedIn={false} />} />      
      <Route path="/footer" element={<Footer/>} />
      <Route path="/footer" element={<SideBar/>} />
      <Route path="/LoginPage" element={<LoginPage />} />
      <Route path="/SignUpPage" element={<SignUpPage />} />
      <Route path="/UserNameCheck" element={<UserNameCheck/>} />
      <Route path="/ConfirmPassword" element={<ConfirmPassword/>} />
      <Route path="/SuccessSnaekbar" element={<SuccessSnaekbar/>} />
      <Route path="/UnSuccessful" element={<UnSuccessful/>} />




      {/* Private route accessible by User*/}
      <Route path="/BarberSelectionPage" element={<RoleChecker element={<BarberSelectionPage />} adminOnly={true} user={true}token={token} />} />
      <Route path="/BranchPage" element={<RoleChecker element={<BranchPage />} adminOnly={true} user={true} token={token} />} />
      <Route path="/DateTimePage" element={<RoleChecker element={<DateTimePage />} adminOnly={true} user={true} token={token} />} />
      <Route path="/UserBillPage" element={<RoleChecker element={<UserBillPage />} adminOnly={true} user={true} token={token} />} />



      {/* Private routes accessible to both admin and users */}
      <Route path="/ReportOrderPage" element={<RoleChecker element={<ReportOrderPage />} token={token} adminOnly={true} />} />
      <Route path="/BookingSevice" element={<RoleChecker element={<BookingSevice />} token={token} adminOnly={true} />} />
      <Route path="/OrderProduct" element={<RoleChecker element={<OrderProduct />} token={token} adminOnly={true} />} />
      <Route path="/ImportProductPage" element={<RoleChecker element={<ImportProductPage />} token={token} adminOnly={true} />} />
      <Route path="/EmpManagementPage" element={<RoleChecker element={<EmpManagementPage />} ceo={true}  token={token} adminOnly={true} />} />
      <Route path="/InputImportProduct/:getOrderid" element={<RoleChecker element={<InputImportProduct />} token={token} adminOnly={true}/>} />
      <Route path="/AppointmentManagePage" element={<RoleChecker element={<AppointmentManagePage />} token={token} adminOnly={true}/>} />
      <Route path="/AdminAppointmentPage" element={<RoleChecker element={<AdminAppointmentPage />} token={token} adminOnly={true}/>} />
      <Route path="/UseServicePage/:appId/:cusId/:branchId/:empId" element={<RoleChecker element={<UseServicePage />} token={token} adminOnly={true}/>} />
      <Route path="/AdminDashBoardPage" element={<RoleChecker element={<AdminDashBoardPage />} token={token} adminOnly={true}/>} />
      <Route path="/ProTypePage" element={<RoleChecker element={<ProTypePage />} token={token} adminOnly={true}/>} />
      <Route path="/CusManagePage" element={<RoleChecker element={<CusManagePage />} token={token} adminOnly={true}/>} />
      <Route path="/SupManagePage" element={<RoleChecker element={<SupManagePage />} token={token} adminOnly={true} />} />
      <Route path="/BranchManagePage" element={<RoleChecker element={<BranchManagePage />} token={token}  ceo={true}/>} />
      <Route path="/ReportBillPage" element={<RoleChecker element={<ReportBillPage />} token={token} adminOnly={true}/>} />
      <Route path="/IncomePage" element={<RoleChecker element={<IncomePage />} token={token} adminOnly={true}/>} />
      <Route path="/Expenesepage" element={<RoleChecker element={<Expenesepage />} token={token} adminOnly={true}/>} />
      <Route path="/BookingPage" element={<RoleChecker element={<BookingPage />} token={token} adminOnly={true}  employee={true}/>} />
      <Route path="/StockPage" element={<RoleChecker element={<StockPage />} token={token} adminOnly={true}/>} />
      <Route path="/CalendarPage/:appId/:cusUsername" element={<RoleChecker element={<CalendarPage />} token={token} adminOnly={true}/>} />
      <Route path="/AddBookingPage" element={<RoleChecker element={<AddBookingPage />} token={token} adminOnly={true}/>} />
      <Route path="/ProManagePage" element={<RoleChecker element={<ProManagePage />} token={token} adminOnly={true}/>} />
      <Route path="/ReportImportProduct" element={<RoleChecker element={<ReportImportProduct />} token={token} adminOnly={true}/>} />
    </Routes>
  );
};
   
export default App;
