import "./Footer.css";
import Logo from "../../image/LOGO.png";

function Footer() {
  return (
    <div>
      <div className="footer-box">
        <div className="conten-div">
          <div>
            <img className="foot-logo" src={Logo} alt="My Image" />
          </div>
        </div>
        <div className="conten-div">
          <div>
            <h2>Context</h2>
            <h4>Text..........</h4>
            <h4>Text..........</h4>
            <h4>Text..........</h4>
            <h4>Text..........</h4>
          </div>
        </div>
        <div className="conten-div">
          <div>
            <h2>Resource</h2>
            <h4>Text..........</h4>
            <h4>Text..........</h4>
            <h4>Text..........</h4>
            <h4>Text..........</h4>
          </div>
        </div>
        <div className="conten-div">
          <div>
            <h2>SITMAP</h2>
            <h4>Text..........</h4>
            <h4>Text..........</h4>
            <h4>Text..........</h4>
            <h4>Text..........</h4>
          </div>
        </div>
      </div>
      <div className="inside-footer">
        <h3>Power By Hugcha team</h3>
      </div>
    </div>
  );
}
export default Footer;
