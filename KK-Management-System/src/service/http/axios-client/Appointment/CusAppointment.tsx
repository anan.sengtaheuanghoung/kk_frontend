

import axios from "axios";

export interface Branch {
  branch_id: string;
  branch_name: string;
 
}
interface ApiResponse {
  branches: Branch[];
}

export const FetchBranch = async (): Promise<Branch[]> => {
  try {
    const response = await axios.get<ApiResponse>("http://localhost:8080/branches");
     return response.data.branches;  
  } catch (error) {
    console.error("Error during show data", error);
    throw error;
  }
};



export const fecthEmployeeName = async (branchId: string) => {
  try {
    const response = await axios.get(`http://localhost:8080/employees/${branchId}`);
    return response.data; // Assuming the response contains an 'employees' array
  } catch (error) {
    console.error("Error during fetching employee names:", error);
    throw error;
  }
};


 



import { getStoredToken } from "../Login-axios"; 
import { jwtDecode, JwtPayload } from 'jwt-decode';

// interface UserData {
//   employeeId: string;
//   branchId: string;
//   time: string;
//   date: string;
//   userId: string; // Change cus_id to userId
// }

// interface ExtendedJwtPayload extends JwtPayload {
//   userId: string; // Change cus_id to userId
// }


// export const SubmitAppointment = (
//   employeeId: string,
//   branchId: string,
//   time: string,
//   date: string,
// ) => {
//   const authToken = getStoredToken();
//   console.log(authToken);

//   if (!authToken) {
//     console.error("Authentication token not found");
//     return;
//   }

//   const decodedToken: ExtendedJwtPayload = jwtDecode(authToken);
//   const userId: string = decodedToken.userId; 
//   const userData: UserData = {
//     employeeId: employeeId,
//     branchId: branchId,
//     time: time,
//     date: date,
//     userId: userId 
//   };

//   const headers = {
//     'Authorization': `Bearer ${authToken}`,
//     'Content-Type': 'application/json'
//   };

//   axios
//     .post("http://localhost:8080/submit-appointment", userData, { headers: headers })
//     .then((response) => {
//       console.log("Successful booking");
//       console.log(response.data.app_id);
//       return response.data.app_id
//     })
//     .catch((error) => {
//       if (error.response && error.response.status === 401) {
//         console.error("User not authenticated");
//       } else {
//         console.error("Error during booking:", error); 
//       }
      
//     });
// };



interface UserData {
  employeeId: string;
  branchId: string;
  time: string;
  date: string;
  userId: string; // Change cus_id to userId
}
interface ExtendedJwtPayload extends JwtPayload {
  userId: string; // Change cus_id to userId
}
export const SubmitAppointment = async (
  employeeId: string,
  branchId: string,
  time: string,
  date: string,
): Promise<string | void> => { // Return a promise that resolves to a string or void
  const authToken = getStoredToken();
  console.log(authToken);

  if (!authToken) {
    console.error("Authentication token not found");
    return;
  }
  const decodedToken: ExtendedJwtPayload = jwtDecode(authToken);
  const userId: string = decodedToken.userId; 
  const userData: UserData = {
    employeeId: employeeId,
    branchId: branchId,
    time: time,
    date: date,
    userId: userId 
  };
  const headers = {
    'Authorization': `Bearer ${authToken}`,
    'Content-Type': 'application/json'
  };
  try {
    const response = await axios.post("http://localhost:8080/submit-appointment", userData, { headers });
    console.log("Successful booking");
    console.log(response.data.app_id);
    return response.data.app_id;
  } catch (error) { 
  }
};






//cancle 
interface canceldata {
  app_id: string[];
  reason: string;
}

//Save button
export const CancelAppointment = (
  app_id: string[],
  reason: string,
 

) => {
  const CancleData: canceldata = {
      app_id: app_id,
      reason:reason,
  };
  const authToken = getStoredToken();
  const headers = {
    'Authorization': `Bearer ${authToken}`,
    'Content-Type': 'application/json'
  };
  axios
    .post("http://localhost:8080/cancel-appointment", CancleData, {
        headers: headers,
    })
    .then((response) => {
      console.log("cancel successful");
      console.log(response.data);
         
    })
    .catch((error) => {
      console.error("Error adding supplier:", error);
       
    });
};

