import React, { useState, useRef } from "react";
import { styled, css } from "@mui/system";
import { Modal as BaseModal } from "@mui/base/Modal";
import { Backdrop, Button, TextField } from "@mui/material";
import "./Order-Product-Popup.css";
import { SaveNewAmount } from "../../../../../service/http/axios-client/Admin-Side/Order-and-Import/Orders";
const blue = {
  200: "#99CCFF",
  300: "#66B2FF",
  400: "#3399FF",
  500: "#007FFF",
  600: "#0072E5",
  700: "#0066CC",
};

const grey = {
  50: "#F3F6F9",
  100: "#E5EAF2",
  200: "#DAE2ED",
  300: "#C7D0DD",
  400: "#B0B8C4",
  500: "#9DA8B7",
  600: "#6B7A90",
  700: "#434D5B",
  800: "#303740",
  900: "#1C2025",
};

const Modal = styled(BaseModal)`
  position: fixed;
  z-index: 1300;
  inset: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledBackdrop = styled(Backdrop)`
  z-index: -1;
  position: fixed;
  inset: 0;
  background-color: rgb(0 0 0 / 0.5);
  -webkit-tap-highlight-color: transparent;
`;

const ModalContent = styled("div")(
  ({ theme }) => css`
    font-family: "IBM Plex Sans", sans-serif;
    font-weight: 500;
    text-align: start;
    position: relative;
    display: flex;
    flex-direction: column;
    gap: 8px;
    width: 500px;
    height: auto;
    overflow: auto;
    background-color: ${theme.palette.mode === "dark" ? grey[900] : "white"};
    border-radius: 8px;
    box-shadow: 0 4px 12px
      ${theme.palette.mode === "dark" ? "rgb(0 0 0 / 0.5)" : "rgb(0 0 0 / 0.2)"};
    padding: 24px;
    color: ${theme.palette.mode === "dark" ? grey[50] : grey[900]};

    & .modal-title {
      margin: 0;
      line-height: 1.5rem;
      margin-bottom: 8px;
    }

    & .modal-description {
      margin: 0;
      line-height: 1.5rem;
      font-weight: 400;
      color: ${theme.palette.mode === "dark" ? grey[400] : grey[800]};
      margin-bottom: 4px;
    }
  `
);

const TriggerButton = styled("button")(
  ({ theme }) => css`
    font-family: "phetsarath ot";
    font-weight: 400;
    font-size: 18px;
    line-height: 1.5;
    padding: 8px 16px;
    border-radius: 8px;
    transition: all 150ms ease;
    cursor: pointer;
    background: #d9d9d9;
    color: black;
    box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);

    &:hover {
      background: "grey";
      border-color: ${theme.palette.mode === "dark" ? grey[600] : grey[300]};
    }

    &:active {
      background: ${theme.palette.mode === "dark" ? grey[700] : grey[100]};
    }

    &:focus-visible {
      box-shadow: 0 0 0 4px
        ${theme.palette.mode === "dark" ? blue[300] : blue[200]};
      outline: none;
    }
  `
);

export default function OrderProductPopup({
  productId,
}: {
  productId: string;
}) {
  const [open, setOpen] = useState(false);
  const contentRef = useRef<HTMLDivElement | null>(null);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  //store new amount
  const [newAmount, getNewAmount] = useState("");

  const handleSave = () => {
    const amountAsNumber = parseInt(newAmount); // Convert string to number

    if (isNaN(amountAsNumber)) {
      console.error("Invalid amount");
      return;
    }

    SaveNewAmount(productId, amountAsNumber);
  };

  return (
    <div>
      <TriggerButton onClick={handleOpen}>ສັ່ງ</TriggerButton>{" "}
      {/* Opens the modal */}
      <Modal
        open={open}
        onClose={handleClose}
        slots={{ backdrop: StyledBackdrop }}
      >
        <ModalContent>
          <div className="Order-Product-Popup-Maindiv" ref={contentRef}>
            <div className="Order-Product-Popup-title">
              <p>ກະລຸນາປ້ອນຈໍານວນເພື່ອສັ່ງ </p>
            </div>
            <div className="Order-Product-Popup-textfield">
              <p>ຈໍານວນ:</p>
              <TextField
                value={newAmount}
                onChange={(e) => getNewAmount(e.target.value)}
              ></TextField>
            </div>
            <div className="Order-Product-Popup-button">
              <Button>ກັບຄືນ</Button>
              <Button onClick={handleSave}>ຕົກລົງ</Button>
            </div>
          </div>
        </ModalContent>
      </Modal>
    </div>
  );
}
