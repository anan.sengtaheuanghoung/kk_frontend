// import React, { useState, useEffect } from 'react';
// import axios from 'axios';

// const EmployeeList = () => {
//   const [branches, setBranches] = useState([]);
//   const [selectedBranch, setSelectedBranch] = useState('');
//   const [employees, setEmployees] = useState([]);

//   // Fetch branches from the backend when the component mounts
//   useEffect(() => {
//     const fetchBranches = async () => {
//       try {
//         const response = await axios.get('/branches');
//         setBranches(response.data.branches);
//       } catch (error) {
//         console.error('Error fetching branches:', error);
//       }
//     };

//     fetchBranches();
//   }, []);

//   // Fetch employees when a branch is selected
//   useEffect(() => {
//     const fetchEmployees = async () => {
//       if (selectedBranch) {
//         try {
//           const response = await axios.get(/employees/${selectedBranch});
//           setEmployees(response.data.employees);
//         } catch (error) {
//           console.error('Error fetching employees:', error);
//         }
//       }
//     };

//     fetchEmployees();
//   }, [selectedBranch]);

//   const handleBranchChange = (event) => {
//     setSelectedBranch(event.target.value);
//   };

//   return (
//     <div>
//       <h1>Employee List</h1>
//       <div>
//         <label htmlFor="branchSelect">Select Branch:</label>
//         <select id="branchSelect" value={selectedBranch} onChange={handleBranchChange}>
//           <option value="">Select Branch</option>
//           {branches.map(branch => (
//             <option key={branch.branch_id} value={branch.branch_id}>{branch.branch_id}</option>
//           ))}
//         </select>
//       </div>
//       <div>
//         <h2>Employees:</h2>
//         <ul>
//           {employees.map(employee => (
//             <li key={employee.employee_id}>{employee.name}</li>
//           ))}
//         </ul>
//       </div>
//     </div>
//   );
// };

// export default EmployeeList;