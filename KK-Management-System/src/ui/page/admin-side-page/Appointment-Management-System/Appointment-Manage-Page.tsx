import "./Appointment-Manage-Page.css";
import SideBar from "../../../style/AdminPage-sideBar";
import AppointmentDisplay from "./component/Appointment-Display";
import AppointmentForm from "./component/Appointment-Form";
import { SetStateAction, useState } from "react";
import { Dayjs } from "dayjs";
function AppointmentManagePage() {
  //handle text search 
  const [searchQuery, setSearchQuery] = useState('');
  const handleSearch = (value: SetStateAction<string>) => {
    setSearchQuery(value);
  };
  //handle date Rangepicker 
  const [startDate, setStartDate] = useState<Dayjs | null>(null);
  const [endDate, setEndDate] = useState<Dayjs | null>(null);
  const handleDateChange = (start: Dayjs | null, end: Dayjs | null) => {
    setStartDate(start);
    setEndDate(end);
  };
  return (
    <div className="Appoint-manage-page">
      <div className="Appoint-page-first-grid">
        <SideBar />
      </div>
      <div className="Appoint-page-second-grid">
        <AppointmentForm onSearch={handleSearch} onDateChange={handleDateChange}/>
        <AppointmentDisplay searchData={searchQuery}  startDate={startDate} endDate={endDate} />
      </div>
    </div>
  );
}
export default AppointmentManagePage;
