
import './Booking-Display.css'
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Button } from "@mui/material";
import { useEffect, useState } from 'react';
import { fetchBookingReport } from '../../../../../../service/http/axios-client/Admin-Side/Report/Booking';


interface bookingData {
    app_id:string,
    cus_username:string,
    app_date:string,
    app_datetime:string,
    emp_name:string,
    app_status:string,

}

 function BookingDisplay () {
    //store state
    const [BookingImport, setBooking] = useState<bookingData[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
  
    const fetchBookingImport = async () => {
      try {
        const ImportList: bookingData[] = await fetchBookingReport();
        setBooking(ImportList);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching product data:", error);
      }
    };
   
    useEffect(() => {
      fetchBookingImport();
    }, []);
    return(
        <div className="BookingDisplay">
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell  style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ລະຫັດການຈອງ</TableCell>
                <TableCell  style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ຊື່ລູກຄ້າ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ວັນທີ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ເວລາ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ຊ່າງຕັດຜົມ</TableCell>
                <TableCell style={{fontSize:23, fontFamily:"phetsarath ot"}}align="center">ສະຖານະ</TableCell>
               </TableRow>
            </TableHead>
            <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={5}>Loading...</TableCell>
              </TableRow>                      
            ) : (
              BookingImport.map((booking) => (
                <TableRow key={booking.app_id}>
                  <TableCell className="Map-ReportOrder">
                    {booking.app_id}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {booking.cus_username}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {booking.app_date}
                  </TableCell>
                  <TableCell className="Map-ReportOrder">
                    {booking.app_datetime}
                  </TableCell>    
                  <TableCell className="Map-ReportOrder">
                    {booking.emp_name}
                  </TableCell>  
                  <TableCell className="Map-ReportOrder">
                    {booking.app_status}
                  </TableCell>              
                </TableRow>
              ))
            )}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    )
}
export default BookingDisplay