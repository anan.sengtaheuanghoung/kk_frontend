import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  styled,
  tableCellClasses,
} from "@mui/material";
import "./Cus-Manage-display.css";
import React, { ReactNode, useEffect, useState } from "react";
import axios from "axios";
import { create } from "zustand";
import { getStoredToken } from "../../../../../service/http/axios-client/Login-axios";

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

interface useCusStoreState {
  selectedData: {
    id: string;
    username: string;
    email: string;
    phoneNumber: string;
    password: string;
  };
  setSelectedData: (data: {
    id: string;
    username: string;
    email: string;
    phoneNumber: string;
    password: string;
  }) => void;
}

// Zustand store
export const useCusStore = create<useCusStoreState>((set) => ({
  selectedData: {
    id: "",
    username: "",
    email: "",
    phoneNumber: "",
    password: "",
  },
  setSelectedData: (data: any) => set({ selectedData: data }),
}));

interface Column {
  id: keyof Data;
  label: string;
  minWidth?: number;
  align?: "left" | "center" | "right";
}

const columns: readonly Column[] = [
  { id: "ID", label: "ລະຫັດ" },
  { id: "Name", label: "ຊື່" },
  { id: "PhoneNumber", label: "ເບີໂທ" },
  { id: "Email", label: "ອີເມວ" },
  { id: "Address", label: "ລະຫັດຜ່ານ" },
];

interface Data {
  cus_email: ReactNode;
  cus_pnumber: ReactNode;
  cus_username: ReactNode;
  cus_password: string;
  cus_id: number;
  ID: number;
  Name: string;
  PhoneNumber: number;
  Email: string;
  Address: string;
}

function CusMangeDisplay() {
  const [rows, setRows] = useState<Data[]>([]);
  const { selectedData, setSelectedData } = useCusStore();
  const [searchQuery, setSearchQuery] = useState("");

  const handleRowClick = (row: {
    cus_id: any;
    cus_email: any;
    cus_username: any;
    cus_password: any;
    cus_pnumber: any;
  }) => {
    setSelectedData({
      id: row.cus_id,
      username: row.cus_username,
      email: row.cus_email,
      password: row.cus_password,
      phoneNumber: row.cus_pnumber,
    });
  };

  const authToken = getStoredToken();
  const headers = {
    Authorization: `Bearer ${authToken}`,
  };
  //fetch data to display
  const fetchCusdata = () => {
    axios
      .get("http://localhost:8080/management/customers",{
        headers: headers,
      })
      .then((response) => {
        setRows(response.data);
        console.log("fetching Cusdata ", response.data);
      })
      .catch((error) => {
        console.error("Error fetching Cusdata data:", error);
      });
  };

  useEffect(() => {
    fetchCusdata();
  }, [selectedData]);
  const filteredRows = rows.filter(
    (row) =>
      typeof row.cus_username === "string" &&
      row.cus_username.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <div className="CusManageDisplay">
      <div className="CusManageDisplay-grid1">
        <p>ຄົ້ນຫາ:</p>
        <TextField
          className="CusManageDisplay-grid1-textfield"
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
          sx={{
            "& fieldset": { border: "none" },
          }}
        />
      </div>
      <div className="CusManageDisplay-grid2">
        <TableContainer sx={{ maxHeight: 340 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{
                      minWidth: column.minWidth,
                      fontSize: 24,
                      width: 100,
                      fontFamily: "phetsarath ot",
                    }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredRows.map((row) => (
                <StyledTableRow
                  key={row.cus_id}
                  onClick={() => handleRowClick(row)}
                >
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.cus_id}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.cus_username}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.cus_pnumber}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.cus_email}
                  </StyledTableCell>
                  <StyledTableCell style={{ fontSize: 17 }}>
                    {row.cus_password}
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}

export default CusMangeDisplay;
