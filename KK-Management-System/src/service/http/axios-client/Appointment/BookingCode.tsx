import axios from "axios";
import { getStoredToken, useUserProfileStore } from "../Login-axios";
import { JwtPayload, jwtDecode } from "jwt-decode";
import create from 'zustand';
import { useEffect, useState } from "react";








// interface BookingCodeState {
//   bookingCodes: string[];
//   appStatus: string; // Add appStatus property
//   cusId:string;
//   setBookingCodes: (codes: string[]) => void;
//   setAppStatus: (status: string) => void;   
//   setcusId: (status: string) => void;  

// }

// export const useBookingCodeStore = create<BookingCodeState>((set) => ({
//   bookingCodes: [],
//   appStatus: '', // Initialize appStatus
//   cusId:'',
//   setBookingCodes: (codes) => set({ bookingCodes: codes }),
//   setAppStatus: (status) => set({ appStatus: status }), // Define setAppStatus function
//   setcusId: (cusId) => set({ cusId: cusId }), // Define setAppStatus function

// }));


// interface ExtendedJwtPayload extends JwtPayload {
//   cusId: string;
// }


// const authToken = getStoredToken();
// const headers = {
//   Authorization: `Bearer ${authToken}`,
// };

// //code that you will get when you have done an appointment
// export const BookingCode = () => {
//   if (!authToken) {
//     console.error("Authentication token not found");
//     return;
//   }
//   const decodedToken: ExtendedJwtPayload = jwtDecode(authToken);
//   const cusId: string = decodedToken.cusId;
//   console.log("data from decode",cusId)
  
//   axios
//     .get("http://localhost:8080/your-code", {
//       headers: headers,
//       params: { cusId: cusId },
//     })
//     .then((response) => {
//       const bookingCodes = response.data.app_id;
//       const appStatus = response.data.app_status;
//       const cusId = response.data.cusId;


//       useBookingCodeStore.getState().setBookingCodes(bookingCodes); // Update the Zustand store
//       useBookingCodeStore.getState().setAppStatus(appStatus); // Update app status in Zustand store
//       useBookingCodeStore.getState().setcusId(cusId); // Update app status in Zustand store

//       console.log("appStatus Code Given", cusId  );
//     })
//     .catch((error) => {
//       if (error.response && error.response.status === 401) {
//         console.error("User not authenticated");
//       } else {
//         console.error("Error providing code", error);
//       }
//     });
// };

 


// interface BookingCodeState {
//   bookingCodes: string[];
//   setBookingCodes: (codes: string[]) => void;
// }
 


// // store zustand for booking code  
// export const useBookingCodeStore = create<BookingCodeState>((set) => ({
//   bookingCodes: [],
//   setBookingCodes: (codes) => set({ bookingCodes: codes }),
// }));
 
// interface ExtendedJwtPayload extends JwtPayload {
//   cusId: string;
// }
// //code that you will get when you have done an appointment
// export const BookingCode =   (cusId:string) => {
// const authToken = getStoredToken();
// const headers = {
//   Authorization: `Bearer ${authToken}`,
// };
//   if (!authToken) {
//     console.error("Authentication token not found");
//     return;
//   }
//   // const decodedToken: ExtendedJwtPayload = jwtDecode(authToken);
//   // const cusId: string = decodedToken.cusId;
//    axios
//     .get("http://localhost:8080/your-code", {
//       headers: headers,
//       params: { cusId: cusId },
//     })
      
//     .then ((response) => {
//       const bookingCodes = response.data.app_id; 
//       useBookingCodeStore.getState().setBookingCodes(bookingCodes); // Update the Zustand store
//       console.log("Booking Code Given", bookingCodes);
//     })
//     .catch((error) => {
//       if (error.response && error.response.status === 401) {
//         console.error("User not authenticated");
//       } else {
//         console.error("Error providing code", error);
//       }
//     });
// };



// Store Zustand for booking code  
interface BookingCodeState {
  bookingCodes: string[];
  setBookingCodes: (codes: string[]) => void;
}

export const useBookingCodeStore = create<BookingCodeState>((set) => ({
  bookingCodes: [],
  setBookingCodes: (codes) => set({ bookingCodes: codes }),
}));

interface ExtendedJwtPayload extends JwtPayload {
  cusId: string;
}



// Code that you will get when you have done an appointment
export const BookingCode = async (cusId: string): Promise<void> => {
  const authToken = getStoredToken();
  const headers = {
    Authorization: `Bearer ${authToken}`,
  };

  if (!authToken) {
    console.error("Authentication token not found");
    return;
  }
  try {
    const response = await axios.get("http://localhost:8080/your-code", {
      headers: headers,
      params: { cusId: cusId },
    });

    const bookingCodes = response.data.app_id;
    useBookingCodeStore.getState().setBookingCodes(bookingCodes); // Update the Zustand store
    console.log("Booking Code Given", bookingCodes);
  } catch (error) {
    
  }
};












  
export const StatusChecking = (cus_id:string ,  onUnsuccess: () => void,onSuccess: () => void
) => {
  console.log("see data at statuschecking ",cus_id)
  const requestData = {
   cusId:cus_id
  };
  const authToken = getStoredToken();
const headers = {
  Authorization: `Bearer ${authToken}`,
};
  axios
    .post("http://localhost:8080/status-checking", requestData,{   
      headers: headers, 
    })  
    .then((response) => {
       console.log(response.data); 
       onSuccess()
    })
    .catch((err) => { 
      onUnsuccess()
      console.error("Error during SaveProductType:", err.message);
    });
};








