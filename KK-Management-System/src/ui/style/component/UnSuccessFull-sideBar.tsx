import * as React from "react";
import Button from "@mui/material/Button";
import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import { Alert, Stack } from "@mui/material";
import { create } from "zustand";

interface UnsuccessfullSnackbar {
  UnsuccessOpen: boolean;
  setUnsuccessOpen: (UnsuccessOpen: boolean) => void;
}

export const useUnsuccessfulSnackbarStore = create<UnsuccessfullSnackbar>(
  (set) => ({
    UnsuccessOpen: false,
    setUnsuccessOpen: (UnsuccessOpen) => set({ UnsuccessOpen }),
  })
);

export const UnSuccessful = () => {
  const { UnsuccessOpen, setUnsuccessOpen } = useUnsuccessfulSnackbarStore();

  const handleClose = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setUnsuccessOpen(false);
  };

  return (
    <div>
      <Snackbar
        open={UnsuccessOpen}
        autoHideDuration={2000}
        onClose={handleClose}
        message="Note archived"
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Stack sx={{ width: "100%" }} spacing={2}>
          <Alert
            severity="error"
            style={{ width: 470, fontSize: 20, fontFamily: "phetsarath ot" }}
          >
            ການເຂົ້າສູ່ລະບົບຜິດພາດ!! (ກະລຸນາປ້ອນຂໍ້ມູນໃຫຖືກຕ້ອງ)
          </Alert>
        </Stack>
      </Snackbar>
    </div>
  );
};
