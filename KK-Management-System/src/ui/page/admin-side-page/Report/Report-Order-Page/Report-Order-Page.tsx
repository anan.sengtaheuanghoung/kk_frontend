import "./Report-Order-Page.css";
import ReportOrderDisplay from "./component/Report-Order-Display";
import ReportOrderForm from "./component/Report-Order-Form";
import SideBar from "../../../../style/AdminPage-sideBar";
import { SetStateAction, useState } from "react";
import { Dayjs } from "dayjs";
function ReportOrderPage() {
  //handle text search 
  const [searchQuery, setSearchQuery] = useState('');
  const handleSearch = (value: SetStateAction<string>) => {
    setSearchQuery(value);
  };

  //handle date Rangepicker 
  const [startDate, setStartDate] = useState<Dayjs | null>(null);
  const [endDate, setEndDate] = useState<Dayjs | null>(null);
  const handleDateChange = (start: Dayjs | null, end: Dayjs | null) => {
    setStartDate(start);
    setEndDate(end);
  };

  return (
    <div className="Report-Order-page">
      <div className="Report-Order-first-grid">
        <SideBar />
      </div>
      <div className="Report-Order-second-grid">
        <ReportOrderForm onSearch={handleSearch} onDateChange={handleDateChange}/>
        <ReportOrderDisplay searchData={searchQuery}  startDate={startDate} endDate={endDate} />
      </div>
    </div>
  );
}
export default ReportOrderPage;
