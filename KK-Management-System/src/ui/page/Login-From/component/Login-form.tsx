// import { useState } from "react";
// import { Button, FormControl, TextField } from "@mui/material";
// import "./Login-form.css";
//  import {
//   handleUserLogin,
// } from "../../../../service/http/axios-client/Login-axios";
// import { Link, useNavigate } from "react-router-dom";
// import Header from "../../../style/Header";
// import { useAuthStore } from "../../../style/Header";
// import { SuccessSnaekbar } from "../../../style/component/Successfull-Snaekbar";
// import { useSnackbarStore } from "../../../style/component/Successfull-Snaekbar";
// import { UnSuccessful, useUnsuccessfulSnackbarStore } from "../../../style/component/UnSuccessFull-sideBar";
// function LoginForm() {

//   const [usernameOrEmail, setUsernameOrEmail] = useState("");
//   const [password, setPassword] = useState("");
//   const navigate = useNavigate(); // Obtain the navigate function using useNavigate hook
//   const setIsLoggedIn = useAuthStore((state) => state.setIsLoggedIn);
//   const setOpen = useSnackbarStore((state) => state.setOpen);//
//   const setUnsuccessOpen = useUnsuccessfulSnackbarStore((state) => state.setUnsuccessOpen);

//   const handleSignIn = () => {
//     handleUserLogin(
//       usernameOrEmail,
//       password,
//       () => {
//         setTimeout(() => {
//           navigate('/');
//         }, 1500);
//       },
//       () => {
//         setUnsuccessOpen(true);
//       },
//       (token, role) => {
//         localStorage.setItem("token", token);
//         setIsLoggedIn(true);
//         setOpen(true);
//       }
//     );
//   };

//   return (
//     <>
//       <Header isLoggedIn={false} />
//       <FormControl className="Login-form-stye">
//         <h1>ເຂົ້າສູ່່ລະບົບ</h1>
//         <div className="form-Input">
//           <h4>ຊື່</h4>
//           <TextField
//             placeholder="Username"
//             type="email"
//             onChange={(e) => setUsernameOrEmail(e.target.value)}
//             style={{ width: 300, marginTop: -20 }}
//           />
//         </div>
//         <div className="form-Input">
//           <h4>ລະຫັດຜ່ານ</h4>
//           <TextField
//             placeholder="Password"
//             onChange={(e) => setPassword(e.target.value)}
//             style={{ width: 300, marginTop: -20 }}
//           />
//           <br />
//           <Link to="/UserNameCheck">ລືມລະຫັດຜ່ານ?</Link>
//         </div>
//         <Button variant="contained" onClick={handleSignIn}>
//           ເຂົ້າສູ່່ລະບົບ
//         </Button>
//         <p>
//           ທ່ານມີບັນຊີເເລ້ວບໍ?{" "}
//           <a href="/SignUpPage" style={{ color: "red" }}>
//             ສ້າງບັນຊີ
//           </a>
//         </p>
//       </FormControl>
//       <SuccessSnaekbar/>
//       <UnSuccessful/>
//     </>
//   );
// }

// export default LoginForm;


import { useEffect, useState } from "react";
import { Button, FormControl, TextField } from "@mui/material";
import "./Login-form.css";
import { handleUserLogin } from "../../../../service/http/axios-client/Login-axios";
import { Link, useNavigate } from "react-router-dom";
import Header from "../../../style/Header";
import { useAuthStore } from "../../../style/Header";
import { SuccessSnaekbar } from "../../../style/component/Successfull-Snaekbar";
import { useSnackbarStore } from "../../../style/component/Successfull-Snaekbar";
import {
  UnSuccessful,
  useUnsuccessfulSnackbarStore,
} from "../../../style/component/UnSuccessFull-sideBar";
import { getUserInfo } from "../../../../service/http/axios-client/Login-axios";
import { create } from "zustand";


interface UserData {
  cus_email: string;
  cus_username: string;
  emp_name: string;
  emp_photo: string;
}

 


function LoginForm() {
  const [usernameOrEmail, setUsernameOrEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate(); // Obtain the navigate function using useNavigate hook
  const setIsLoggedIn = useAuthStore((state) => state.setIsLoggedIn);
  const setOpen = useSnackbarStore((state) => state.setOpen);
  const setUnsuccessOpen = useUnsuccessfulSnackbarStore(
    (state) => state.setUnsuccessOpen
  );


  



const handleSignIn = () => {
    handleUserLogin(
      usernameOrEmail,
      password,
      () => {
        // Navigate based on user role
        setTimeout(() => {
          const role = localStorage.getItem("role");
          if (role === "admin" || role === "ceo" ) {
            navigate("/AdminDashBoardPage");
          }
          else if ( role === "employee") {
            navigate("/BookingPage");
          }
          else {
            navigate("/");
          }
        }, 1500);
      },
      () => {
        getUserInfo();
      },
      () => {
        setUnsuccessOpen(true);
      },
      (token, role, user) => {
        localStorage.setItem("token", token);
        localStorage.setItem("role", role);
        localStorage.setItem("user", JSON.stringify(user));
        setIsLoggedIn(true);
        setOpen(true);
      }
    );
  };



  

  return (
    <>
      <Header isLoggedIn={false} />
      <FormControl className="Login-form-stye">
        <h1>ເຂົ້າສູ່ລະບົບ</h1>
        <div className="form-Input">
          <h4>ຊື່</h4>
          <TextField
            placeholder="Username"
            type="email"
            onChange={(e) => setUsernameOrEmail(e.target.value)}
            style={{ width: 300, marginTop: -20 }}
          />
        </div>
        <div className="form-Input">
          <h4>ລະຫັດຜ່ານ</h4>
          <TextField
            type="password"
            placeholder="Password"
            onChange={(e) => setPassword(e.target.value)}
            style={{ width: 300, marginTop: -20 }}
          />
          <br />
          <Link to="/UserNameCheck">ລືມລະຫັດຜ່ານ?</Link>
        </div>
        <Button variant="contained" onClick={handleSignIn}>
          ເຂົ້າສູ່ລະບົບ
        </Button>
        <p>
          ທ່ານມີບັນຊີເເລ້ວບໍ?{" "}
          <a href="/SignUpPage" style={{ color: "red" }}>
            ສ້າງບັນຊີ
          </a>
        </p>
      </FormControl>
      <SuccessSnaekbar />
      <UnSuccessful />
    </>
  );
}

export default LoginForm;
